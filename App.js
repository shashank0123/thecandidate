import React, { useEffect, useState, Component} from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  EventEmitter,
  NativeModules,
  DeviceEventEmitter,
  NativeEventEmitter,
} from 'react-native';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import axios from 'axios';
import { Root, Toast, Container } from 'native-base';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import { url } from './src/utils/BaseUrl/BaseUrl';
import { removeToken } from './src/utils/Token/Token';

import Signup from './src/screens/Signup/Signup';
import Login from './src/screens/Login/Login';
import Tab from './src/components/Tab/Tab';
import Splash from './src/screens/Splash/Splash';
import Onbording from './src/screens/Onbording/Onbording';
import Auth from './src/components/Auth/Auth';
import Notification from './src/screens/Notification/Notification';
import PaymentHistory from './src/screens/PaymentHistory/PaymentHistory';
import Faq from './src/screens/Faq/Faq';
import CancelOrder from './src/components/CancelOrder/CancelOrder';
import Feedback from './src/components/Feedback/Feedback';
import PromoCode from './src/components/PromoCode/PromoCode';
import AddFeedback from './src/components/AddFeedback/AddFeedback';
import ProfileEdit from './src/screens/ProfileEdit/ProfileEdit';
import SearchPage from './src/components/SearchPage/SearchPage';


import {store} from './src/api/store'


const Stack = createStackNavigator();




export class App extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    removeToken().then(res => {

    })
   

    this.checkPermission();
    this.createNotificationListeners(); //add this line
  }

  componentWillMount() {
    Toast.toastInstance = null;
  }

  componentWillUnmount() {
    Toast.toastInstance = null;
    this.notificationListener();
    this.notificationOpenedListener();
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    console.log(fcmToken, 'token>>>>>>>>')
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }

  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }


  async createNotificationListeners() {

    // create channel

    const channel = new firebase.notifications.Android.Channel(
      'chargebizz',
      'Chargebizz',
      firebase.notifications.Android.Importance.Max
    ).setDescription('A natural description of the channel');
    firebase.notifications().android.createChannel(channel);



    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      // const { title, body } = notification;
      console.log(notification, 'foreground');

      if (Platform.OS === 'android') {

        const localNotification = new firebase.notifications.Notification({
          sound: 'default',
          show_in_foreground: true,
        })
          .setNotificationId(notification._notificationId)
          .setTitle(notification._title)
          .setSubtitle(notification._subtitle)
          .setBody(notification._body)
          .setData(notification._data)
          .android.setChannelId('chargebizz') // e.g. the id you chose above
          .android.setColor('#000000') // you can set a color here
          .android.setPriority(firebase.notifications.Android.Priority.High)
          .android.setAutoCancel(true) // close

        firebase.notifications()
          .displayNotification(localNotification)
          .catch(err => console.error(err));

      } else if (Platform.OS === 'ios') {

        const localNotification = new firebase.notifications.Notification()
          .setNotificationId(notification._notificationId)
          .setTitle(notification._title)
          .setSubtitle(notification._subtitle)
          .setBody(notification._body)
          .setData(notification._data)
        // .ios.setBadge(notification._ios.badge);

        firebase.notifications()
          .displayNotification(localNotification)
          .catch(err => console.error(err));

      }
    });

    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      console.log(notificationOpen, 'background clicked');
      const { title, body } = notificationOpen.notification;
    });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      const { title, body } = notificationOpen.notification;
      console.log(title + body + 'opened when app is closed');
    }
    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      console.log(JSON.stringify(message));
    });
  }

  render() {
    return (
      <Root>
        <Container>
          <NavigationContainer >
            <Stack.Navigator
              screenOptions={
                {
                  headerShown: false,
                  // cardStyle: { backgroundColor: '#FFFFFF' },
                }
              }
            >
              <Stack.Screen name="Splash" component={Splash} />
              <Stack.Screen name="Onbording" component={Onbording} />
              <Stack.Screen name="Tab" component={Tab} />
              <Stack.Screen name="Auth" component={Auth} />
              <Stack.Screen name="MyVehicle" component={MyVehicle} />
              <Stack.Screen name="Booking" component={Booking} />
              <Stack.Screen name="Notification" component={Notification} />
              <Stack.Screen name="PaymentHistory" component={PaymentHistory} />
              <Stack.Screen name="Faq" component={Faq} />
              <Stack.Screen name="Charging" component={Charging} />
              <Stack.Screen name="Account" component={ProfileEdit} />
              <Stack.Screen name="Search" component={SearchPage} />
            </Stack.Navigator>
          </NavigationContainer>
        </Container>
      </Root>
    )
  }
}

const styles = StyleSheet.create({

});

export default App;