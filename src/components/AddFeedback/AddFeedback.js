import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import Para from '../Shared/Text/Para'
import { commonStyle } from '../../utils/Style/Style'
import { fontFamily } from '../../utils/FontFamily/FontFamily'
import { Textarea, Button, Input } from 'native-base'
import validate from '../../utils/Validation/Validation'
import { TextInput } from 'react-native-gesture-handler'

export default class AddFeedback extends Component {

    state = {
        controls: {
            title: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                    required: true,
                }
            },
            description: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                    required: true,
                }
            },
        }
    }

    changeTextHandler = (event, key) => {
        this.setState(prevState => {
            return {
                controls: {
                    ...prevState.controls,
                    [key]: {
                        ...prevState.controls[key],
                        value: event,
                        touched: true,
                        valid: validate(event, prevState.controls[key].validate, key)
                    }
                }
            };
        });
    };

    addFeedback = () => {
        let data = {};
        for (let key of Object.keys(this.state.controls)) {
            data[key] = this.state.controls[key].value;
        }

        this.props.addFeedback(data);
    }

    render() {
        return (
            <View style={styles.feedback}>
                <Para bold={true} style={commonStyle.blue}>Please Share your Valuable Feedback</Para>
                <View style={styles.input}>
                    <TextInput style={styles.title} placeholder="Title" value={this.state.controls.title.value} onChangeText={(e) => this.changeTextHandler(e, 'title')} />
                </View>
                <View style={styles.textArea}>
                    <Textarea rowSpan={5} style={styles.text} placeholder="Feeback...." value={this.state.controls.description.value} onChangeText={(e) => this.changeTextHandler(e, 'description')} />
                </View>
                <View>
                    <Button style={commonStyle.button} onPress={this.addFeedback}><Para bold={true} style={commonStyle.white}>Submit</Para></Button>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    feedback: {
        padding: 25
    },
    textArea: {
        marginVertical: 20
    },
    text: {
        backgroundColor: '#D0E1F9',
        borderRadius: 10,
        fontFamily: fontFamily.regular
    },
    title: {
        backgroundColor: '#D0E1F9',
        borderRadius: 10,
        fontFamily: fontFamily.regular,
        padding: 10,
        marginTop: 15
    }
})
