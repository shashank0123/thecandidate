import React, { Component } from 'react'
import { Text, ImageBackground, Image, TouchableOpacity, StyleSheet, View, BackHandler, ToastAndroid } from 'react-native'
import Para from '../../components/Shared/Text/Para'
import AuthHeader from '../../components/Shared/AuthHeader/AuthHeader';
import CustomInput from '../../components/Shared/CustomInput/CustomInput';
const backgroundImage = require("../../../assets/images/login-signup.png");
import { Icon, Input } from 'native-base';
import { Fade } from '../Fade/Fade';
import Signup from '../../screens/Signup/Signup';
import Login from '../../screens/Login/Login';
import Otp from '../../screens/Otp/Otp';
import { ScrollView } from 'react-native-gesture-handler';
import { disableRoute } from '../../utils/Constants/Constatnts';
import { getLoginCount, getToken } from '../../utils/Token/Token';
import AsyncStorage from '@react-native-community/async-storage';
import WebView from 'react-native-webview';
var count = 0;

export default class Auth extends Component {

    constructor(props) {
        super(props);
        this.currentRouteName = 'auth'
        this.props.navigation.addListener('blur', () => {
            this.onLeave();
        });
        this.props.navigation.addListener('focus', () => {
            this.onFocus();
        });
    }

    componentDidMount = () => {
        getLoginCount().then(res => {
            if (res == 'yes') {
                this.tabChange('login');
            }
        })
    }

    state = {
        active: 'signup',
        otp: false,
        user: null,
        blocked: false
    }

    // componentWillReceiveProps = (props) => {
    //     // this.showOtp({name:'Umesh',phoneNumber:9342536478});
    //     if (props.route && props.route.params && props.route.params.active) {
    //         this.tabChange(props.route.params.active)
    //     }
    // }

    tabChange = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                active: val,
                otp: false
            }
        })
    }

    showOtp = (user) => {
        console.log(user);
        this.setState(prev => {
            return {
                ...prev,
                otp: true,
                user: user
            }
        })
    }

    afterOtp = (screen, params) => {
        this.setState(prev => {
            return {
                ...prev,
                active: 'signup',
                otp: false
            }
        })
        this.props.navigation.navigate(screen, params);
    }

    onFocus() {
        if (this.props.route && this.props.route.params && this.props.route.params.active) {
            this.tabChange(this.props.route.params.active);
            if (this.props.route.params.blocked) {
                this.showBlocked(this.props.route.params.blocked)
            }
        }
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);

        getToken().then(res => {
            if (res) {
                this.props.navigation.navigate('Tab')
            }
        })
    }

    onLeave() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    showBlocked = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                blocked: val
            }
        })
    }


    handleBackButtonClick = () => {
        if (disableRoute.indexOf(this.currentRouteName) >= 0) {
            count += 1;
            if (count == 1) {
                ToastAndroid.show('Press again for exit!', ToastAndroid.SHORT);
            }
            else if (count == 2) {
                count = 0;
                BackHandler.exitApp();
            }
            return true;
        }
        else {
            return false;
        }

    }


    render() {
        return (
            <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
                <ScrollView style={styles.signupPage}>
                    <AuthHeader active={this.state.active} tabChange={this.tabChange}></AuthHeader>
                    <Fade visible={(this.state.active == 'signup' && !this.state.otp)}>
                        <Signup navigateToOtp={this.showOtp}></Signup>
                    </Fade>
                    <Fade visible={(this.state.active == 'login' && !this.state.otp)}>
                        <Login navigateToOtp={this.showOtp} afterOtp={this.afterOtp} blocked={this.state.blocked} showBlocked={this.showBlocked}></Login>
                    </Fade>
                    <Fade visible={this.state.otp}>
                        <Otp user={this.state.user} active={this.state.active} afterOtp={this.afterOtp} tabChange={this.tabChange}></Otp>
                    </Fade>
                </ScrollView>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        // justifyContent: "center"
    },
    signupPage: {
        paddingVertical: 20,
        paddingHorizontal: 25,
    },
    formDiv: {
        paddingVertical: 70
    },
    nameDiv: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 40
    },
    nameFont: {
        fontSize: 25,
        color: 'white'
    },
    hello: {
        marginRight: 10
    },
    inputDiv: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#0F5ABF',
        marginBottom: 20,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 3,
        paddingHorizontal: 20,
        paddingVertical: 3

    },
    input: {
        // backgroundColor: 'red',
        flex: 1,
        marginRight: 15
    },
    icon: {
        color: '#9FBDE5',
        fontSize: 22
    },
    custom: {
        color: 'white'
    },
    socialIconDiv: {
        flexDirection: 'row'
    },
    socialIcon: {
        marginRight: 10,
        marginTop: 5
    },
    submitDiv: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between'
    },
    submitText: {
        color: 'white',
        fontSize: 17
    }

})
