import React, { Component } from 'react'
import { Text, StyleSheet, View, ImageBackground } from 'react-native'
import Para from '../Shared/Text/Para'
import { commonStyle } from '../../utils/Style/Style'
import { Image } from 'react-native-animatable'
import { Button } from 'native-base'
const backgroundImage = require("../../../assets/images/login-signup.png");

export default class BookedSuccess extends Component {

    state = {
        success: this.props.route.params.success,
        msg: this.props.route.params.msg
    }

    componentDidMount = () => {
    }

    navigate = (val) => {
        this.props.navigation.navigate(val);
    }

    goBack = () => {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
                <View style={styles.bookingComplete}>
                    <View style={[commonStyle.alignCenter]}>
                        {
                            this.state.success ?
                                <View style={commonStyle.alignCenter}>
                                    <Image source={require('../../../assets/images/success.png')} style={[commonStyle.img, styles.success]} />
                                    <Para bold={true} style={[commonStyle.white, styles.text]}>
                                        {this.state.msg}
                                    </Para>
                                </View>
                                :
                                <View style={commonStyle.alignCenter}>
                                    <Image source={require('../../../assets/images/failed.png')} style={[commonStyle.img, styles.failed]} />
                                    <Para bold={true} style={[commonStyle.white, styles.text]}>
                                        {this.state.msg}
                                    </Para>
                                </View>
                        }
                        {/* {
                            !this.state.success ?
                                <Button onPress={this.goBack} style={[commonStyle.button, styles.whiteBtn, styles.button, { marginBottom: 0 }]}><Para bold={true} style={commonStyle.blue}>Try Again</Para></Button>
                                : null
                        } */}
                        <Button onPress={() => this.navigate('Scan')} style={[commonStyle.button, styles.whiteBtn, styles.button]}><Para bold={true} style={commonStyle.blue}>Scan QR</Para></Button>
                        <Button onPress={() => this.navigate('Stations')} style={[commonStyle.button, styles.whiteBtn]}><Para bold={true} style={commonStyle.blue}>Go to Home</Para></Button>
                    </View>
                </View>
            </ImageBackground>

        )
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        resizeMode: "cover",
        // justifyContent: "center"
    },
    bookingComplete: {
        flex: 1,
        // backgroundColor: '#000823',
        paddingVertical: 25,
        paddingHorizontal: 35,
        justifyContent: 'center'
    },
    text: {
        fontSize: 20,
        textAlign: 'center',
        lineHeight: 30
    },
    success: {
        height: 100,
        width: 100,
        marginBottom: 20
    },
    failed: {
        height: 70,
        width: 70,
        marginBottom: 20
    },
    button: {
        marginTop: 30,
        marginBottom: 30
    },
    whiteBtn: {
        backgroundColor: 'white'
    }
})
