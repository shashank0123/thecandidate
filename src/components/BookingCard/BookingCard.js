import React, { Component } from 'react'
import { Text, StyleSheet, View, Linking } from 'react-native'
import { Grid, Col, Button, Root, Container } from 'native-base'
import Para from '../Shared/Text/Para'
import { commonStyle } from '../../utils/Style/Style'
import { Image } from 'react-native-animatable'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { dateFormat, time_convert } from '../../utils/Time/Time'
import Modal from 'react-native-modal';
import Delete from '../Delete/Delete'
import { url } from '../../utils/BaseUrl/BaseUrl'
import axios from 'axios';
import { msg, errMsg } from '../../utils/Toaster/Toaster'
import CancelOrder from '../CancelOrder/CancelOrder'

export default class BookingCard extends Component {

    state = {
        // cancelModalVisible: false,
        // loading: false
    }

    // cancelModal = (val) => {
    //     this.setState(prev => {
    //         return {
    //             ...prev,
    //             cancelModalVisible: val
    //         }
    //     })
    // }

    // cancelBooking = async () => {
    //     this.showLoading(true);
    //     try {
    //         let res = await axios.put(`${url}/booking/cancel/${this.props.booking._id}`);
    //         if (res) {
    //             msg(res['data']['msg']);
    //             console.log('hi>>>>>',res['data']['msg'])
    //             this.showLoading(false);
    //             this.props.update();
    //             this.cancelModal(false)
    //         }
    //     }
    //     catch (err) {
    //         if (err) {
    //             this.showLoading(false);
    //             errMsg(err.response)
    //         }
    //     }
    // }

    // showLoading = (val) => {
    //     this.setState(prev => {
    //         return {
    //             ...prev,
    //             loading: val
    //         }
    //     })
    // }


    openMap(station) {
        if (station && station.geolocation.coordinates) {
            console.log('open directions', station.geolocation.coordinates);
            if (Platform.OS == 'android') {
                Linking.openURL(`http://maps.google.com/maps?daddr=${station.geolocation.coordinates[1]},${station.geolocation.coordinates[0]}`);
            }
            else {
                Linking.openURL(`http://maps.apple.com/maps?daddr=${station.geolocation.coordinates[1]},${station.geolocation.coordinates[0]}`);
            }

        }

    }

    renderRating = (rating) => {
        const stars = []
        for (let i = 0; i < 5; i++) {
            stars.push((i < rating) ? <Image source={require('../../../assets/images/star-c.png')} style={[styles.starImg, commonStyle.img]} /> : <Image source={require('../../../assets/images/star.png')} style={[styles.starImg, commonStyle.img]} />)
        }
        return stars;
    }

    render() {

        return (
            <View style={styles.bookingCard}>
                <TouchableOpacity onPress={() => this.props.bookingDetails(this.props.booking)}><Para bold={true} style={styles.name}>{this.props.booking.station ? this.props.booking.station.name : ''}</Para></TouchableOpacity>
                <View style={[styles.grid, this.props.type != 'history' ? { paddingBottom: 10 } : null]}>
                    <View style={commonStyle.flexRow}>
                        <View style={{ width: '75%' }}>
                            <View style={commonStyle.flexRow}>
                                <Para style={{ width: 80 }} bold={true}>Order Id</Para>
                                <Para style={[styles.blueText, commonStyle.small]}>{this.props.booking.id}</Para>
                            </View>
                            <View style={commonStyle.flexRow}>
                                <Para style={{ width: 80 }} bold={true}>Start Time</Para>
                                <Para style={[styles.blueText, commonStyle.small]}>{dateFormat(this.props.booking.startTime)}</Para>
                            </View>
                            <View style={commonStyle.flexRow}>
                                <Para style={{ width: 80 }} bold={true}>Time Slot</Para>
                                <Para style={[styles.blueText, commonStyle.small]}>{time_convert(this.props.booking.minutes)}</Para>
                            </View>
                            <View style={commonStyle.flexRow}>
                                <Para style={{ width: 80 }} bold={true}>Amount</Para>
                                <Para style={[styles.blueText, commonStyle.small]}>₹ {this.props.booking.price.toFixed(2)}</Para>
                            </View>
                        </View>
                        <View style={{ width: '25%' }}>
                            {
                                this.props.type == 'history' ?
                                    <View>
                                        <TouchableOpacity style={styles.bookNow} onPress={() => this.props.bookAgain(this.props.booking)}>
                                            <Image source={require('../../../assets/images/book.png')} style={[styles.book, commonStyle.img]} />
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <View style={commonStyle.flexEnd}>
                                        <TouchableOpacity onPress={() => this.openMap(this.props.booking.station)}>
                                            <View style={[commonStyle.flexEnd]}>
                                                <Image source={require('../../../assets/images/navigate.png')} style={[commonStyle.img, styles.navigateimg]} />
                                            </View>
                                            <Para style={[commonStyle.blue, commonStyle.small]}>Navigate</Para>
                                        </TouchableOpacity>
                                    </View>
                            }
                        </View>
                    </View>
                </View>
                <View style={styles.bottom}>
                    {/* 8th Feb, 05:45 pm */}
                    {
                        this.props.booking.status ?
                            <Para bold={true} style={[styles.smallText, (this.props.booking.status.toLowerCase() == 'cancelled') ? { color: 'red' } : null]}>{this.props.booking.status}</Para>
                            : null
                    }
                    {
                        this.props.type == 'history' ?
                            <View style={styles.star}>
                                {
                                    this.renderRating(this.props.booking.rating ? this.props.booking.rating.star : 0)
                                }
                                {/* <Image source={require('../../../assets/images/star.png')} style={[styles.starImg, commonStyle.img]} />
                                <Image source={require('../../../assets/images/star.png')} style={[styles.starImg, commonStyle.img]} />
                                <Image source={require('../../../assets/images/star.png')} style={[styles.starImg, commonStyle.img]} /> */}
                            </View>
                            : <View>
                                <TouchableOpacity style={styles.cancel} onPress={() => this.props.cancelModal(true, this.props.booking)}><Para style={[commonStyle.blue, commonStyle.small]}>Cancel</Para></TouchableOpacity>
                            </View>
                    }
                </View>
                {/* <Modal isVisible={this.state.cancelModalVisible} style={styles.modal} animationType="slide" animationOut='slideOutDown' animationIn='slideInUp'>
                    <Root>
                        <Container style={{ backgroundColor: 'transparent', display: 'flex', flexDirection: 'row', alignItems: 'flex-end' }}>
                            <View style={styles.modalView}>
                                <View style={styles.modalContent}>
                                    <CancelOrder cancel={this.cancelBooking} closeModal={() => this.cancelModal(false)} booking={this.props.booking} loading={this.state.loading}></CancelOrder>
                                </View>
                            </View>
                        </Container>
                    </Root>
                </Modal> */}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    bookingCard: {
        marginVertical: 10,
        padding: 15,
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: "#e0e0e0",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0,
        shadowRadius: 0.84,

        elevation: 1,
    },
    blueText: {
        color: '#808080',
        marginLeft: 10
    },
    smallText: {
        color: '#808080',
        fontSize: 13,
    },
    name: {
        fontSize: 18,
        marginBottom: 10
    },
    grid: {
        // paddingBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#F0F0F0'
    },
    bottom: {
        paddingTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:'center'
    },
    book: {
        height: 80,
        width: 80,
        position: 'relative',
        left: 15,
        top: -10
    },
    bookNow: {
        alignItems: 'flex-end'
    },
    star: {
        flexDirection: 'row'
    },
    starImg: {
        height: 12
    },
    navigateimg: {
        height: 40,
        width: 40,
        marginBottom: 5
    },
    cancel: {
        height: 22,
        paddingHorizontal: 7,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#1371EF',
        borderRadius: 5,
        display: 'flex',
        alignItems: 'center',
        flexDirection:'row',
        justifyContent: 'center',
        elevation: 0
    },
    // modalContent: {
    //     paddingTop: 20,
    //     paddingBottom: 10
    // },
    // modalView: {
    //     backgroundColor: 'white',
    //     borderTopLeftRadius: 10,
    //     borderTopRightRadius: 10,
    //     position: 'relative',
    //     width:'100%'
    //     // width:Dimension.width,
    //     // flex:1
    //     // left:0
    //     // position:'absolute',
    //     // bottom:0
    // },
    // modal: {
    //     margin: 0, // This is the important style you need to set
    //     // alignItems: undefined,
    //     justifyContent: 'flex-end',
    // },
})
