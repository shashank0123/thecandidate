import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import Para from '../Shared/Text/Para';
import { commonStyle } from '../../utils/Style/Style';

export default class BreakupCharges extends Component {

    componentDidMount = () => {
        console.log(this.props.charges);
    }
    render() {
        return (
            <View style={styles.breakup}>
                <Para extraBold={true} style={styles.name}>Booking Charges</Para>
                <View style={styles.pricing}>
                    <View style={[commonStyle.flexRow]}>
                        <View><Para bold={true} style={commonStyle.blue}>Pricing Per Minute : </Para></View>
                        <View><Para style={[commonStyle.gray, commonStyle.small]}>₹ {this.props.charges.pricingPerMinute}</Para></View>
                    </View>
                    <View style={[commonStyle.flexRow, { marginTop: 5 }]}>
                        <View><Para bold={true} style={commonStyle.blue}>Slot Time : </Para></View>
                        <View><Para style={[commonStyle.gray, commonStyle.small]}>{this.props.charges.slotTime} min</Para></View>
                    </View>
                    <View style={[commonStyle.flexRow, { marginTop: 5 }]}>
                        <View><Para bold={true} style={commonStyle.blue}>Price : </Para></View>
                        <View><Para style={[commonStyle.gray, commonStyle.small]}>₹ {this.props.charges.price ? this.props.charges.price.toFixed(2) : 0}</Para></View>
                    </View>
                    <View style={[commonStyle.flexRow, { marginTop: 5 }]}>
                        <View><Para bold={true} style={commonStyle.blue}>Coupon Discount: </Para></View>
                        <View><Para style={[commonStyle.gray, commonStyle.small]}>₹ {this.props.charges.discount ? this.props.charges.discount.toFixed(2) : 0}</Para></View>
                    </View>
                    <View style={[commonStyle.flexRow, { marginTop: 5 }]}>
                        <View><Para bold={true} style={commonStyle.blue}>GST : </Para></View>
                        <View><Para style={[commonStyle.gray, commonStyle.small]}>₹ {this.props.charges.gst ? this.props.charges.gst.toFixed(2) : 0}</Para></View>
                    </View>
                    <View style={[commonStyle.flexRow, { marginTop: 5 }]}>
                        <View><Para bold={true} style={commonStyle.blue}>Total Charges : </Para></View>
                        <View><Para style={[commonStyle.gray, commonStyle.small]}>₹ {this.props.charges.total ? this.props.charges.total.toFixed(2) : 0}</Para></View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    name: {
        fontSize: 20
    },
    breakup: {
        paddingHorizontal: 25,
        paddingVertical: 20
    },
    pricing: {
        marginTop: 20
    }
})
