import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, Linking, TouchableOpacity } from 'react-native'
import Para from '../Shared/Text/Para'
import { commonStyle } from '../../utils/Style/Style'
import { Button, Spinner } from 'native-base'
import { dateFormat } from '../../utils/Time/Time'
import { tcUrl } from '../../utils/BaseUrl/BaseUrl'

export default class CancelOrder extends Component {


    goback = () => {
        this.props.navigation.goBack()
    }

    openTC = () => {
        Linking.openURL(tcUrl);
    }


    render() {
        return (
            <View style={styles.cancelView}>
                {
                    this.props.booking ?
                        <View>
                            <Para style={[styles.name, commonStyle.blue]} extraBold={true}>Cancel Order</Para>
                            <View>
                                <View style={[commonStyle.flexRow, { marginBottom: 6 }]}>
                                    <Para style={styles.marginLeft}>Order Id</Para>
                                    <Para style={[commonStyle.blue]}>{this.props.booking.id}</Para>
                                </View>
                                <View style={[commonStyle.flexRow, { marginBottom: 6 }]}>
                                    <Para style={styles.marginLeft}>Timing</Para>
                                    <Para style={[commonStyle.blue]}>{dateFormat(this.props.booking.startTime)}</Para>
                                </View>
                                <View style={[commonStyle.flexRow, { marginBottom: 6 }]}>
                                    <Para style={styles.marginLeft}>Charges</Para>
                                    <Para style={[commonStyle.blue]}>₹ {this.props.booking.price}</Para>
                                </View>
                            </View>

                            <View style={[commonStyle.flexRow, { marginTop: 30 }]}>
                                <Image source={require('../../../assets/images/warning.png')} style={[styles.warningImg, commonStyle.img]} />
                                <Para style={[styles.name]} extraBold={true}>Warning</Para>
                            </View>
                            <View>
                                <Para style={commonStyle.gray}>If you cancel 24 hours prior to the booking, you will get 100% of your booking amount in 3-5 working days.</Para>
                                <Para style={commonStyle.gray}>If you cancel with less than 24 hours prior to the booking, then you will be charged a cancelation amount of 20%.  The remaining 80% of your booking amount will be refunded in 3-5 working days.</Para>
                            </View>
                            <View>
                                <TouchableOpacity onPress={this.openTC}><Para extraBold={true} style={{ marginTop: 10 }}>Read Terms & Conditions</Para></TouchableOpacity>
                            </View>
                            <View style={styles.buttonDiv}>
                                <Button style={styles.button} onPress={this.props.cancel}>
                                    {
                                        this.props.loading ?
                                            <Spinner color='white' size={40}></Spinner>
                                            :
                                            <Para bold={true} style={commonStyle.white}>Yes, Cancel Order</Para>
                                    }
                                </Button>
                                <Button style={[styles.buttonBorder]} onPress={this.props.closeModal}><Para bold={true} style={styles.borderText}>No, Don't</Para></Button>
                            </View>
                        </View>
                        : null
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    cancelView: {
        padding: 25
    },
    name: {
        fontSize: 20,
        marginBottom: 20
    },
    warningImg: {
        marginRight: 20,
        height: 20,
        width: 20
    },
    marginLeft: {
        width: 80
    },
    buttonDiv: {
        marginTop: 30,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    button: {
        width: '45%',
        display: 'flex',
        borderRadius: 5,
        height: 60,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonBorder: {
        width: '45%',
        display: 'flex',
        borderRadius: 5,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderWidth: 1.5,
        borderColor: '#1371EF'
    },
    borderText: {
        color: '#1371EF',
        textAlign: 'center',
        display: 'flex',

    }
})
