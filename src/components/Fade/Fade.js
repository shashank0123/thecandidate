import { Animated } from 'react-native';
import React, { Component } from 'react';

export class Fade extends Component {

    state = {
        visible: this.props.visible,
    };

    componentWillMount() {
        this._visibility = new Animated.Value(this.props.visible ? 1 : 0);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.visible) {
            this.setState({ visible: true });
        }
        Animated.timing(this._visibility, {
            toValue: nextProps.visible ? 1 : 0,
            duration: 600,
            useNativeDriver:true
        }).start(() => {
            this.setState({ visible: nextProps.visible });
        });
    }

    render() {
        const { visible, style, children, ...rest } = this.props;

        const containerStyle = {
            opacity: this._visibility.interpolate({
                inputRange: [0.3, 1],
                outputRange: [0.3, 1],
            }),
            // transform: [
            //     {
            //         translateY: this._visibility.interpolate({
            //             inputRange: [-10, 1],
            //             outputRange: [-20, 0]
            //         })
            //     },
            // ],
        };

        const combinedStyle = [containerStyle, style];
        return (
            <Animated.View style={this.state.visible ? combinedStyle : containerStyle} {...rest}>
                {this.state.visible ? children : null}
            </Animated.View>
        );
    }
}