import React, { Component } from 'react'
import { Text, StyleSheet, View, Image,TouchableOpacity } from 'react-native'
import { commonStyle } from '../../utils/Style/Style'
import Para from '../Shared/Text/Para'
import { Icon, Button, Textarea, Spinner } from 'native-base'
import { fontFamily } from '../../utils/FontFamily/FontFamily'
import validate from '../../utils/Validation/Validation'
import { msg, errMsg } from '../../utils/Toaster/Toaster'
import { url } from '../../utils/BaseUrl/BaseUrl'
import axios from 'axios';

export default class Feedback extends Component {

    state = {
        stars: [{ active: false, val: 1 }, { active: false, val: 2 }, { active: false, val: 3 }, { active: false, val: 4 }, { active: false, val: 5 }],
        controls: {

            star: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                    required: true,
                }
            },
            description: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                }
            },
            badges: {
                value: [],
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                }
            },
        },
        loading: false
    }

    changeTextHandler = (event, key) => {
        this.setState(prevState => {
            return {
                controls: {
                    ...prevState.controls,
                    [key]: {
                        ...prevState.controls[key],
                        value: event,
                        touched: true,
                        valid: validate(event, prevState.controls[key].validate, key)
                    }
                }
            };
        });
    };

    ratingChange = (val) => {
        this.changeTextHandler(val, 'star');
        let stars = [...this.state.stars];
        stars.forEach(star => {
            if (star.val <= val) {
                star.active = true;
            }
            else {
                star.active = false;
            }
        });
        this.setState(prev => {
            return {
                ...prev,
                stars: stars
            }
        })
    }

    submit = async () => {
        this.showLoading(true);
        let data = {}
        for (let key of Object.keys(this.state.controls)) {
            data[key] = this.state.controls[key].value;
        }

        // console.log(data, 'data>>>');
        try {
            let res = await axios.put(`${url}/booking/rate/${this.props.booking._id}`, data);
            if (res) {
                this.showLoading(false);
                msg(res['data']['msg']);
                this.props.getBooking();
                this.props.ratingModal(false);
            }
        }
        catch (err) {
            if (err) {
                this.showLoading(false);
                errMsg(err.response);
            }
        }
    }

    showLoading = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                loading: val
            }
        })
    }

    tagChange = (val) => {
        let badges = this.state.controls.badges.value;
        let index = badges.indexOf(val);
        if (index >= 0) {
            badges.splice(index, 1);
        }
        else {
            badges.push(val);
        }

        this.setState(prev => {
            return {
                ...prev,
                controls: {
                    ...prev.controls,
                    badges: {
                        ...prev.controls.badges,
                        value: badges
                    }
                }
            }
        })
    }


    render() {
        return (
            <View style={styles.feedback}>
                <View style={[commonStyle.textCenter]}>
                    <View style={[commonStyle.textCenter, commonStyle.flexRow]}>
                        <Para extraBold={true} style={[styles.name, commonStyle.blue]}>Thankyou, You have saved</Para>
                    </View>
                    <View style={styles.save}>
                        <Para style={commonStyle.blue} bold={true}>0.5</Para>
                        <Image source={require('../../../assets/images/tree.png')} style={[styles.tree, commonStyle.img]} />
                        <Para style={commonStyle.blue} bold={true}>this charge</Para>
                    </View>
                    <View>
                        <Para style={[commonStyle.onlyTextCenter, commonStyle.gray]}>To help us serve you better,</Para>
                        <Para style={[commonStyle.onlyTextCenter, commonStyle.gray]}>kindly rate your experience.</Para>
                    </View>
                    <View style={styles.starDiv}>
                        {
                            this.state.stars.map((star, i) => {
                                return < Icon name="star" key={i} style={[styles.star, star.active ? styles.active : null]} onPress={() => this.ratingChange(star.val)} />
                            })
                        }
                    </View>
                    <View style={styles.tab}>
                        <TouchableOpacity style={[styles.tabContent, this.state.controls.badges.value.indexOf('clean') >= 0 ? styles.tagActive : null]} onPress={() => this.tagChange('clean')}>
                            <Para style={[this.state.controls.badges.value.indexOf('clean') >= 0 ? commonStyle.white : null, commonStyle.small]}>Clean</Para>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.tabContent, this.state.controls.badges.value.indexOf('fast') >= 0 ? styles.tagActive : null]} onPress={() => this.tagChange('fast')}>
                            <Para style={[this.state.controls.badges.value.indexOf('fast') >= 0 ? commonStyle.white : null, commonStyle.small]}>Fast</Para>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.tabContent, this.state.controls.badges.value.indexOf('helpful') >= 0 ? styles.tagActive : null]} onPress={() => this.tagChange('helpful')}>
                            <Para style={[this.state.controls.badges.value.indexOf('helpful') >= 0 ? commonStyle.white : null, commonStyle.small]}>Helpful</Para>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.tabContent, this.state.controls.badges.value.indexOf('slow') >= 0 ? styles.tagActive : null]} onPress={() => this.tagChange('slow')}>
                            <Para style={[this.state.controls.badges.value.indexOf('slow') >= 0 ? commonStyle.white : null, commonStyle.small]}>Slow</Para>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.textArea}>
                        <Textarea rowSpan={5} style={styles.text} placeholder="Write your experience ...." onChangeText={(e) => this.changeTextHandler(e, 'description')} />
                    </View>
                    <View style={styles.buttonView}>
                        <Button style={styles.cancelBtn} onPress={this.props.cancel}><Para style={[commonStyle.blue]} bold={true}>Cancel</Para></Button>
                        <Button style={[commonStyle.button, styles.saveBtn]} onPress={this.submit}>
                            {
                                this.state.loading ?
                                    <Spinner color='white' size={40}></Spinner>
                                    :
                                    <Para bold={true} style={commonStyle.white}>Submit</Para>
                            }
                        </Button>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    feedback: {
        padding: 25
    },
    name: {
        fontSize: 25
    },
    save: {
        paddingTop: 10,
        paddingBottom: 20,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    tree: {
        height: 23
    },
    starDiv: {
        flexDirection: 'row',
        marginTop: 20,
        marginBottom: 20,
        justifyContent: 'center'
    },
    star: {
        margin: 5,
        color: '#D0E1F9',
        fontSize: 32
    },
    tab: {
        display: "flex",
        justifyContent: 'space-between',
        flexDirection: 'row',
        height: 50,
        width: '100%',
        // padding: 8
    },
    tabContent: {
        backgroundColor: 'white',
        borderRadius: 10,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        // width:'100%',
        flex: 1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 8,
        paddingHorizontal: 7,
        paddingVertical: 7,
        margin: 5,
        textAlign: 'center'
    },
    textArea: {
        marginVertical: 20
    },
    text: {
        backgroundColor: '#D0E1F9',
        borderRadius: 10,
        fontFamily: fontFamily.regular
    },
    active: {
        color: 'orange'
    },
    tagActive: {
        backgroundColor: '#1371EF'
    },
    buttonView: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    cancelBtn: {
        backgroundColor: 'white',
        width: '30%',
        marginRight: 10,
        height: 50,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    saveBtn: {
        width: '30%',
        height: 50
    }
})
