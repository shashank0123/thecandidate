import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native'
import axios from 'axios';
import { url } from '../../utils/BaseUrl/BaseUrl';
import Para from '../Shared/Text/Para';
import { commonStyle } from '../../utils/Style/Style';
import { Button } from 'native-base';

export default class Filter extends Component {

    state = {
        powersArray: [],
        pinsArray: [],
        controls: {
            power: {
                value: ''
            },
            pin: {
                value: ''
            }
        }
    }

    componentDidMount = () => {
        this.getPowerPin();
    }

    getPowerPin = async () => {
        try {
            let res = await axios.get(`${url}/common/meta`)
            if (res) {
                console.log(res['data']['data'])
                this.setState(prev => {
                    return {
                        ...prev,
                        powersArray: res['data']['data']['powers'],
                        pinsArray: res['data']['data']['pin'],
                        controls: {
                            ...prev.controls,

                            power: {
                                ...prev.controls.power,
                                value: res['data']['data']['powers'][0]
                            },
                            pin: {
                                ...prev.controls.pin,
                                value: res['data']['data']['pin'][0]
                            }
                        }
                    }
                })
            }
        }
        catch (err) {
            if (err) {
                console.log(err)
            }
        }
    }

    changeHandler = (val, key) => {
        this.setState(prev => {
            return {
                ...prev,
                controls: {
                    ...prev.controls,
                    [key]: {
                        ...prev.controls[key],
                        value: val
                    }
                }
            }
        })
    }

    applyFilter = () => {
        let data = {};
        for (let control of Object.keys(this.state.controls)) {
            data[control] = this.state.controls[control].value;
        }
        this.props.cancel();
        this.props.applyFilter(data);
    }

    render() {
        return (
            <View style={{ paddingHorizontal: 25, paddingVertical: 20 }}>
                <View><Para bold={true} style={styles.bigFont}>Apply Filters</Para></View>
                <View style={styles.filterDiv}>
                    {
                        this.state.powersArray.length > 0 ?
                            <View>
                                <Para bold={true}>Power</Para>
                                <View style={styles.tab}>
                                    {
                                        this.state.powersArray.map((power, i) => {
                                            return <TouchableOpacity key={i} style={[styles.tabContent, (this.state.controls.power.value == power) ? styles.activeTab : null]} onPress={() => this.changeHandler(power, 'power')}>
                                                <Para style={[(this.state.controls.power.value == power) ? styles.activePara : null, commonStyle.small]}>{power}</Para>
                                            </TouchableOpacity>


                                        })
                                    }
                                </View>
                            </View>
                            : null
                    }
                </View>

                <View style={styles.filterDiv}>
                    {
                        this.state.pinsArray.length > 0 ?
                            <View>
                                <Para bold={true}>Pin</Para>
                                <View style={styles.tab}>
                                    {
                                        this.state.pinsArray.map((pin, i) => {
                                            return <TouchableOpacity key={i} style={[styles.tabContent, (this.state.controls.pin.value == pin) ? styles.activeTab : null]} onPress={() => this.changeHandler(pin, 'pin')}>
                                                <Para style={[(this.state.controls.pin.value == pin) ? styles.activePara : null, commonStyle.small]}>{pin}</Para>
                                            </TouchableOpacity>


                                        })
                                    }
                                </View>
                            </View>
                            : null
                    }
                </View>
                <View style={styles.buttonDiv}>
                    {/* <Button style={[commonStyle.button, styles.whiteBtn, { width: '40%' }]} onPress={this.props.cancel}><Para bold={true} style={commonStyle.blue}>Cancel</Para></Button> */}
                    <Button style={[commonStyle.button, { width: '40%' }]} onPress={this.applyFilter}><Para bold={true} style={commonStyle.white}>Apply</Para></Button>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    tab: {
        display: "flex",
        // justifyContent: 'space-around',
        flexDirection: 'row',
        paddingVertical: 8,
        paddingHorizontal: 10,
        marginTop: 10
    },
    tabContent: {
        backgroundColor: 'white',
        borderRadius: 10,
        textAlign: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        marginRight: 20,

        elevation: 8,
        paddingHorizontal: 10,
        paddingVertical: 8,
    },
    activePara: {
        color: 'white'
    },
    activeTab: {
        backgroundColor: '#1371EF'
    },
    filterDiv: {
        marginTop: 20
    },
    buttonDiv: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: 30
    },
    whiteBtn: {
        marginRight: 15,
        backgroundColor: 'white'
    },
    bigFont: {
        fontSize: 20
    }
})
