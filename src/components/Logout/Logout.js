import React from 'react'
import { View, Text } from 'react-native'
import Para from '../Shared/Text/Para'

const Logout = () => {
    return (
        <View>
            <View>
                <Para style={styles.title} extraBold={true}>Logout</Para>
                <Para>Are you sure?</Para>
                <View style={styles.buttonView}>
                    <Button onPress={this.props.closeModal} style={styles.noButton}><Para bold={true} style={commonStyle.blue}>No</Para></Button>
                    <Button style={[commonStyle.button, styles.yesButton]} onPress={this.props.logout}><Para bold={true} style={commonStyle.white}>Yes</Para></Button>
                </View>
            </View>
        </View>
    )
}

export default Logout
