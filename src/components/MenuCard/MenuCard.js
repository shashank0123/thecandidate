import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import Para from '../Shared/Text/Para'
import { TouchableOpacity } from 'react-native-gesture-handler'

export default class MenuCard extends Component {

    render() {
        return (
            <TouchableOpacity style={styles.menuCard} onPress={() => this.props.navigate(this.props.menu)}>
                <Para bold={true}>{this.props.menu.title}</Para>
                {this.props.menu.image}
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    menuCard: {
        padding: 18,
        backgroundColor: 'white',
        marginBottom: 20,
        borderRadius: 10,
        shadowColor: "#e0e0e0",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0,
        shadowRadius: 0.84,

        elevation: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
})
