import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { commonStyle } from '../../utils/Style/Style'
import Para from '../Shared/Text/Para'

export default class NoBooking extends Component {
    render() {
        return (
            <View>
                <View style={[commonStyle.flexRow, commonStyle.textCenter]}>
                    {this.props.image}
                </View>
                <View style={styles.content}>
                    <Para extraBold={true} style={styles.head}>{this.props.title}</Para>
                    <Para style={styles.para}>{this.props.para}</Para>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    head: {
        fontSize: 16,
        color: '#1371EF'
    },
    para: {
        color: '#808080',
        textAlign: 'center',
        marginTop: 10
    },
    content: {
        alignItems: 'center',
        paddingHorizontal: 20,
        marginTop: 25
    }
})
