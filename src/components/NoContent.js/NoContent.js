import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import Para from '../Shared/Text/Para'
import Dimension from '../../utils/Dimension/Dimension'
import { commonStyle } from '../../utils/Style/Style'
import { TouchableOpacity } from 'react-native-gesture-handler'

export default class NoContent extends Component {
    render() {
        return (
            <View style={styles.noBookingDiv}>
                <TouchableOpacity onPress={this.props.click ? this.props.addVehicle : null}>
                    <View style={[commonStyle.flexRow, commonStyle.textCenter]}>
                        {this.props.image}
                    </View>
                    <View style={styles.content}>
                        <Para extraBold={true} style={styles.head}>{this.props.title}</Para>
                        <Para style={styles.para}>{this.props.para}</Para>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    noBookingDiv: {
        flex: 1,
        height: '100%',
        justifyContent: 'center',
        // height:Dimension.height -100
    },
    head: {
        fontSize: 16,
        color: '#1371EF'
    },
    para: {
        color: '#808080',
        textAlign: 'center',
        marginTop: 10
    },
    content: {
        alignItems: 'center',
        paddingHorizontal: 20,
        marginTop: 25
    }
})
