import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Para from '../Shared/Text/Para'
import { commonStyle } from '../../utils/Style/Style'
import { Image } from 'react-native-animatable'
import Axios from 'axios'
import { url } from '../../utils/BaseUrl/BaseUrl'

export default class NotificationCard extends Component {

    state = {
        notification: null,
    }
    componentDidMount = () => {
        this.getUserData();
    }

    getUserData = async () => {
        let notification = {
            title: '',
            body: ''
        }
        try {
            let res = await Axios.get(`${url}/user`);
            if (res) {
                notification.title = this.props.noti.title.replace('{{name}}', res['data']['data']['name']);
                notification.title = notification.title.replace('{{phoneNumber}}', res['data']['data']['phoneNumber']);
                notification.body = this.props.noti.body.replace('{{phoneNumber}}', res['data']['data']['phoneNumber']);
                notification.body = notification.body.replace('{{name}}', res['data']['data']['name']);
                this.setState((prev) => {
                    return {
                        ...prev,
                        notification: notification
                    }
                });
            }
        }
        catch (err) {
            if (err) {
                console.log(err.response)
            }
        }
    }

    render() {
        return (
            <View style={styles.menuCard}>
                <View style={{ width: '70%' }}>
                    {
                        this.state.notification ?
                            <View>
                                <Para bold={true}>{this.props.noti ? this.state.notification.title : ''}</Para>
                                <Para style={[commonStyle.small, commonStyle.gray, styles.para]}>{this.props.noti ? this.state.notification.body : ''}</Para>
                                <TouchableOpacity onPress={this.props.navigate}><Para bold={true} style={[commonStyle.small, commonStyle.blue]}>Book Now</Para></TouchableOpacity>
                            </View>
                            : null
                    }
                </View>
                <View>
                    <Image source={require('../../../assets/images/not-img.png')} style={[styles.notImg, commonStyle.img]} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    menuCard: {
        padding: 18,
        backgroundColor: 'white',
        marginBottom: 20,
        borderRadius: 10,
        shadowColor: "#e0e0e0",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0,
        shadowRadius: 0.84,

        elevation: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    notImg: {
        height: 60,
        width: 60,
    },
    para: {
        marginVertical: 5
    }
})
