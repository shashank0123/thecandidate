import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { Item, Input, Button } from 'native-base'
import Para from '../Shared/Text/Para'
import { commonStyle } from '../../utils/Style/Style'
import { fontFamily } from '../../utils/FontFamily/FontFamily'
import { errMsg } from '../../utils/Toaster/Toaster'

export default class OtherModel extends Component {

    state = {
        controls: {
            manufacturer: {
                value: ''
            },
            model: {
                value: ''
            }
        }
    }

    getData = () => {
        if (this.props.options == 'both') {
            if (this.state.controls.model.value && this.state.controls.manufacturer.value) {
                this.props.modalData({ manufacturer: this.state.controls.manufacturer.value, model: this.state.controls.model.value })
            }
            else {
                errMsg('All fields are required');
            }
        }
        else {
            if (this.state.controls.model.value) {
                this.props.modalData({ model: this.state.controls.model.value })
            }
            else {
                errMsg('Model is required');
            }
        }
    }

    onChangeText = (event, key) => {
        this.setState(prevState => {
            return {
                controls: {
                    ...prevState.controls,
                    [key]: {
                        ...prevState.controls[key],
                        value: event,
                    }
                }
            };
        });
    };

    render() {
        return (
            <View style={{ padding: 20 }}>
                <View>
                    {
                        this.props.options == 'both' ?
                            <Para extraBold={true} style={{ fontSize: 20, marginBottom: 15 }}>Add Menufacturer</Para>
                            :
                            <Para extraBold={true} style={{ fontSize: 20, marginBottom: 15 }}>Add Model</Para>

                    }
                </View>
                {
                    this.props.options == 'both' ?
                        <View style={styles.inputDiv}>
                            <Item style={styles.item}>
                                <Input placeholder="Manufecturer" style={styles.input} value={this.state.controls.manufacturer.value} onChangeText={(e) => this.onChangeText(e, 'manufacturer')} placeholderTextColor="#808080"></Input>
                            </Item>
                        </View>
                        : null
                }


                <View style={styles.inputDiv}>
                    <Item style={styles.item}>
                        <Input placeholder="Model" style={styles.input} value={this.state.controls.model.value} onChangeText={(e) => this.onChangeText(e, 'model')} placeholderTextColor="#808080"></Input>
                    </Item>
                </View>

                <View style={styles.buttonView}>
                    <Button onPress={() => this.props.closeModal(this.props.options)} style={styles.noButton}><Para bold={true} style={commonStyle.blue}>Cancel</Para></Button>
                    <Button style={[commonStyle.button, styles.yesButton]} onPress={this.getData}><Para bold={true} style={commonStyle.white}>Save</Para></Button>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    inputDiv: {
        width: '100%',
        // backgroundColor: '#E7EEF9',
        // borderRadius: 10,
        // paddingVertical: 5,
        marginBottom: 20
    },
    input: {
        fontFamily: fontFamily.regular
    },
    buttonView: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: 15
    },
    noButton: {
        width: '30%',
        height: 50,
        backgroundColor: 'white',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    yesButton: {
        width: '30%',
        height: 50,
        marginLeft: 15
    }
})
