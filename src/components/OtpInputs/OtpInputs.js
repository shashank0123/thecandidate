import React from 'react';
import { StyleSheet } from 'react-native';
import { Content, Item, Input, Col, Grid } from 'native-base';

class OtpInputs extends React.Component {
    state = { otp: [] };
    otpTextInput = [];

    componentDidMount() {
        this.otpTextInput[0]._root.focus();
    }

    renderInputs() {
        const inputs = Array(4).fill(0);
        const txt = inputs.map(
            (i, j) => <Col key={j} style={styles.txtMargin}><Item regular style={styles.item}>
                <Input
                    style={[styles.inputRadius]}
                    keyboardType="numeric"
                    maxLength={1}
                    onChangeText={v => this.focusNext(j, v)}
                    onKeyPress={e => this.focusPrevious(e.nativeEvent.key, j)}
                    ref={ref => this.otpTextInput[j] = ref}
                />
            </Item></Col>
        );
        return txt;
    }

    focusPrevious(key, index) {
        if (key === 'Backspace' && index !== 0)
            this.otpTextInput[index - 1]._root.focus();
    }

    focusNext(index, value) {
        if (index < this.otpTextInput.length - 1 && value) {
            this.otpTextInput[index + 1]._root.focus();
        }
        if (index === this.otpTextInput.length - 1) {
            this.otpTextInput[index]._root.blur();
        }
        const otp = this.state.otp;
        otp[index] = value;
        this.setState({ otp });
        this.props.getOtp(otp.join(''));


    }


    render() {
        return (
            this.renderInputs()
        );
    }
}

const styles = StyleSheet.create({
    gridPad: { padding: 30 },
    txtMargin: { margin: 3 },
    inputRadius: {
        textAlign: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#0F5ABF',
        marginBottom: 8,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 3,
        paddingHorizontal: 20,
        paddingVertical: 3,
        color: 'white',
    },
    item: {
        borderColor: 'transparent',
    }
});

export default OtpInputs;