import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import Para from '../Shared/Text/Para'
import { Button } from 'native-base'
import { commonStyle } from '../../utils/Style/Style'

export default class PaymentFailed extends Component {
    render() {
        return (
            <View style={styles.failed}>
                <View>
                    <Para style={styles.title} extraBold={true}>Transaction Failed</Para>
                    <Para>{this.props.errMsg}</Para>
                    <View style={styles.buttonView}>
                        <Button onPress={this.props.closeModal} style={styles.noButton}><Para bold={true} style={commonStyle.blue}>Cancel</Para></Button>
                        {
                            this.props.failedCount >= 2 ?
                                null : <Button style={[commonStyle.button, styles.yesButton]} onPress={this.props.tryAgain}><Para bold={true} style={commonStyle.white}>Try Again</Para></Button>
                        }
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    failed: {
        paddingHorizontal: 20,
        paddingVertical: 15
    },
    title: {
        fontSize: 20,
        marginBottom: 10
    },
    buttonView: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: 40
    },
    noButton: {
        width: '30%',
        height: 50,
        backgroundColor: 'white',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 20
    },
    yesButton: {
        width: '30%',
        height: 50
    }
})
