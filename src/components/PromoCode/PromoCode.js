import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, Keyboard } from 'react-native'
import { Item, Input, Button } from 'native-base'
import CustomInput from '../Shared/CustomInput/CustomInput'
import Para from '../Shared/Text/Para'
import { commonStyle } from '../../utils/Style/Style'
import { fontFamily } from '../../utils/FontFamily/FontFamily'
import axios from 'axios';
import { url } from '../../utils/BaseUrl/BaseUrl'
import { ScrollView } from 'react-native-gesture-handler'

export default class PromoCode extends Component {

    state = {
        promoCodes: [],
        promocode: null,
        // selectedPromo:null
    }

    componentDidMount = () => {
        this.getPromo();
    }

    getPromo = async () => {
        try {
            let res = await axios.get(`${url}/promocode`);
            if (res) {
                console.log(res['data'])
                this.setState(prev => {
                    return {
                        ...prev,
                        promoCodes: res['data']['data']
                    }
                })
            }
        }
        catch (err) {
            if (err) {
                console.log(err.response)
            }
        }
    }

    onChangeText = (event) => {
        this.setState(prev => {
            return {
                ...prev,
                promocode: event
            }
        })
    }

    apply = (code) => {
        this.props.applyPromoCode(code);
    }

    render() {
        return (
            <ScrollView style={styles.promoCode} keyboardShouldPersistTaps='always'>
                <View style={styles.inputDiv}>
                    <Item style={styles.item}>
                        <Input placeholder="Enter/Paste Promocode here" style={styles.input} value={this.state.promocode} onChangeText={(e) => this.onChangeText(e)} placeholderTextColor="#808080"></Input>
                    </Item>
                    <View style={styles.applyBtn}>
                        <TouchableOpacity onPress={() => this.apply(this.state.promocode)}><Para bold={true} style={[styles.textApply, commonStyle.blue]}>Apply</Para></TouchableOpacity>
                    </View>
                </View>
                <View>
                    <Para bold={true}>Exclusive Offers for you</Para>
                </View>
                <View style={styles.codes}>
                    {
                        this.state.promoCodes.length > 0 ?
                            this.state.promoCodes.map((promo, i) => {
                                return <View key={i}>
                                    <View style={[styles.description, (i == this.state.promoCodes.length - 1) ? { borderColor: 'white' } : null]}>
                                        <View style={{ width: '70%', paddingRight: 10 }}>
                                            <Para bold={true} style={styles.promoName}>{promo.name}</Para>
                                            <Para style={[commonStyle.gray, styles.promoText]}>{promo.description}</Para>
                                        </View>
                                        <View style={{ width: '30%', paddingLeft: 10 }}>
                                            <Button style={styles.btn} onPress={() => this.apply(promo.name)}><Para bold={true} style={commonStyle.blue}>Apply</Para></Button>
                                        </View>
                                    </View>
                                </View>
                            })
                            : <Para style={commonStyle.gray}>No Promo Code is available for you.</Para>
                    }
                </View>
            </ScrollView >
        )
    }
}

const styles = StyleSheet.create({
    promoCode: {
        padding: 25
    },
    promoName: {
        textTransform: 'uppercase'
    },
    inputDiv: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        backgroundColor: '#E7EEF9',
        borderRadius: 10,
        paddingVertical: 5,
        marginBottom: 20
    },
    item: {
        width: '70%',
        borderColor: 'transparent',
        paddingLeft: 10
    },
    applyBtn: {
        flexGrow: 1,
        alignItems: 'center'
    },
    input: {
        fontFamily: fontFamily.regular,
        fontSize: 14
    },
    codes: {
        marginTop: 20
    },
    description: {
        marginTop: 15,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: '#e0e0e0',
        paddingBottom: 15
        // justifyContent:'space-between'
    },
    promoText: {
        // width: '70%',
    },
    btn: {
        backgroundColor: 'white',
        elevation: 0,
        borderWidth: 1,
        borderColor: '#1371EF',
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 35
    }
})
