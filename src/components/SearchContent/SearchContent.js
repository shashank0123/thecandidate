import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import StationBox from '../StationBox/StationBox'
import { Button } from 'native-base'
import { ScrollView } from 'react-native-gesture-handler'

export default class SearchContent extends Component {

    render() {
        return (
            <ScrollView>
                <Button onPress={this.props.showList}><Text>Normal</Text></Button>
                <View style={styles.SearchContent}>
                    {
                        (this.props.stationData && this.props.stationData.nearBy) ?
                            this.props.stationData.nearBy.map((station, i) => {
                                return <StationBox station={station} key={i} in={i} length={this.props.stationData.nearBy.length - 1}></StationBox>
                            })
                            : null
                    }
                </View>
                <View style={styles.SearchContent}>
                    {
                        (this.props.stationData && this.props.stationData.frequent) ?
                            this.props.stationData.frequent.map((station, i) => {
                                return <StationBox station={station} key={i} in={i} length={this.props.stationData.frequent.length - 1}></StationBox>
                            })
                            : null
                    }
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    SearchContent: {
        paddingHorizontal: 15,
        paddingBottom: 70,
    }
})
