import React, { Component } from 'react'
import { Text, StyleSheet, View, Animated, TouchableOpacity, BackHandler, ToastAndroid } from 'react-native'
// import { TranslateYAndOpacity, SharedElement } from 'react-native-motion';
import SearchBar from '../Shared/SearchBar/SearchBar';
import { url, mapPlaceUrl, placesSelectedParams } from '../../utils/BaseUrl/BaseUrl';
import axios from 'axios';
import Geolocation from 'react-native-geolocation-service';
import Para from '../Shared/Text/Para';
import StationBox from '../StationBox/StationBox';
import { ScrollView } from 'react-native-gesture-handler';
import Dimension from '../../utils/Dimension/Dimension';
import { commonStyle } from '../../utils/Style/Style';
import { disableRoute } from '../../utils/Constants/Constatnts';
import { Image } from 'react-native-animatable';
import Filter from '../Filter/Filter';
import Modal from 'react-native-modal';
import Axios from 'axios';
import SearchStationSkeleton from '../Skeleton/SearchStationSkeleton';
import { Root, Container } from 'native-base';

var count = 0;

export default class SearchPage extends Component {
    constructor(props) {
        super(props);
        this.startAnimation();
        this.currentRouteName = 'search';
        this.props.navigation.addListener('blur', () => {
            this.onLeave();
        });
        this.props.navigation.addListener('focus', () => {
            this.onFocus();
        });
    }

    state = {
        active: 'search',
        animation: new Animated.Value(300),
        stationData: null,
        placesArray: [],
        showFilter: false,
        filterData: null,
        loading: false
    }

    componentDidMount = () => {
        setTimeout(() => {
            this.setState(prev => {
                return {
                    ...prev,
                    stationData: this.props.route.params.stationData
                }
            })
        }, 500)
    }

    startAnimation = () => {
        Animated.timing(this.state.animation, {
            toValue: 0,
            duration: 500,
            useNativeDriver: true
        }).start(() => {
            this.state.animation.setValue(0);
            //If you remove above line then it will stop the animation at toValue point
        });

    }

    // getLocation = () => {
    //     Geolocation.getCurrentPosition(
    //         (position) => {
    //             this.getStations({ long: position.coords.longitude, lat: position.coords.latitude });
    //         },
    //         (error) => {
    //             errMsg(error.message);
    //             this.getStations({ long: 72.6328277, lat: 23.205429 });
    //         },
    //         { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    //     );

    // }


    // getStations = async (params) => {
    //     try {
    //         let res = await axios.get(`${url}/station/near-by`, { params: params });
    //         if (res) {
    //             console.log(res['data'])
    //             // this.showLocationOfStations(res['data']['data']['nearBy']);
    //             this.setState(prev => {
    //                 return {
    //                     ...prev,
    //                     stationData: res['data']['data'],
    //                     // displayStation: [...res['data']['data']['nearBy'].slice(0, 2)]
    //                 }
    //             })
    //         }
    //     }
    //     catch (err) {
    //         if (err) {
    //             console.log(err.response)
    //         }
    //     }
    // }


    singleStation = (station) => {
        // console.log(station, 'stations')
        this.props.route.params.removeAnimation(station);
        this.props.navigation.navigate('Stations');
    }

    getPlaces = (places) => {
        console.log(places, 'places')
        this.setState(prev => {
            return {
                ...prev,
                placesArray: places
            }
        })
    }

    onSelectAddress = (place) => {
        this.getPlace(place.place_id);
    }

    getPlace = async (place) => {
        let params = { ...{ placeid: place }, ...placesSelectedParams };
        try {
            let res = await axios.get(`${mapPlaceUrl}`, { params: params });
            if (res) {
                if (res['data']['result']) {
                    let location = res['data']['result']['geometry'] ? res['data']['result']['geometry']['location'] : '';
                    if (location) {
                        // console.log(location, 'location')
                        this.props.route.params.goBack({ lat: location.lat, long: location.lng });
                        this.props.navigation.navigate('Stations');
                    }

                }
            }
        }
        catch (err) {
            if (err) {
                console.log(err, err.response)
            }
        }
    }

    onFocus() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    onLeave() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }


    handleBackButtonClick = () => {
        this.props.route.params.removeAnimation('');
        if (disableRoute.indexOf(this.currentRouteName) >= 0) {
            count += 1;
            if (count == 1) {
                ToastAndroid.show('Press again for exit!', ToastAndroid.SHORT);
            }
            else if (count == 2) {
                count = 0;
                BackHandler.exitApp();
            }
            return true;
        }
        else {
            return false;
        }

    }

    goBack = () => {
        this.props.route.params.removeAnimation('');
        this.props.navigation.navigate('Stations');
    }

    getLocation = () => {
        Geolocation.getCurrentPosition(
            (position) => {
                this.getStations({ long: position.coords.longitude, lat: position.coords.latitude });
            },
            (error) => {
                errMsg(error.message);
                // console.log(error);
                this.getStations({ long: 72.6328277, lat: 23.205429 });
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );

    }

    getStations = async (params) => {
        this.showLoading(true);
        if (this.state.filterData) {
            params = { ...params, ...this.state.filterData };
        }
        try {
            let res = await Axios.get(`${url}/station/near-by`, { params: params });
            if (res) {
                this.showLoading(false);
                this.setState(prev => {
                    return {
                        ...prev,
                        stationData: res['data']['data'],
                    }
                })
            }
        }
        catch (err) {
            if (err) {
                this.showLoading(false);
                console.log(err.response, 'err')
            }
        }
    }

    showLoading = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                loading: val
            }
        })
    }




    filterModal = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                showFilter: val
            }
        })
    }

    applyFilter = (data) => {
        this.setState(prev => {
            return {
                ...prev,
                filterData: data
            }
        });
        // this.props.route.params.filterApplied(data);
        this.getLocation();
    }

    cancelFilter = () => {
        this.setState(prev => {
            return {
                ...prev,
                filterData: null
            }
        });
        // this.props.route.params.filterApplied(null);
        this.getLocation();
    }

    render() {
        const transformStyle = {
            transform: [{
                translateY: this.state.animation,
            }]
        }
        return (
            <Animated.View style={[transformStyle, styles.searchContent]}>
                <View style={styles.placeView}>
                    <View style={[commonStyle.flexRow, commonStyle.alignCenter]}>
                        <TouchableOpacity onPress={this.goBack}><Image source={require('../../../assets/images/search-back.png')} style={[styles.searchIcon, commonStyle.img]} /></TouchableOpacity>
                        {/* <Para bold={true}>Search</Para> */}
                    </View>
                    <View style={styles.searchBar}>
                        <SearchBar active={this.state.active} showSearch={this.showSearch} searchStations={this.searchStations} getPlaces={this.getPlaces} filter={() => this.filterModal(true)} cancelFilter={this.cancelFilter} filterData={this.state.filterData}></SearchBar>
                        {
                            this.state.placesArray.length > 0 ?
                                <View style={styles.places}>
                                    {
                                        this.state.placesArray.map((place, i) => {
                                            return <TouchableOpacity style={styles.list} onPress={() => this.onSelectAddress(place)} key={i}>
                                                <Para style={commonStyle.small}>{place.description}</Para>
                                            </TouchableOpacity>
                                        })
                                    }
                                </View>
                                : null
                        }
                    </View>
                </View>
                <View style={styles.listView}>
                    {
                        !this.state.loading ?
                            <ScrollView keyboardShouldPersistTaps="always">

                                {
                                    (this.state.stationData && this.state.stationData['nearBy']) ?

                                        <View>
                                            <Para bold={true} style={{ paddingHorizontal: 10 }}>NearBy</Para>
                                            {
                                                this.state.stationData['nearBy'].map((item, index) => {
                                                    return <StationBox station={item} key={index} length={this.state.stationData['nearBy'].length - 1} in={index} singleStation={this.singleStation}></StationBox>
                                                })
                                            }
                                        </View>
                                        : null
                                }

                                <View style={{ marginTop: 15 }}>
                                    {
                                        (this.state.stationData && this.state.stationData['frequent']) ?

                                            <View>
                                                <Para bold={true} style={{ paddingHorizontal: 10 }}>Frequent</Para>
                                                {
                                                    this.state.stationData['frequent'].map((item, index) => {
                                                        return <StationBox station={item} key={index} length={this.state.stationData['frequent'].length - 1} in={index} singleStation={this.singleStation}></StationBox>
                                                    })
                                                }
                                            </View>
                                            : null
                                    }
                                </View>

                            </ScrollView>
                            : <SearchStationSkeleton></SearchStationSkeleton>
                    }
                </View>
                <Modal isVisible={this.state.showFilter} style={styles.modal} animationOut='slideOutDown' animationIn='slideInUp' backdropOpacity={0.2}>
                    <Root>
                        <Container style={{ backgroundColor: 'transparent', display: 'flex', flexDirection: 'row', alignItems: 'flex-end' }}>
                            <View style={styles.modalView}>
                                <View style={styles.closeBtn}>
                                    <TouchableOpacity onPress={() => this.filterModal(false)}>
                                        <Image source={require('../../../assets/images/close.png')} style={[commonStyle.img, styles.closeIcon]} />
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.modalContent}>
                                    <Filter charges={this.state.charges} cancel={() => this.filterModal(false)} applyFilter={this.applyFilter}></Filter>
                                </View>
                            </View>
                        </Container>
                    </Root>
                </Modal>
            </Animated.View>
        )
    }
}

const styles = StyleSheet.create({
    searchContent: {
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    searchIcon: {
        height: 30,
        width: 30,
        // marginRight:50
    },
    listView: {
        position: 'relative',
        zIndex: 10,
        height: Dimension.height,
        paddingBottom: 40,
        paddingTop:120
    },
    placeView:{
        position: 'absolute',
        top:10,
        left:20,
        width:'100%',
        zIndex: 1000000,
    },
    searchBar: {
        // position: 'relative',
        // zIndex: 100000
    },
    places: {
        // position: 'absolute',
        // top: 68,
        // left: 5,
        width: dimension.width - 50,
        paddingHorizontal: 25,
        backgroundColor: 'white',
        zIndex: 1000000,
        borderRadius: 10,
        shadowColor: "#e0e0e0",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 1.25,
        shadowRadius: 3.84,

        elevation: 2,
    },
    list: {
        padding: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#e0e0e0',
        position: 'relative',
        zIndex: 100000
    },
    modal: {
        margin: 0, // This is the important style you need to set
        // alignItems: undefined,
        justifyContent: 'flex-end',
    },
    closeBtn: {
        position: 'absolute',
        right: 15,
        top: -28,
        zIndex: 10,
        height: 60,
        width: 60,
    },
    closeIcon: {
        width: '100%',
        height: '100%'
    },
    modalContent: {
        paddingTop: 20,
        paddingBottom: 10
    },
    modalView: {
        backgroundColor: 'white',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        position: 'relative',
        width:'100%'
        // width:Dimension.width,
        // flex:1
        // left:0
        // position:'absolute',
        // bottom:0
    },
})
