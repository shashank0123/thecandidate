import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import Para from '../Text/Para';
import { TouchableOpacity } from 'react-native-gesture-handler';


export default class AuthHeader extends Component {
    render() {
        return (
            <View style={styles.logoDiv}>
                <Image
                    source={require("../../../../assets/images/logo.png")}
                    style={styles.image}
                />
                <View style={styles.tab}>
                    <TouchableOpacity onPress={() => this.props.tabChange('login')} style={ this.props.active == 'login' ? styles.borderBottom : null}><Para bold={true} style={[styles.para]}>Sign In</Para></TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.tabChange('signup')} style={[this.props.active == 'signup' ? styles.borderBottom : null,styles.singupText]}><Para bold={true} style={[styles.para]}>Sign Up</Para></TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    logoDiv: {
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center'
    },
    image: {
        height: 60,
        width: 60,
        marginLeft: -5
    },
    tab: {
        flexDirection: 'row'
    },
    para: {
        color: 'white',
        fontSize: 18,
        paddingBottom: 5
    },
    singupText: {
        marginLeft: 20
    },
    borderBottom: {
        borderBottomWidth: 2,
        borderColor: 'white'
    }
})
