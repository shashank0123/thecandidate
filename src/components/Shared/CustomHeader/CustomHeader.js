import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import { Header, Left, Body, Right, Title, Button, Icon, } from 'native-base';
import { fontFamily } from '../../../utils/FontFamily/FontFamily';
import { commonStyle } from '../../../utils/Style/Style';

export default class CustomHeader extends Component {

    render() {
        return (
            <Header style={styles.header}>

                <Left style={[styles.index, !this.props.backBtn ? styles.hide : null]}>
                    <Button transparent onPress={this.props.goBack}>
                        <Icon style={commonStyle.white} name='arrow-back' />
                    </Button>
                </Left>
                <Body style={[styles.index, styles.title]}>
                    <Title style={styles.headerText}>{this.props.title}</Title>
                </Body>
                <Right style={[styles.rightIcon, styles.index, !this.props.rightIcon ? styles.hide : null]}>
                    <Button transparent onPress={this.props.rightClick}>
                        <Icon style={commonStyle.white} name={this.props.icon} />
                    </Button>
                </Right>
                {/* <Image source={require('../../../../assets/images/header-back.png')} style={styles.headerImage}></Image> */}
            </Header>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        position: 'relative',
        // height: 100,
        display: 'flex',
        alignItems: 'flex-end',
        backgroundColor: '#1371EF',
        elevation: 0,
        width: '100%',
    },
    index: {
        position: 'relative',
        zIndex: 1,
        // backgroundColor:'red'
    },
    headerText: {
        fontFamily: fontFamily.bold,
        fontSize: 22,
        color:'white'
    },
    headerImage: {
        position: "absolute",
        top: 0,
        left: 0,
        height: '100%',
    },
    title: {
    },
    hide: {
        opacity: 0
    }
})
