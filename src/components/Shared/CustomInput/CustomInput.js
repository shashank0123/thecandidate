import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native';
import { Input, Item } from 'native-base';
import Para from '../Text/Para';


export default class CustomInput extends Component {


    render() {
        return (
            <View>
                <Item regular style={styles.item} >
                    <Input {...this.props} style={[styles.currentInput, this.props.style]} />
                </Item>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    item: {
        borderColor: 'transparent',
        marginLeft: -10,
        // color:'white'
    }
    ,
    currentInput: {
        fontFamily: "Muli-Regular",
    },

})
