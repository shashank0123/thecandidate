import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, TouchableOpacity } from 'react-native'
import { Item, Input } from 'native-base'
import { commonStyle } from '../../../utils/Style/Style'
import Para from '../Text/Para'
// import {  } from 'react-native-gesture-handler'
import { fontFamily } from '../../../utils/FontFamily/FontFamily'
import dimension from '../../../utils/Dimension/Dimension';
import { url, mapUrl, placesParams, mapPlaceUrl, placesSelectedParams } from '../../../utils/BaseUrl/BaseUrl'
import axios from 'axios';
import Dimension from '../../../utils/Dimension/Dimension'

export default class SearchBar extends Component {

    state = {
        active: 'list',
        placesArray: []
    }

    componentDidMount = () => {
    }

    // componentWillReceiveProps = (prop) => {
    //     this.setState(prev => {
    //         return {
    //             ...prev,
    //             active: prop.active
    //         }
    //     });
    //     // if(this.inputRef){
    //     //     this.inputRef.focus();
    //     // }
    // }



    searchPlaces = async (val) => {
        let params = { ...{ input: val }, ...placesParams };
        try {
            let res = await axios.get(`${mapUrl}`, { params: params });
            if (res) {
                this.props.getPlaces(res['data']['predictions']);
            }
        }
        catch (err) {
            if (err) {
                console.log(err, err.response)
            }
        }
    }



    render() {
        return (
            <View style={styles.searchBar}
            >
                <Image source={require('../../../../assets/images/search.png')} style={[styles.searchImg, commonStyle.img]} />
                {
                    this.props.active === 'search' ?
                        <Item style={styles.item}

                        >

                            <Input placeholder="Search Here..." autoFocus={true} style={styles.input} onChangeText={this.searchPlaces} placeholderTextColor="#808080" />
                            {/* {
                                this.state.placesArray.length > 0 ?
                                    <View style={styles.places}>
                                        {
                                            this.state.placesArray.map(place => {
                                                return <TouchableOpacity style={styles.list} onPress={() => this.onSelectAddress(place)}>
                                                    <Para style={commonStyle.small}>{place.description}</Para>
                                                </TouchableOpacity>
                                            })
                                        }
                                    </View>
                                    : null
                            } */}
                            {/* <GooglePlacesAutocomplete
                                    placeholder='Search Here...'
                                    minLength={2} // minimum length of text to search
                                    autoFocus={true}
                                    returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                                    keyboardAppearance={'light'} // Can be left out for default keyboardAppearance https://facebook.github.io/react-native/docs/textinput.html#keyboardappearance
                                    listViewDisplayed={undefined}    // true/false/undefined
                                    fetchDetails={true}
                                    renderDescription={row => row.description} // custom description render
                                    onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                                        // this.onSelectAddress(details, data)
                                        console.log(data,details,'aaja')
                                    }}

                                    getDefaultValue={() => ''}

                                    query={{
                                        // available options: https://developers.google.com/places/web-service/autocomplete
                                        key: 'AIzaSyBxJQedWcV3yjCq9nUBe0FH9xVmMISqiiM',
                                        language: 'en', // language of the results
                                        types: '(cities)' // default: 'geocode'
                                    }}


                                    styles={
                                        {
                                            textInputContainer: {
                                                backgroundColor: 'rgba(0,0,0,0)',
                                                borderTopWidth: 0,
                                                borderBottomWidth: 0,
                                            },
                                            textInput: {
                                                marginLeft: 0,
                                                marginRight: 0,
                                                marginTop: 0,
                                                height: '100%',
                                                fontFamily: fontFamily.bold,
                                                color: '#5d5d5d',
                                            },
                                            predefinedPlacesDescription: {
                                                color: '#808080'
                                            },
                                            listView: {
                                                top: 55,
                                                left: -40,
                                                borderRadius: 5,
                                                position: 'absolute',
                                                // zIndex: 100000,
                                                fontFamily: fontFamily.regular,
                                                backgroundColor: 'white',
                                                width: dimension.width - 35,
                                            }
                                        }}
                                    ref={ref => this.inputRef = ref}
                                    // textInputProps={{
                                    //     ref: (textInput) => {
                                    //       setTimeout(() => textInput && textInput.focus(), 100);
                                    //     },
                                    //   }}
                                    currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
                                    currentLocationLabel="Current location"
                                    nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                                    GoogleReverseGeocodingQuery={{
                                        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                                    }}
                                    GooglePlacesSearchQuery={{
                                        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                        rankby: 'distance',
                                        // type: 'cafe'
                                    }}

                                    GooglePlacesDetailsQuery={{
                                        // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
                                        fields: 'formatted_address',
                                    }}
                                    enablePoweredByContainer={false}


                                    // filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} 
                                    // predefinedPlaces={[homePlace, workPlace]}

                                    debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
                                /> */}
                        </Item>
                        :
                        null
                }
                {
                    this.props.active === 'list' ?
                        <View style={styles.fakeBox}>
                            <TouchableOpacity style={styles.fakeView} onPress={() => this.props.showSearch('yes')}>
                                <Para bold={true} style={styles.fakeText}>Search Here...</Para>
                            </TouchableOpacity>
                        </View>
                        : null
                }
                {
                    this.props.filterData ?
                        <TouchableOpacity onPress={this.props.cancelFilter}><Image source={require('../../../../assets/images/cancel-filter.png')} style={[styles.cancelImg, commonStyle.img]} /></TouchableOpacity>
                        :
                        <TouchableOpacity onPress={this.props.filter}><Image source={require('../../../../assets/images/filter.png')} style={[styles.filterImg, commonStyle.img]} /></TouchableOpacity>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    searchBar: {
        backgroundColor: 'white',
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: "center",
        shadowColor: "#e0e0e0",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 1.25,
        shadowRadius: 3.84,

        elevation: 2,
        marginVertical: 15,
        paddingHorizontal: 15
    },
    item: {
        // backgroundColor:'red',
        flex: 1,
        borderColor: 'transparent',
        paddingHorizontal: 5,
        height: 45
    },
    input: {
        fontFamily: "CoreSansG-Medium"
    },
    searchImg: {
        height: 20,
        width: 20,
    },
    filterImg: {
        height: 20,
        width: 20,
    },
    cancelImg:{
        height: 17,
        width: 17,
    },
    fakeText: {
        color: '#808080',
    },
    fakeView: {
        flexGrow: 1,
        height: '100%',
        paddingHorizontal: 10,
        justifyContent: 'center',
        width: '100%'
    },
    fakeBox: {
        flex: 1,
        height: 45,
    },
    places: {
        position: 'absolute',
        top: 50,
        left: -30,
        width: dimension.width - 45,
        paddingHorizontal: 25,
        backgroundColor: 'white',
        zIndex: 1000000,
        borderRadius: 10,
        shadowColor: "#e0e0e0",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 1.25,
        shadowRadius: 3.84,

        elevation: 2,
    },
    list: {
        padding: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#e0e0e0',
        position: 'relative',
        zIndex: 100000
    }

})
