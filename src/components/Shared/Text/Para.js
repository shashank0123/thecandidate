import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'

export default class Para extends Component {
    render() {
        return (
            <View>
                {
                    this.props.extraBold ?
                        <Text style={[styles.extraBoldFont, this.props.style]}>{this.props.children}</Text>
                        :
                        <Text style={[this.props.bold ? styles.boldFont : styles.font, this.props.style]}>{this.props.children}</Text>

                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    font: {
        fontFamily: "Muli-Regular",
        fontSize: 16
    },
    boldFont: {
        fontFamily: "CoreSansG-Medium",
        fontSize: 16
    },
    extraBoldFont: {
        fontFamily: "CoreSansG-Bold",
    }
})
