import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import { Image } from 'react-native-animatable';
import { commonStyle } from '../../utils/Style/Style';
import { imageUrl } from '../../utils/BaseUrl/BaseUrl';
import WebView from 'react-native-webview';
import Dimension from '../../utils/Dimension/Dimension';

export default class ShowMap extends Component {

    state = {
    }

    componentWillReceiveProps = (prop) => {
        if (prop.zoomCoords) {
            let initialRegion = Object.assign({}, prop.zoomCoords);
            initialRegion["latitudeDelta"] = 0.005;
            initialRegion["longitudeDelta"] = 0.005;
            this.mapView.animateToRegion(initialRegion, 2000);
        }

        // console.log(prop)
    }


    render() {
        return (
            <View style={styles.mapDiv}>
                <MapView
                    provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                    style={styles.map}
                    moveOnMarkerPress={true}
                    ref={ref => (this.mapView = ref)}
                    region={this.props.zoomCoords}
                >
                    {
                        (this.props.markers && this.props.markers.length > 0) ?
                            this.props.markers.map(marker => (
                                <Marker
                                    coordinate={marker.latlng}
                                    title={marker.title}
                                    description={marker.description}
                                    onPress={() => this.props.markerClick(marker._id)}
                                    // image={marker._id == 'user' ? require('../../../assets/images/user.png') : require('../../../assets/images/pin.png')}
                                    key={marker._id}
                                >
                                    {
                                        marker._id == 'user' ?
                                            (this.props.userData && this.props.userData.profilePic) ?
                                                <Image source={{ uri: `${imageUrl}${this.props.userData.profilePic}` }} style={[{ width: 30, height: 30, borderRadius: 100 }, styles.profile]} />
                                                : <Image source={require('../../../assets/images/placeholder.png')} style={[{ width: 30, height: 30, borderRadius: 100 }, styles.profile]} />
                                            :
                                            marker._id == 'search' ?
                                                <Image source={require('../../../assets/images/searchpin.png')} style={[{ width: 30, height: 30 }, commonStyle.img]} />
                                                : <Image source={require('../../../assets/images/pin.png')} style={[{ width: 55, height: 55 }, commonStyle.img]} />
                                    }
                                </Marker>
                            ))
                            : null
                    }
                </MapView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mapDiv: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        ...StyleSheet.absoluteFillObject,
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        width: '100%',
        height: '100%',
    },
    profile: {
        borderWidth: 2.5,
        borderColor: '#1371EF'
    }
})
