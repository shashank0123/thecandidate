import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

export default class BookingSkeleton extends Component {
    render() {
        return (
            <SkeletonPlaceholder>
                <View style={{ backgroundColor: 'white', borderRadius: 5, padding: 20, marginVertical: 15 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View>
                            <View style={{ width: 120, height: 20, borderRadius: 4 }} />
                            <View
                                style={{ marginTop: 6, width: 80, height: 20, borderRadius: 4 }}
                            />
                            <View
                                style={{ marginTop: 6, width: 100, height: 20, borderRadius: 4 }}
                            />
                            <View
                                style={{ marginTop: 6, width: 140, height: 15, borderRadius: 4 }}
                            />
                        </View>
                        <View>
                            <View
                                style={{ marginTop: 6, width: 60, height: 60, borderRadius: 60 }}
                            />
                            <View style={{ width: 60, height: 10, borderRadius: 4, marginTop: 10 }} />
                        </View>
                    </View>
                </View>
            </SkeletonPlaceholder>
        )
    }
}

const styles = StyleSheet.create({})
