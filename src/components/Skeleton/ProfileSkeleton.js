import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

export default class ProfileSkeleton extends Component {
    render() {
        return (
            <SkeletonPlaceholder>
                <View>
                    <View style={{ width: '100%', height: 150, borderRadius: 4 }} />
                    <View style={{ alignItems: 'center' }}>
                        <View style={{ width: 100, height: 100, borderRadius: 20, borderWidth: 5, borderColor: 'white',position:'relative',top:-50 }} />
                    </View>
                    <View style={{ paddingHorizontal:30 }}>
                        <View style={{ width: '100%', height: 55, borderRadius: 10,marginBottom:20 }} />
                        <View style={{ width: '100%', height: 55, borderRadius: 10,marginBottom:20 }} />
                        <View style={{ width: '100%', height: 55, borderRadius: 10,marginBottom:20 }} />
                        <View style={{ width: '100%', height: 55, borderRadius: 10,marginBottom:20 }} />
                    </View>
                </View>
            </SkeletonPlaceholder>
        )
    }
}

const styles = StyleSheet.create({})
