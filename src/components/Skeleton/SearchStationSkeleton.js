import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

export default class SearchStationSkeleton extends Component {
    render() {
        return (
            <SkeletonPlaceholder>
                <View style={{ paddingVertical: 20, paddingHorizontal: 30 }}>
                    <View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ width: 120, height: 15, borderRadius: 5 }} />
                            <View style={{ width: 50, height: 15, borderRadius: 5, marginLeft: 20 }} />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                            <View style={{ width: 70, height: 15, borderRadius: 5 }} />
                            <View style={{ width: 50, height: 15, borderRadius: 5, marginLeft: 20 }} />
                        </View>
                    </View>
                    <View style={{ width: '100%', height: 2, borderRadius: 5, marginTop: 20 }} />
                    <View style={{ marginTop: 20 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ width: 80, height: 15, borderRadius: 5 }} />
                            <View style={{ width: 50, height: 15, borderRadius: 5, marginLeft: 20 }} />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                            <View style={{ width: 60, height: 15, borderRadius: 5 }} />
                            <View style={{ width: 50, height: 15, borderRadius: 5, marginLeft: 20 }} />
                        </View>
                    </View>
                    <View style={{ width: '100%', height: 2, borderRadius: 5, marginTop: 20 }} />
                    <View style={{ marginTop: 20 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ width: 80, height: 15, borderRadius: 5 }} />
                            <View style={{ width: 50, height: 15, borderRadius: 5, marginLeft: 20 }} />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                            <View style={{ width: 60, height: 15, borderRadius: 5 }} />
                            <View style={{ width: 50, height: 15, borderRadius: 5, marginLeft: 20 }} />
                        </View>
                    </View>
                    <View style={{ width: '100%', height: 2, borderRadius: 5, marginTop: 20 }} />
                    <View style={{ marginTop: 20 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ width: 80, height: 15, borderRadius: 5 }} />
                            <View style={{ width: 50, height: 15, borderRadius: 5, marginLeft: 20 }} />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                            <View style={{ width: 60, height: 15, borderRadius: 5 }} />
                            <View style={{ width: 50, height: 15, borderRadius: 5, marginLeft: 20 }} />
                        </View>
                    </View>
                    <View style={{ width: '100%', height: 2, borderRadius: 5, marginTop: 20 }} />
                    <View style={{ marginTop: 20 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ width: 80, height: 15, borderRadius: 5 }} />
                            <View style={{ width: 50, height: 15, borderRadius: 5, marginLeft: 20 }} />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                            <View style={{ width: 60, height: 15, borderRadius: 5 }} />
                            <View style={{ width: 50, height: 15, borderRadius: 5, marginLeft: 20 }} />
                        </View>
                    </View>
                    <View style={{ width: '100%', height: 2, borderRadius: 5, marginTop: 20 }} />
                    <View style={{ marginTop: 20 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ width: 80, height: 15, borderRadius: 5 }} />
                            <View style={{ width: 50, height: 15, borderRadius: 5, marginLeft: 20 }} />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                            <View style={{ width: 60, height: 15, borderRadius: 5 }} />
                            <View style={{ width: 50, height: 15, borderRadius: 5, marginLeft: 20 }} />
                        </View>
                    </View>
                </View>
            </SkeletonPlaceholder>
        )
    }
}

const styles = StyleSheet.create({})
