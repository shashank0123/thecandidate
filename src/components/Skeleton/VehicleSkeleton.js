import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

export default class VehicleSkeleton extends Component {
    render() {
        return (
            <SkeletonPlaceholder>
                <View style={{ padding: 30 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ width: 50, height: 20, borderRadius: 5 }} />
                        <View style={{ width: 50, height: 20, borderRadius: 5 }} />
                    </View>
                    <View style={{ alignItems: 'center', marginTop: 20 }}>
                        <View style={{ width: 180, height: 150, borderRadius: 20 }} />
                    </View>
                    <View style={{ paddingHorizontal: 30,marginTop:30 }}>
                        <View style={{ flexDirection: 'row', justifyContent:'center',marginTop:20 }}>
                            <View style={{ width: 100, height: 20, borderRadius: 5 }} />
                            <View style={{ width: 100, height: 20, borderRadius: 5,marginLeft:20 }} />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent:'center',marginTop:20 }}>
                            <View style={{ width: 100, height: 20, borderRadius: 5 }} />
                            <View style={{ width: 100, height: 20, borderRadius: 5,marginLeft:20 }} />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent:'center',marginTop:20 }}>
                            <View style={{ width: 100, height: 20, borderRadius: 5 }} />
                            <View style={{ width: 100, height: 20, borderRadius: 5,marginLeft:20 }} />
                        </View>
                    </View>
                </View>
            </SkeletonPlaceholder>
        )
    }
}

const styles = StyleSheet.create({})
