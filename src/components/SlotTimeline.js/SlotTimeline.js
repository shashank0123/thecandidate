'use strict';

import React, { Component } from 'react'
import { View, ScrollView, Text, StyleSheet, Animated } from 'react-native'
import TimeTest from '../TimeTest/TimeTest';
import moment from 'moment';

export default class SlotTimeline extends Component {

    state = {
        timeSlots: []
    }

    _handleGaugeChange = (val) => {
        // console.log(val,'change gauge');
        this.props.chageStartingTime(this.state.timeSlots[val])
    }

    componentDidMount = () => {
        this.slotChanges(this.props.timelineSlots)
    }

    componentWillReceiveProps = (nprops) => {
        // console.log(moment(this.props.timelineSlots[0].slotStart).format('hh:mm a'), 'timeslots')
        this.slotChanges(nprops.timelineSlots);
    }

    slotChanges = (slots) => {
        let newSlots = [];
        newSlots = [...slots];
        let end = moment(slots[slots.length - 1].slotEnd).format('HH:mm');
        let start = moment(slots[slots.length - 1].slotStart).format('HH:mm');
        let timeSlots = newSlots.map((time, index) => {
            return {
                // time: moment(time.slotStart,'YYYY-MM-DD HH:mm').format('hh:mm a'),
                time: moment(time.slotStart).format('hh:mm a'),
                index: index,
                booked: time.booked
            }
        });

        timeSlots = this.addMoreSlots(start, end, timeSlots, slots[slots.length - 1].booked);
        console.log(timeSlots,'slots')


        // console.log(timeSlots, 'timeslots>>>>')


        this.setState(prev => {
            return {
                ...prev,
                timeSlots: timeSlots
            }
        })
    }

    addMoreSlots = (start, end, data, booked) => {
        let startofDay = start;
        while (startofDay != end) {
            startofDay = moment(startofDay, 'HH:mm').add(15, 'minutes').format("HH:mm");
            data.push({
                time: moment(startofDay, 'HH:mm').format('hh:mm a'),
                booked: booked,
                index: data.length
            })
        }
        return data;
    }

    render() {

        return (
            <View>
                {
                    this.state.timeSlots.length > 0 ?
                        <TimeTest
                            min={0}
                            max={this.state.timeSlots.length}
                            largeInterval={4}
                            slots={this.state.timeSlots}
                            value={0}
                            slotTime={this.props.slotTime}
                            onChange={this._handleGaugeChange}
                            startingTime={this.props.startingTime}
                        >
                        </TimeTest>
                        : null
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({

});
