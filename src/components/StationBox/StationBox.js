import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import Para from '../Shared/Text/Para'
import { commonStyle } from '../../utils/Style/Style'
import { TouchableOpacity } from 'react-native-gesture-handler'

export default class StationBox extends Component {
    render() {
        return (
            <TouchableOpacity style={[(this.props.in == this.props.length) ? styles.noBorder : styles.singleStation]} onPress={() => this.props.singleStation(this.props.station,true)}>
                {
                    this.props.station ?
                        <View>
                            <View style={styles.distance}>
                                <Para style={commonStyle.small}>{this.props.station.name}</Para>
                                {
                                    this.props.station.dist ?
                                        <Para style={[styles.otherText, commonStyle.small]}>{Math.floor(this.props.station.dist.calculated / 1000)}km</Para>
                                        : null
                                }
                            </View>
                            <View style={styles.distance}>
                                <Para style={[styles.timeText, commonStyle.small]}>Next Slot Available at 05:30 pm</Para>
                                <Para style={[styles.otherText, commonStyle.small]}>Away</Para>
                            </View>
                        </View>
                        : null
                }

            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    singleStation: {
        borderBottomWidth: 1,
        borderBottomColor: '#e0e0e0',
        paddingHorizontal: 10,
        paddingVertical: 15
    },
    distance: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 2
    },
    timeText: {
        color: '#808080'
    },
    otherText: {
        color: '#A9A9A9'
    },
    noBorder: {
        borderColor: 'transparent',
        borderWidth: 0,
        paddingHorizontal: 10,
        paddingVertical: 15
    }
})
