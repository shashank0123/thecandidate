import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, Linking, Platform, TouchableOpacity, DeviceEventEmitter, NativeEventEmitter, NativeModules, Keyboard } from 'react-native'
import { commonStyle } from '../../utils/Style/Style'
import Para from '../Shared/Text/Para'
import { ScrollView } from 'react-native-gesture-handler'
import { Button, Col, Grid, Item, Picker, Icon, Spinner, Root, Container } from 'native-base'
import { fontFamily } from '../../utils/FontFamily/FontFamily'
import moment from 'moment';
import { addressFormat } from '../../utils/AddressFormat/AddressFormat'
import { timeArray, slotTimeArrays } from '../../utils/Time/Time'
import validate from '../../utils/Validation/Validation'
import axios from 'axios';
import { url } from '../../utils/BaseUrl/BaseUrl'
import { Timeline } from '../Timeline/Timeline'
import { msg, errMsg } from '../../utils/Toaster/Toaster'
import Modal from 'react-native-modal';
import PromoCode from '../PromoCode/PromoCode'
import Dimension from '../../utils/Dimension/Dimension'
import SlotTimeline from '../SlotTimeline.js/SlotTimeline'
import BreakupCharges from '../BreakupCharges/BreakupCharges'
import PaymentFailed from '../PaymentFailed/PaymentFailed'
import StationImages from '../StationsImages/StationImages'
import WebView from 'react-native-webview'
var sliderStarting = null;
// import Paytm from '@philly25/react-native-paytm';

// Daat received from PayTM
const paytmConfig = {
    MID: 'hClrxN77120745465019',
    WEBSITE: 'APPSTAGING',
    // WEBSITE: 'chargebizz.com',
    CHANNEL_ID: 'WAP',
    INDUSTRY_TYPE_ID: 'Retail',
    CALLBACK_URL: 'https://securegw.paytm.in/theia/paytmCallback?ORDER_ID='
}


export default class StationDetails extends Component {
    state = {
        datesArray: [],
        timesArray: [],
        stationDetails: this.props.station,
        slotArray: [],
        timelineSlots: [],
        powersArray: [],
        pinsArray: [],
        disablepowersArray: [],
        disablepinsArray: [],
        combinations: [],
        price: 0,
        timeSlotValues: [0, 1],
        promoModalVisible: false,
        breakupCharges: false,
        charges: null,
        paytmTxnToken: null,
        loading: false,
        promo: null,
        failModal: false,
        paymentErrMsg: null,
        paymentData: null,
        failedCount: 0,
        showImageModal: false,
        timeLoading: false,

        // sliderStarting: null,
        controls: {
            date: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                    required: true,
                }
            },
            startingTime: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                    required: true,
                }
            },
            slotTime: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                    required: true,
                }
            },
            power: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                }
            },
            pin: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                }
            },
        }
    }

    componentDidMount = () => {
        this.getStationDetails();
        // console.log(this.state.stationDetails)
    }


    getStationDetails = async () => {
        try {
            let res = await axios.get(`${url}/station/${this.state.stationDetails._id}`)
            if (res) {
                console.log(res['data']['data'], 'station details>>>>>>>>')
                this.setState(prev => {
                    return {
                        ...prev,
                        stationDetails: res['data']['data']
                    }
                });
                this.getPowerPin();
            }
        }
        catch (err) {
            if (err) {
                console.log(err)
            }
        }
    }


    componentWillMount() {

        if (Platform.OS == 'ios') {
            // this.onPayTmResponseIOS();
        }
        else {
            DeviceEventEmitter.addListener('PayTMResponse', this.onPayTmResponse);
            DeviceEventEmitter.addListener('PayTMResponseApp', this.onPayTmResponseApp);
        }
    }

    componentWillUnmount() {

        if (Platform.OS == 'ios') {
            // this.onPayIOSRemoveListner();
        }
        else {
            DeviceEventEmitter.removeListener('PayTMResponse', this.onPayTmResponse);
            DeviceEventEmitter.removeListener('PayTMResponseApp', this.onPayTmResponseApp);
        }
    }

    getPowerPin = async () => {
        try {
            let res = await axios.get(`${url}/common/meta`)
            if (res) {
                // console.log(res['data']['data'])
                this.setState(prev => {
                    return {
                        ...prev,
                        powersArray: res['data']['data']['powers'],
                        pinsArray: res['data']['data']['pin'],
                        controls: {
                            ...prev.controls,

                            // power: {
                            //     ...prev.controls.power,
                            //     value: res['data']['data']['powers'][0]
                            // },
                            // pin: {
                            //     ...prev.controls.pin,
                            //     value: res['data']['data']['pin'][0]
                            // }
                        }
                    }
                });

                // console.log('yha p kya h ', this.state.stationDetails)
                if (this.state.stationDetails && this.state.stationDetails['machines']) {
                    this.checkPowerPin(this.state.stationDetails['machines'], res['data']['data']['powers'], res['data']['data']['pin']);
                }
                // this.getDates();
            }
        }
        catch (err) {
            if (err) {
                console.log(err)
            }
        }
    }

    getDates = (power, pin) => {
        // console.log('hanji date m aaya >>>>>')
        let currentDate = moment(new Date());
        let dates = [{ date: currentDate, label: 'Today' }];
        for (let i = 0; i < 6; i++) {
            let newDate = moment(currentDate, "DD-MM-YYYY").add(1, 'days');
            currentDate = newDate;
            dates.push({ date: newDate, label: newDate.format('DD') });
            // console.log(newDate.format('DD'));
        }
        // console.log(times)
        let slotTimeArray = slotTimeArrays();
        let mainTime = this.getCurrentTime();
        let validDate;
        if (moment().format('HH:mm') > moment('23:45', 'HH:mm').format('HH:mm')) {
            validDate = dates[1].date;
        }
        else {
            validDate = dates[0].date;
        }
        let times = timeArray();
        sliderStarting = mainTime;

        console.log(mainTime, 'maintime???????')
        this.setState(prev => {
            return {
                ...prev,
                datesArray: dates,
                timesArray: times,
                slotArray: slotTimeArray,
                controls: {
                    ...prev.controls,
                    date: {
                        ...prev.controls.date,
                        value: validDate
                    },
                    startingTime: {
                        ...prev.controls.startingTime,
                        value: mainTime
                    },
                    slotTime: {
                        ...prev.controls.slotTime,
                        value: slotTimeArray[0].value
                    }
                }
            }
        })

        if (this.props.booking) {
            // console.log('hi booking ?????', booking)
            let booking = this.props.booking;

            // let bookingStartTime = moment(booking.startTime, 'YYYY-MM-DD HH:mm').format('HH:mm');
            let bookingStartTime = moment(new Date(booking.startTime)).format('HH:mm');
            let mainTime = this.getCurrentTime();
            if (mainTime > bookingStartTime) {
                bookingStartTime = mainTime;
            }
            // console.log(booking.startTime, bookingStartTime, 'booking starting time>>>>>>>')
            this.setState(prev => {
                return {
                    ...prev,
                    controls: {
                        ...prev.controls,
                        // date: {
                        //     ...prev.controls.date,
                        //     value: dates[0].date
                        // },
                        startingTime: {
                            ...prev.controls.startingTime,
                            value: bookingStartTime
                        },
                        slotTime: {
                            ...prev.controls.slotTime,
                            value: booking.minutes
                        },
                        power: {
                            ...prev.controls.power,
                            value: booking.machine.power
                        },
                        pin: {
                            ...prev.controls.pin,
                            value: booking.machine.pin
                        }
                    }
                }
            })

            this.getTimeSlot(dates[0].date, bookingStartTime, booking.minutes, booking.machine.power, booking.machine.pin);
            this.getPricing(booking.minutes, booking.machine.power);
        }
        else {
            this.getTimeSlot(dates[0].date, mainTime, slotTimeArray[0].value, power, pin, true);
            this.getPricing(slotTimeArray[0].value, power);
        }
    }


    getTimeSlot = async (date, startingTime, slotTime, power, pin, val) => {
        this.timelineLoading(true);
        let dateTime = this.mergeBoth(date, startingTime)
        // console.log(dateTime.format(),'mainTime')

        let params = {
            date: dateTime,
            slotTime: slotTime,
            station: this.state.stationDetails._id,
            power: power,
            pin: pin
        }
        try {
            let res = await axios.get(`${url}/booking/slots`, { params: params });
            if (res) {
                this.timelineLoading(false);
                // console.log(res['data']['data'], 'slots new')
                if (val) {
                    this.goToAvailableSlot(res['data']['data']['slots']);
                }
                this.setState(prev => {
                    return {
                        ...prev,
                        stationDetails: res['data']['data']['station'],
                        timelineSlots: res['data']['data']['slots'],
                        // price: res['data']['data']['price']
                    }
                });

            }
        }
        catch (err) {
            if (err) {
                this.timelineLoading(false);
                console.log(err)
            }
        }
    }

    timelineLoading = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                timeLoading: val
            }
        })
    }

    goToAvailableSlot = (slots) => {
        // console.log(slots);
        for (let i = 0; i < slots.length; i++) {
            if (!slots[i].booked) {
                let startTime = moment(new Date(slots[i].slotStart)).format('HH:mm');
                // console.log(startTime, 'sdafdasf?????')
                this.setState(prev => {
                    return {
                        ...prev,
                        controls: {
                            ...prev.controls,
                            startingTime: {
                                ...prev.controls.startingTime,
                                value: startTime
                            }
                        }
                    }
                })
                break;
            }
        }
    }

    getPricing = async (slotTime, power, promo) => {
        let params = {
            slotTime: slotTime,
            power: power,
        }
        if (promo) {
            params.promocode = promo
        }
        try {
            let res = await axios.get(`${url}/booking/pricing`, { params: params });
            if (res) {
                // console.log(res.data.data, 'charges>>>>>>')
                this.promoModal(false);
                this.setState(prev => {
                    return {
                        ...prev,
                        price: res['data']['data']['total'],
                        charges: res['data']['data'],
                        // promo: promo
                    }
                });
            }
        }
        catch (err) {
            if (err) {
                errMsg(err.response);
            }
        }
    }

    getCurrentTime = (date) => {
        let currentTime = moment().format('HH:mm');
        let currentMin = currentTime.split(':')[1];
        // console.log(currentMin, currentTime)
        let mainTime;
        if (currentMin >= 0 && currentMin < 15) {
            mainTime = (currentTime.split(':')[0] + ':' + 15);
        }
        else if (currentMin >= 15 && currentMin < 30) {
            mainTime = (currentTime.split(':')[0] + ':' + 30);

        }
        else if (currentMin >= 30 && currentMin < 45) {
            mainTime = (currentTime.split(':')[0] + ':' + 45);

        }
        else if (currentMin >= 45) {
            mainTime = ((Number(currentTime.split(':')[0]) + 1) + ':' + '00');
        }


        return mainTime;
    }

    checkPowerPin = (machines, powersState, pinsState) => {
        let pins = [...pinsState];
        let powers = [...powersState];
        let disablePins = [];
        let disablePowers = [];
        let combinations = [];
        // console.log(machines, 'machines')
        machines.forEach(machine => {
            let index = combinations.findIndex(comb => {
                return (comb.power == machine.power)
            });

            if (index == -1) {
                combinations.push({ power: machine.power, pin: [machine.pin] });
            }
            else {
                if (combinations[index].pin.indexOf(machine.pin) == -1) {
                    combinations[index].pin.push(machine.pin);
                }
            }
        });

        pinsState.forEach((pin, pIndex) => {
            let pinIndex = machines.findIndex(machine => {
                return machine.pin == pin;
            });
            if (pinIndex == -1) {
                disablePins.push(pinsState[pIndex]);
                pins.splice(pIndex, 1);
            }
        })

        powersState.forEach((power, pIndex) => {
            let powerIndex = machines.findIndex(machine => {
                return machine.power == power;
            });
            if (powerIndex == -1) {
                disablePowers.push(powersState[pIndex]);
                powers.splice(pIndex, 1);
            }
        })

        let validPins = [];
        let validPinIndex = combinations.findIndex(comb => {
            return comb.power == powers[0];
        });

        if (validPinIndex >= 0) {
            validPins = combinations[validPinIndex].pin;
        }

        console.log('<<<<check man', disablePins, combinations)



        this.getDates(powers[0], validPins[0]);

        this.setState(prev => {
            return {
                ...prev,
                disablepinsArray: disablePins,
                disablepowersArray: disablePowers,
                combinations: combinations,
                pinsArray: validPins,
                controls: {
                    ...prev.controls,

                    power: {
                        ...prev.controls.power,
                        value: powers[0]
                    },
                    pin: {
                        ...prev.controls.pin,
                        value: validPins[0]
                    }
                }
            }
        });

    }

    openMap(geolocation) {
        if (geolocation.coordinates) {
            // console.log('open directions', geolocation.coordinates);
            if (Platform.OS == 'android') {
                Linking.openURL(`http://maps.google.com/maps?daddr=${geolocation.coordinates[1]},${geolocation.coordinates[0]}`);
            }
            else {
                Linking.openURL(`http://maps.apple.com/maps?daddr=${geolocation.coordinates[1]},${geolocation.coordinates[0]}`);
            }

        }

    }

    changeHandler = (event, key) => {
        if (key == 'date') {
            let newTimes;
            if (moment(event).format('DD-MM-YY') != moment(this.state.datesArray[0].date).format('DD-MM-YY')) {
                newTimes = timeArray();
                sliderStarting = newTimes[0].value;
            }
            else {
                sliderStarting = this.getCurrentTime();

                if (moment().format('HH:mm') > moment('23:45', 'HH:mm').format('HH:mm')) {
                    errMsg("Please select next date to book any slot.");
                }
                newTimes = timeArray(sliderStarting);
            }

            this.setState(prevState => {
                return {
                    ...prevState,
                    // timesArray: newTimes,
                    controls: {
                        ...prevState.controls,
                        startingTime: {
                            ...prevState.controls.startingTime,
                            value: sliderStarting,
                            touched: true,
                            valid: validate(event, prevState.controls.startingTime.validate, key)
                        }
                    }
                };
            });
        }

        let check = true;

        if (key == 'power' || key == 'pin') {
            check = this.checkDisablity(key, event);
            console.log(check, 'check failed')
        }

        if (key == 'slotTime') {
            // let startTime = this.state.controls.startingTime.value;
            // startTime = moment(startTime, 'HH:mm').add(event, 'minutes').format("HH:mm");
            // console.log(startTime, this.state.controls.startingTime.value)
            // if (startTime > this.state.controls.startingTime.value) {
            //     check = true;
            // }
            // else {
            //     check = false;
            //     errMsg('Please select different date for this slot');
            // }

            // console.log(sliderStarting, 'change slot')
            this.setState(prevState => {
                return {
                    controls: {
                        ...prevState.controls,
                        startingTime: {
                            ...prevState.controls.startingTime,
                            value: sliderStarting,
                        }
                    }
                };
            });
        }

        let newVal;

        if (check) {
            newVal = event;
            this.setState(prevState => {
                return {
                    controls: {
                        ...prevState.controls,
                        [key]: {
                            ...prevState.controls[key],
                            value: event,
                            touched: true,
                            valid: validate(event, prevState.controls[key].validate, key)
                        }
                    }
                };
            });


        }
        else {
            newVal = this.state.controls[key].value;
        }

        if (key != 'startingTime') {
            let params = {};
            let data = { ...this.state.controls }
            for (let control of Object.keys(data)) {
                params[control] = this.state.controls[control].value;
            }
            // console.log(params);
            params[key] = newVal;

            if (key == 'date') {
                params.startingTime = sliderStarting;
            }

            if (key == 'power' && check) {
                params.pin = check;
            }

            if (key == 'slotTime') {
                if (moment(params.date).format('DD-MM-YY') != moment(this.state.datesArray[0].date).format('DD-MM-YY')) {
                    params.startingTime = this.state.timesArray[0].value;
                    // sliderStarting = newTimes[0].value;
                }
                else {
                    params.startingTime = this.getCurrentTime();
                    // sliderStarting = this.getCurrentTime();
                }
            }

            // console.log(params, 'yha p kya ja rha h')
            this.getTimeSlot(params.date, params.startingTime, params.slotTime, params.power, params.pin);
            this.getPricing(params.slotTime, params.power);
        }
        if (key == 'startingTime') {
            let isPastTime = this.checkPastTime(event);
            if (!isPastTime) {
                sliderStarting = event;
            }
            else {
                errMsg("It's too late to select this time.");
                this.setState(prevState => {
                    return {
                        controls: {
                            ...prevState.controls,
                            startingTime: {
                                ...prevState.controls.startingTime,
                                value: sliderStarting,
                            }
                        }
                    };
                });
            }
            // sliderStarting = event;
        }

    };

    checkPastTime = (time) => {
        if (moment(this.state.controls.date.value).format('DD-MM-YY') == moment(this.state.datesArray[0].date).format('DD-MM-YY')) {
            let currentTime = this.getCurrentTime()
            // console.log(currentTime, time, 'check time');
            // console.log(currentTime, time, 'yhi h time')
            if (time < currentTime) {
                this.setState(prev => {
                    return {
                        ...prev,
                        controls: {
                            ...prev.controls,
                            startingTime: {
                                ...prev.controls.startingTime,
                                value: sliderStarting,
                            }
                        }
                    }
                })
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    checkDisablity = (key, value) => {
        let array;
        if (key == 'power') {
            array = this.state.disablepowersArray;
        }
        else {
            array = this.state.disablepinsArray;
        }
        // console.log(array, value, 'check disable')
        if (array.indexOf(value) >= 0) {
            errMsg('Machine is not available with this ' + key);
            return false;
        }
        else {
            if (key == 'power') {
                let pinIndex = this.state.combinations.findIndex(comb => {
                    return comb.power == value;
                })

                if (pinIndex >= 0) {
                    this.setState(prev => {
                        return {
                            ...prev,
                            pinsArray: this.state.combinations[pinIndex].pin,
                            controls: {
                                ...prev.controls,
                                pin: {
                                    ...prev.controls.pin,
                                    value: this.state.combinations[pinIndex].pin[0],
                                }
                            }
                        }
                    })

                    return this.state.combinations[pinIndex].pin[0];
                }
            }
            else {
                return true;
            }
        }
    }

    // multiSliderValueCallback = (values) => {
    //     console.log(this.state.timesArray[values[0]].value, this.state.controls.startingTime.value,'slider callback')
    //     if (this.state.timesArray[values[0]].value != this.state.controls.startingTime.value) {
    //         this.setState(prev => {
    //             return {
    //                 ...prev,
    //                 timeSlotValues: values,
    //                 controls: {
    //                     ...prev.controls,
    //                     startingTime: {
    //                         ...prev.controls.startingTime,
    //                         value: prev.timesArray[values[0]].value
    //                     }
    //                 }
    //             }
    //         })
    //     }
    // }


    mergeBoth = (date, startingTime) => {
        // console.log(date, startingTime)
        let time = startingTime.split(':')
        let dateTime = moment(date).set({ h: time[0], m: time[1], s: '00' });
        // return moment(dateTime).format('YYYY-MM-DD HH:mm');
        // console.log('Time =>', new Date(dateTime), 'ISO Time =>', new Date(dateTime).toISOString())
        return new Date(dateTime).toISOString()
    }

    bookSlot = async () => {

        this.showLoading(true);
        let slotAvailable = this.checkSlotAvailability(this.state.controls);
        if (slotAvailable) {
            let params = {};
            let controls = { ...this.state.controls }
            for (let control of Object.keys(controls)) {
                params[control] = this.state.controls[control].value;
            }
            if (sliderStarting) {
                params.startingTime = sliderStarting;
            }
            params.date = this.mergeBoth(params.date, params.startingTime);
            delete params.startingTime;
            params.station = this.state.stationDetails._id;
            try {
                let res = await axios.post(`${url}/booking`, params);
                if (res) {
                    setTimeout(() => {
                        this.showLoading(false);
                    }, 2000)
                    this.initiatePayment(res['data']['data'])
                }
            }
            catch (err) {
                if (err) {
                    this.showLoading(false);
                    errMsg(err.response);
                }
            }
        }
        else {
            errMsg('Slot is already booked')
        }
    }

    showLoading = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                loading: val
            }
        })
    }

    initiatePayment = (data) => {
        // console.log(data, 'data >>>>')
        let { paytmTxnToken, total, user, _id } = data;
        this.setState(prev => {
            return {
                ...prev,
                paymentData: data
            }
        })
        let callbackUrl = `${paytmConfig.CALLBACK_URL}${String(_id)}`;
        // console.log(_id, paytmTxnToken, 'ye h token >>>>>>>>>>')
        if (Platform.OS == 'android') {
            NativeModules.Paytm.startPayment(String(_id), paytmConfig.MID, paytmTxnToken, String(total), callbackUrl)
        }
        else {
            console.log('ios call>>>', typeof String(_id), typeof paytmConfig.MID, typeof String(paytmTxnToken), typeof String(total), typeof callbackUrl)
            NativeModules.PaytmSwift.openPayment(String(_id), String(paytmConfig.MID), String(paytmTxnToken), String(total), String(callbackUrl))
        }

        // let details = {
        //     MID: paytmConfig.MID,
        //     INDUSTRY_TYPE_ID: paytmConfig.INDUSTRY_TYPE_ID,
        //     WEBSITE: paytmConfig.WEBSITE,
        //     CHANNEL_ID: paytmConfig.CHANNEL_ID,
        //     TXN_AMOUNT: `${total}`, // String
        //     ORDER_ID: String(_id), // String
        //     CUST_ID: String(user), // String
        //     CALLBACK_URL: callbackUrl,
        // };

        // let checksumData = {
        //     "requestType": "Payment",
        //     "mid": paytmConfig.MID,
        //     "websiteName": "chargebizz.com",
        //     "orderId": String(_id),
        //     "txnAmount": {
        //         "value": String(total),
        //         "currency": "INR"
        //     },
        //     "userInfo": {
        //         "custId": String(user)
        //     }
        // }

        // // console.log({ body: checksumData })
        // try {
        //     let res = await axios.post(`${url}/booking/generate-checksum`, checksumData);
        //     if (res) {
        //         let checksum = res['data']['data']['checksum'];
        //         this.runTransaction(total, user, String(_id), '9319434229', 'umeshdhauni@gmail.com', checksum);
        //     }
        // }
        // catch (err) {
        //     if (err) {
        //         errMsg(err.response);
        //     }
        // }

    }

    onPayTmResponse = (resp) => {
        // console.log(JSON.stringify(resp) + 'resp>>>>>>>>>>>>>>>>>>>.')

        // if (resp.STATUS == 'TXN_SUCCESS') {
        //     this.props.navigateSuccess('BookSuccess', { success: true, msg: 'Your Payment is done and Booking is created successfully' });
        // }
        if (resp.status == 'CANCEL' || resp.RESPCODE == '141') {
            // this.showFailedModal(true, 'You have cancelled the transaction.');
            // errMsg('You have cancelled the transaction.')
            if (this.state.failedCount <= 2) {
                this.paymentAttemptedFailed('You have cancelled the transaction.');
            }
        }
        else {
            console.log(resp);
            if (resp.STATUS == 'TXN_FAILURE') {
                if (this.state.failedCount <= 2) {
                    this.paymentAttemptedFailed(resp.RESPMSG);
                }
            }
            else {
                this.checkPaymentStatus(resp.ORDERID, '', this.state.paymentData.paytmTxnToken);
            }
            // this.props.navigateSuccess('BookSuccess', { success: false, msg: resp.status ? resp.status : 'Your payment is failed. Please contact your bank for any queries. If money has been deducted from your account, your bank will inform us within 48 hrs and we will refund the same' });
        }
    };

    onPayTmResponseApp = (resp) => {
        console.log(resp, '>>>>>>>')
        if (resp && resp.event) {
            let response = JSON.parse(resp.event);
            console.log(response);
            if (response.STATUS == 'TXN_FAILURE') {
                if (this.state.failedCount <= 2) {
                    this.paymentAttemptedFailed(response.RESPMSG);
                }
            }
            else {
                this.checkPaymentStatus(response.ORDERID, response.errorMessage, this.state.paymentData.paytmTxnToken);
            }
        }
        else {
            if (this.state.failedCount <= 2) {
                this.paymentAttemptedFailed('You have cancelled the transaction.');
            }
            // errMsg('You transation is cancelled.')
        }
    }


    onPayTmResponseIOS = () => {
        const { PaytmSwift } = NativeModules;
        const eventEmitter = new NativeEventEmitter(PaytmSwift);

        eventEmitter.addListener('PayTMResponseApp', (data) => {
            console.log(data, 'event data')
        });
    }

    onPayIOSRemoveListner = () => {
        const { PaytmSwift } = NativeModules;
        const eventEmitter = new NativeEventEmitter(PaytmSwift);
        eventEmitter.removeListener('PayTMResponseApp');
    }

    paymentAttemptedFailed = async (msg) => {
        console.log('hiii', this.state.paymentData._id);
        try {
            let res = await axios.put(`${url}/booking/payment-attempt-fail/${this.state.paymentData._id}`);
            if (res) {
                console.log('hiii', res);
                this.showFailedModal(true, msg);
                this.setState(prev => {
                    return {
                        ...prev,
                        failedCount: prev.failedCount + 1
                    }
                })
            }
        }
        catch (err) {
            if (err) {
                console.log('hiii', err, err.response);
            }
        }
    }
    paymentFailed = async () => {
        try {
            let res = await axios.put(`${url}/booking/payment-fail/${this.state.paymentData._id}`);
            if (res) {
                console.log('res', res)
                this.showFailedModal(false, null);
                this.setState(prev => {
                    return {
                        ...prev,
                        failedCount: 0,
                        // paymentData: null
                    }
                })
                this.props.bookingDetails(this.state.paymentData._id);
            }
        }
        catch (err) {
            if (err) {

            }
        }
    }

    showFailedModal = (val, msg) => {
        this.setState(prev => {
            return {
                ...prev,
                failModal: val,
                paymentErrMsg: msg
            }
        })
    }



    checkPaymentStatus = async (orderId, errMsg, token) => {
        let data = {
            paytmTxnToken: token
        }
        try {
            let res = await axios.put(`${url}/booking/payment-done/${orderId}`, data);
            if (res) {
                // console.log(res, 'payemnt status api>>>>>')
                this.props.navigateSuccess('BookSuccess', { success: true, msg: 'Your Payment is done and Booking is created successfully' });
            }
        }
        catch (err) {
            if (err) {
                // errMsg(err.response);
                // console.log('<<<<<<<', err.response['data'], 'payemnt status fail api >>>>>>')
                this.props.navigateSuccess('BookSuccess', { success: false, msg: errMsg ? errMsg : 'Your payment is failed. Please contact your bank for any queries. If money has been deducted from your account, your bank will inform us within 48 hrs and we will refund the same', station: this.state.stationDetails._id });
            }
        }
    }

    tryAgain = () => {
        this.showFailedModal(false, null);
        this.initiatePayment(this.state.paymentData);
        // try {
        //     let res = await axios.put(`${url}/booking/payment-done/${orderId}`);
        //     if (res) {

        //     }
        // }
        // catch (err) {
        //     if (err) {

        //     }
        // }
    }


    // failedPayment =() =>{
    //     this.paymentFailed();
    // }


    // runTransaction(amount, customerId, orderId, mobile, email, checkSum) {
    //     try {
    //         const callbackUrl = `${paytmConfig.CALLBACK_URL}${orderId}`;
    //         const details = {
    //             mode: 'Staging', // 'Staging' or 'Production'
    //             MID: paytmConfig.MID,
    //             INDUSTRY_TYPE_ID: paytmConfig.INDUSTRY_TYPE_ID,
    //             WEBSITE: paytmConfig.WEBSITE,
    //             CHANNEL_ID: paytmConfig.CHANNEL_ID,
    //             TXN_AMOUNT: `${amount}`, // String
    //             ORDER_ID: orderId, // String
    //             // EMAIL: email, // String
    //             // MOBILE_NO: mobile, // String
    //             CUST_ID: customerId, // String
    //             CHECKSUMHASH: checkSum, //From your server using PayTM Checksum Utility 
    //             CALLBACK_URL: callbackUrl,
    //         };

    //         // Paytm.startPayment(details)
    //     }
    //     catch (err) {
    //         console.log(err, 'error')
    //     }

    // }

    checkSlotAvailability = (controls) => {
        let startingTime;
        if (sliderStarting) {
            startingTime = sliderStarting;
        }
        else {
            controls.startingTime.value;
        }
        let index = this.state.timelineSlots.findIndex(time => {
            return moment(time.slotStart).format('HH:mm') == startingTime;
        });
        if (index >= 0) {
            if (this.state.timelineSlots[index].booked) {
                return false;
            }
            else {
                return true
            }
        }
        else {
            return false;
        }
    }

    promoModal = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                promoModalVisible: val
            }
        })
    };

    breakupModal = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                breakupCharges: val
            }
        })
    };

    chageStartingTime = (val) => {
        // console.log(val, 'chagne statrging')
        if (val) {
            sliderStarting = moment(val.time, 'hh:mm a').format('HH:mm');
            console.log(sliderStarting)
            this.renderValues(sliderStarting);
        }
    }

    applyPromoCode = (code) => {
        Keyboard.dismiss();
        this.getPricing(this.state.controls.slotTime.value, this.state.controls.power.value, code);
    }

    renderValues = (startingTime) => {
        return (
            <View>
                {/* <View style={[commonStyle.flexRow]}>
                    <View><Para bold={true}>Starting Time : </Para></View>
                    <View><Para style={[commonStyle.gray, commonStyle.small]}>{moment(startingTime, 'HH:mm').format('hh:mm a')}</Para></View>
                </View> */}
                <View style={[commonStyle.flexRow, { marginTop: 5 }]}>
                    <View><Para bold={true}>Slot Time : </Para></View>
                    <View><Para style={[commonStyle.gray, commonStyle.small]}>{this.state.controls.slotTime.value} min</Para></View>
                </View>
            </View>
        )
    }

    imageModal = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                showImageModal: val
            }
        })
    }

    render() {
        return (
            <ScrollView keyboardShouldPersistTaps='handled'>

                {
                    this.state.stationDetails ?
                        <View>
                            <View style={styles.stationDetails}>
                                {/* <Para>{JSON.stringify(this.state.stationDetails)}</Para> */}

                                <View style={[commonStyle.flexRow, commonStyle.textCenter]}>
                                    <View style={styles.line}>
                                    </View>
                                </View>
                                <View style={commonStyle.flexRow}>
                                    <View style={styles.details}>
                                        <View style={[commonStyle.flexRow, { marginBottom: 10 }]}>
                                            <Para style={styles.name} extraBold={true}>{this.state.stationDetails.name}</Para>
                                            <TouchableOpacity onPress={() => this.imageModal(true)}>
                                                <Image source={require('../../../assets/images/info.png')} style={[commonStyle.img, styles.info, { marginTop: 5 }]} />
                                            </TouchableOpacity>
                                        </View>
                                        <Para style={commonStyle.gray}>
                                            {
                                                this.state.stationDetails ?
                                                    addressFormat(this.state.stationDetails.address)
                                                    : 'Unknown'
                                            }
                                        </Para>
                                    </View>
                                    <View style={[styles.navigateDiv]} >
                                        <View style={{ width: '50%' }}>
                                            <TouchableOpacity style={[commonStyle.flexEnd]} onPress={() => this.openMap(this.state.stationDetails.geolocation)}>
                                                <Image source={require('../../../assets/images/navigate.png')} style={[commonStyle.img, styles.navigateImg]} />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={[commonStyle.flexEnd]} onPress={() => this.openMap(this.state.stationDetails.geolocation)}>
                                                <Para bold={true} style={[commonStyle.blue, commonStyle.small]}>Navigate</Para>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View style={[commonStyle.flexRow, styles.selectTime]}>
                                <View style={styles.starting}>
                                    <Para bold={true} style={styles.select}>
                                        Select Power
                                    </Para>
                                    <View style={styles.pickerView}>
                                        <Item picker style={styles.pickerItem}>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name="arrow-down" />}
                                                style={styles.picker}
                                                // textStyle = {{fontSize: 17,color:'yellow',fontFamily:fontFamily.bold}}
                                                placeholder="Select Manufacture"
                                                placeholderStyle={{ color: "#808080", fontFamily: fontFamily.bold }}
                                                placeholderIconColor="#808080"
                                                selectedValue={this.state.controls.power.value}
                                                onValueChange={(e) => this.changeHandler(e, 'power')}
                                            >

                                                {
                                                    this.state.powersArray ?
                                                        this.state.powersArray.map((power, i) => {
                                                            return <Picker.Item label={power} value={power} key={i} />
                                                        })
                                                        : null
                                                }
                                            </Picker>
                                        </Item>
                                    </View>
                                </View>
                                <View style={styles.charging}>
                                    <Para bold={true} style={styles.select}>
                                        Select Pin
                            </Para>
                                    <View style={styles.pickerView}>
                                        <Item picker style={styles.pickerItem}>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name="arrow-down" />}
                                                style={styles.picker}
                                                // textStyle = {{fontSize: 17,color:'yellow',fontFamily:fontFamily.bold}}
                                                placeholder="Select Manufacture"
                                                placeholderStyle={{ color: "#808080", fontFamily: fontFamily.bold }}
                                                placeholderIconColor="#808080"
                                                selectedValue={this.state.controls.pin.value}
                                                onValueChange={(e) => this.changeHandler(e, 'pin')}
                                            >

                                                {
                                                    this.state.pinsArray ?
                                                        this.state.pinsArray.map((pin, i) => {
                                                            return <Picker.Item label={pin} value={pin} key={i} />
                                                        })
                                                        : null
                                                }
                                            </Picker>
                                        </Item>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.selectDate}>
                                <Para bold={true}>Select Date</Para>
                            </View>
                            <View style={styles.datePick}>
                                {
                                    this.state.datesArray ?
                                        this.state.datesArray.map((date, i) => {
                                            return <TouchableOpacity onPress={() => this.changeHandler(date.date, 'date')} key={i} style={[styles.date, date.label == 'Today' ? { width: 70 } : { flex: 1 }, (this.state.controls.date.value == date.date) ? styles.selected : null]}><Para bold={true} style={[(this.state.controls.date.value == date.date) ? commonStyle.white : commonStyle.gray, commonStyle.small]}>{date.label}</Para></TouchableOpacity>
                                        })
                                        : null
                                }
                            </View>
                            <View style={[commonStyle.flexRow, styles.selectTime]}>
                                <View style={styles.starting}>
                                    <Para bold={true} style={styles.select}>
                                        Select Starting Time
                            </Para>
                                    <View style={styles.pickerView}>
                                        <Item picker style={styles.pickerItem}>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name="arrow-down" />}
                                                style={styles.picker}
                                                // textStyle = {{fontSize: 17,color:'yellow',fontFamily:fontFamily.bold}}
                                                placeholder="Select Manufacture"
                                                placeholderStyle={{ color: "#808080", fontFamily: fontFamily.bold }}
                                                placeholderIconColor="#808080"
                                                selectedValue={this.state.controls.startingTime.value}
                                                onValueChange={(e) => this.changeHandler(e, 'startingTime')}
                                            >

                                                {
                                                    this.state.timesArray ?
                                                        this.state.timesArray.map((time, i) => {
                                                            return <Picker.Item label={time.name} value={time.value} key={i} />
                                                        })
                                                        : null
                                                }
                                            </Picker>
                                        </Item>
                                    </View>
                                </View>
                                <View style={styles.charging}>
                                    <Para bold={true} style={styles.select}>
                                        Select Charging Time
                            </Para>
                                    <View style={styles.pickerView}>
                                        <Item picker style={styles.pickerItem}>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name="arrow-down" />}
                                                style={styles.picker}
                                                // textStyle = {{fontSize: 17,color:'yellow',fontFamily:fontFamily.bold}}
                                                placeholder="Select Manufacture"
                                                placeholderStyle={{ color: "#808080", fontFamily: fontFamily.bold }}
                                                placeholderIconColor="#808080"
                                                selectedValue={this.state.controls.slotTime.value}
                                                onValueChange={(e) => this.changeHandler(e, 'slotTime')}
                                            >

                                                {
                                                    this.state.slotArray ?
                                                        this.state.slotArray.map((time, i) => {
                                                            return <Picker.Item label={time.name} value={time.value} key={i} />
                                                        })
                                                        : null
                                                }
                                            </Picker>
                                        </Item>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.timeline}>
                                {/* <Timeline></Timeline> */}
                                {
                                    (this.state.timelineSlots.length > 0) ?
                                        // <ScrollView horizontal={true} style={styles.timelineScroll}>
                                        //     <Timeline
                                        //         min={0}
                                        //         timeSlots={this.state.timelineSlots}
                                        //         max={this.state.timelineSlots.length - 1}
                                        //         LRpadding={0}
                                        //         callback={this.multiSliderValueCallback}
                                        //         single={false}
                                        //         sliderLength={((this.state.timelineSlots.length - 1) * 40)}
                                        //         startingTime={this.state.controls.startingTime.value}
                                        //     ></Timeline>
                                        // </ScrollView>
                                        this.state.timeLoading ?
                                            <View style={{ alignItems: 'center' }}>
                                                <Spinner color='#1371EF' size={20}></Spinner>
                                            </View>
                                            :
                                            <SlotTimeline
                                                timelineSlots={this.state.timelineSlots}
                                                slotTime={this.state.controls.slotTime.value}
                                                startingTime={this.state.controls.startingTime.value}
                                                chageStartingTime={this.chageStartingTime}
                                            >
                                            </SlotTimeline>
                                        : <Para bold={true} style={{ paddingHorizontal: 30 }} >No Slot Available</Para>
                                }

                            </View>
                            <View style={styles.detailInfo}>
                                {
                                    this.renderValues(sliderStarting)
                                }
                                {/* <View style={[commonStyle.flexRow, { marginTop: 5 }]}>
                                    <View><Para bold={true}>Promo Code : </Para></View>
                                    <View><Para style={[commonStyle.gray, commonStyle.small]}></Para></View>
                                </View> */}
                                <View style={{ marginTop: 10 }}>
                                    {
                                        (this.state.charges && this.state.charges.promocode) ?
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <TouchableOpacity style={styles.couponBtn} onPress={() => this.applyPromoCode(null)}><Para bold={true} style={commonStyle.blue}>Remove Coupon</Para></TouchableOpacity>
                                                <Para style={{ marginLeft: 10 }} bold={true}>{this.state.charges.promocode} (₹{this.state.charges.discount ? this.state.charges.discount.toFixed(2) : 0})</Para>
                                            </View>
                                            :
                                            <TouchableOpacity style={styles.couponBtn} onPress={() => this.promoModal(true)}><Para bold={true} style={commonStyle.blue}>Apply Coupon</Para></TouchableOpacity>

                                    }
                                </View>
                            </View>
                            <View style={styles.bookingView}>
                                <View>
                                    <View style={[commonStyle.flexRow, { alignItems: 'center' }]}>
                                        <Para bold={true}>Charges :</Para>
                                        <TouchableOpacity
                                            onPress={() => this.breakupModal(true)}
                                        >
                                            <Image source={require('../../../assets/images/info.png')} style={[commonStyle.img, styles.info,]} />
                                        </TouchableOpacity>
                                    </View>
                                    <Para style={[commonStyle.blue, styles.name, { marginTop: 10 }]} extraBold={true}>₹{this.state.price.toFixed(2)}</Para>
                                </View>
                                <View style={commonStyle.flexEnd}>
                                    <Button style={[commonStyle.button, styles.button]} onPress={this.bookSlot}>
                                        {
                                            this.state.loading ?
                                                <Spinner color='white' size={40}></Spinner>
                                                :
                                                <Para bold={true} style={commonStyle.white} >Book Slot</Para>

                                        }
                                    </Button>
                                </View>
                            </View>
                        </View>
                        : null
                }
                <Modal isVisible={this.state.promoModalVisible} style={styles.modal} animationType="slide" animationOut='slideOutDown' animationIn='slideInUp' backdropOpacity={0.2}>
                    <Root>
                        <Container style={{ backgroundColor: 'transparent', display: 'flex', flexDirection: 'row', alignItems: 'flex-end' }}>
                            <View style={styles.modalView}>
                                <View style={styles.closeBtn}>
                                    <TouchableOpacity onPress={() => this.promoModal(false)}>
                                        <Image source={require('../../../assets/images/close.png')} style={[commonStyle.img, styles.closeIcon]} />
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.modalContent}>
                                    <PromoCode applyPromoCode={this.applyPromoCode} hideModal={() => this.promoModal(false)}></PromoCode>
                                </View>
                            </View>
                        </Container>
                    </Root>
                </Modal>
                <Modal isVisible={this.state.breakupCharges} style={styles.modal} animationType="slide" animationOut='slideOutDown' animationIn='slideInUp' backdropOpacity={0.2}>
                    <View style={styles.modalView}>
                        <View style={styles.closeBtn}>
                            <TouchableOpacity onPress={() => this.breakupModal(false)}>
                                <Image source={require('../../../assets/images/close.png')} style={[commonStyle.img, styles.closeIcon]} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.modalContent}>
                            <BreakupCharges charges={this.state.charges}></BreakupCharges>
                        </View>
                    </View>
                </Modal>
                <Modal isVisible={this.state.failModal} style={styles.modal} animationType="slide" animationOut='slideOutDown' animationIn='slideInUp' backdropOpacity={0.2}>
                    <Root>
                        <Container style={{ backgroundColor: 'transparent', display: 'flex', flexDirection: 'row', alignItems: 'flex-end' }}>
                            <View style={styles.modalView}>
                                <View style={styles.closeBtn}>
                                    <TouchableOpacity onPress={this.paymentFailed}>
                                        <Image source={require('../../../assets/images/close.png')} style={[commonStyle.img, styles.closeIcon]} />
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.modalContent}>
                                    <PaymentFailed errMsg={this.state.paymentErrMsg} tryAgain={this.tryAgain} failedCount={this.state.failedCount} closeModal={this.paymentFailed}></PaymentFailed>
                                </View>
                            </View>
                        </Container>
                    </Root>
                </Modal>
                <Modal isVisible={this.state.showImageModal} style={styles.modal} animationType="slide" animationOut='slideOutDown' animationIn='slideInUp' backdropOpacity={0.8}>
                    <Root>
                        <Container style={{ backgroundColor: 'transparent', display: 'flex', flexDirection: 'row', alignItems: 'flex-end' }}>
                            <View style={{ width: '100%' }}>
                                <View style={[styles.closeBtn, { top: -10 }]}>
                                    <TouchableOpacity onPress={() => this.imageModal(false)}>
                                        <Image source={require('../../../assets/images/close.png')} style={[commonStyle.img, styles.closeIcon]} />
                                    </TouchableOpacity>
                                </View>
                                <View>
                                    <StationImages closeModal={() => this.imageModal(false)} images={this.state.stationDetails.images}></StationImages>
                                </View>
                            </View>
                        </Container>
                    </Root>
                </Modal>
                {
                    this.state.paymentData ?
                        <WebView
                            style={{ flex: 1, height: 100, width: Dimension.width }}
                            useWebKit={true}
                            startInLoadingState={false}
                            source={{ url: 'https://www.google.com' }}
                        />
                        : null
                }

            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    stationDetails: {
        paddingHorizontal: 25,
        paddingTop: 10,
        paddingBottom: 25,
        backgroundColor: 'white',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    line: {
        width: 35,
        height: 5,
        backgroundColor: '#1371EF',
        borderRadius: 5,
        marginBottom: 20
    },
    detailInfo: {
        backgroundColor: 'white',
        paddingHorizontal: 25,
        paddingBottom: 20
    },
    details: {
        width: '60%'
    },
    navigateDiv: {
        width: '40%',
        alignItems: 'flex-end'
        // paddingLeft:30
    },
    name: {
        fontSize: 20,
    },
    info: {
        height: 15,
        marginLeft: 5
    },
    navigateImg: {
        height: 50,
        width: 50,
        marginBottom: 10
    },
    selectDate: {
        paddingVertical: 10,
        // backgroundColor:'red',
        paddingHorizontal: 25
    },
    datePick: {
        padding: 20,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    date: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 8,
        paddingHorizontal: 4,
        paddingVertical: 8,
        borderRadius: 10,
        backgroundColor: 'white',
        textAlign: 'center',
        // paddingHorizontal: 10,
        // height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 6,
    },
    selected: {
        backgroundColor: '#1371EF'
    },
    starting: {
        width: '50%'
    },
    charging: {
        width: '50%'
    },
    select: {
        paddingHorizontal: 25,
        paddingVertical: 10,
        fontSize: 14,
        // height:'100%'
    },
    pickerView: {
        padding: 25,
        backgroundColor: 'white'
    },
    bookingView: {
        padding: 25,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 8,
        backgroundColor: 'white',
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    button: {
        width: '80%'
    },
    timeline: {
        backgroundColor: 'white',
        paddingBottom: 25,
    },
    timelineScroll: {
        // paddingHorizontal: 15
        // top: -25
    },
    pickerItem: {
        backgroundColor: 'white',
        borderRadius: 10,
        borderColor: 'transparent',
        borderWidth: 0,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        height: 40,
        elevation: 6,
    },
    modalView: {
        backgroundColor: 'white',
        width: '100%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        position: 'relative'
        // width:Dimension.width,
        // flex:1
        // left:0
        // position:'absolute',
        // bottom:0
    },
    modal: {
        margin: 0, // This is the important style you need to set
        // alignItems: undefined,
        justifyContent: 'flex-end',
    },
    closeBtn: {
        position: 'absolute',
        right: 15,
        top: -28,
        zIndex: 10,
        height: 60,
        width: 60,
    },
    closeIcon: {
        width: '100%',
        height: '100%'
    },
    modalContent: {
        paddingTop: 20,
        paddingBottom: 10
    },
    couponBtn: {
        borderWidth: 1,
        borderColor: '#1371EF',
        width: 120,
        padding: 5,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        // marginTop: 10
    },
    // root:{
    //     display:'flex',
    //     flexDirection:'row',
    //     alignItems:'flex-end',
    //     // justifyContent:'flex-end'
    // }
})
