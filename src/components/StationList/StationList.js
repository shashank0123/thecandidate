import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import SearchBar from '../Shared/SearchBar/SearchBar'
import Para from '../Shared/Text/Para'
import { commonStyle } from '../../utils/Style/Style'
import StationBox from '../StationBox/StationBox'
import { TouchableOpacity, ScrollView, FlatList } from 'react-native-gesture-handler'
import * as Animatable from 'react-native-animatable';
import { Button } from 'native-base'
import Swiper from 'react-native-swiper'
const cardOptions = { overlayOpacity: 1 };
const contentOptions = { enterFrom: "right" }


export default class StationList extends Component {

    state = {

    }
    constructor(props) {
        super(props);
        // this.textInput = React.createRef();

    }



    render() {
        return (
            <View style={styles.stationBox}
            >
                {/* <Animatable.View
                    style={[styles.timeline, commonStyle.flexRow, commonStyle.textCenter]}
                    animation={this.props.active == 'search' ? hide : show}
                    duration={200}
                >
                    <Para bold={true}>You have saved 15 tree this month</Para>
                </Animatable.View > */}
                <View style={{ height: 30, }}>
                    <Swiper style={styles.wrapper}
                        ref={(swiper) => { this._swiper = swiper; }}
                        showsButtons={false}
                        loop={true}
                        autoplay={true}
                        // activeDot={false}
                        dotStyle={{ position: 'relative', top: 32, borderRadius: 2, width: 6, height: 6, transform: [{ rotate: '45deg' }] }}
                        activeDotStyle={{ position: 'relative', top: 32, borderRadius: 2, width: 6, height: 6, transform: [{ rotate: '45deg' }] }}
                        showsPagination={true}
                        // nextButton={(this.state.index == 2) ? (<TouchableOpacity onPress={this.navigate}><Para style={{ color: '#C4C4C4' }}>Sign up</Para></TouchableOpacity>) : (<Image source={require('../../../assets/images/next.png')} style={styles.icon} />)}
                        // prevButton={< Image source={require('../../../assets/images/previous.png')} style={styles.icon} />}
                        buttonWrapperStyle={styles.buttonWrap}
                    // onIndexChanged={(index) => this.changeIndex(index)}
                    >
                        <View style={[commonStyle.textCenter, commonStyle.flexRow]}>
                            <Para bold={true}>Welcome to Chargebizz</Para>
                        </View>
                        <View style={[commonStyle.textCenter, commonStyle.flexRow]}>
                            <View style={styles.save}>
                                <Para bold={true}>You have saved {Math.floor(this.props.userData?this.props.userData.savedTrees:'null')}</Para>
                                <Animatable.Image source={require('../../../assets/images/tree.png')} style={[styles.tree, commonStyle.img]} />
                                <Para bold={true}>by charging with us</Para>
                            </View>
                        </View>
                        {/* <View></View> */}
                    </Swiper>
                </View>

                <View style={styles.searchBar}>
                    <SearchBar filter={this.props.showFilter} cancelFilter={this.props.cancelFilter} filterData={this.props.filterData} active={this.props.active} showSearch={this.props.showSearch} searchStations={this.props.searchStations}></SearchBar>
                </View>

                {
                    <ScrollView
                        style={styles.displayStation}
                    >
                        <View>
                            {
                                (this.props.displayStation) ?

                                    this.props.displayStation.map((item, index) => {
                                        return <StationBox station={item} key={index} length={this.props.displayStation.length - 1} in={index} singleStation={this.props.singleStation}></StationBox>
                                    })
                                    : <Para>No Station Available</Para>
                            }
                        </View>

                    </ScrollView>
                }


            </View>
        )
    }
}

const styles = StyleSheet.create({
    stationBox: {
        padding: 15,
    },
    headerStyle: {
        backgroundColor: "#68E9FF", padding: 30,
        marginBottom: 10, borderRadius: 5
    },
    textStyle: {
        fontSize: 40, opacity: 0.6,
        textAlign: 'center', fontWeight: 'bold'
    },
    contentStyle: {
        width: "90%",
        padding: 50,
        backgroundColor: "#E85F53"
    },
    displayStation: {
        // paddingBottom:00
    },
    searchBar: {
        position: 'relative', zIndex: 100000
    },
    wrapper: {
        height: 40
    },
    save: {
        // paddingTop: 10,
        // paddingBottom: 20,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    tree: {
        height: 23,
        width: 30
    },

});


const hide = {
    0.5: {
        opacity: 1,

    },
    1: {
        opacity: 0
    },
}

const show = {
    0.5: {
        opacity: 0.5,

    },
    1: {
        opacity: 1
    },
}