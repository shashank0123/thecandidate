import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import Swiper from 'react-native-swiper'
import { imageUrl } from '../../utils/BaseUrl/BaseUrl'
import Dimension from '../../utils/Dimension/Dimension'
import Para from '../Shared/Text/Para'
import { commonStyle } from '../../utils/Style/Style'

export default class StationImages extends Component {


    state = {
        loadSwiper: false
    }
    componentDidMount = () => {
        setTimeout(() => {
            this.setState({ loadSwiper: true });
        }, 100);
    }
    render() {
        return (
            <View style={{ height: Dimension.height - 50 }}>
                {
                    this.state.loadSwiper ?
                        <Swiper style={styles.wrapper}
                            height={Dimension.height}
                            width={Dimension.width}
                            showsButtons={true}
                            loop={false}
                            dotColor={'white'}
                            nextButton={<Image source={require('../../../assets/images/next.png')} style={styles.icon} />}
                            prevButton={< Image source={require('../../../assets/images/previous.png')} style={styles.icon} />}
                            buttonWrapperStyle={styles.buttonWrap}
                        // onIndexChanged={(index) => this.changeIndex(index)}
                        >
                            {
                                this.props.images.length > 0 ?
                                    this.props.images.map((image, i) => {
                                        return <Image source={{ uri: `${imageUrl}${image}` }} style={[styles.image, commonStyle.img]} />
                                    })
                                    : <View style={{height:'100%',justifyContent:'center',alignItems:'center'}}><Para style={[commonStyle.white,{fontSize:20}]} bold={true}>No Preview Available</Para></View>
                            }
                        </Swiper >
                        : null
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    buttonWrap: {
        alignItems: 'flex-end',
        paddingBottom: 23
    },
    // imgDiv: {
    //     height: '100%',
    //     width: '100%',
    //     // marginRight:40
    //     // backgroundColor:'red'
    // },
    image: {
        // maxWidth: Dimension.width,
        // maxHeight: '100%',
        // height: '100%',
        // width:'100%',
        borderRadius: 5,
        maxWidth:Dimension.width-40,
        maxHeight:'100%',
        width:Dimension.width-40,
        height:'100%',
        position:'relative',
        left:20
        // marginRight:40
    },
    wrapper: {
        // height: Dimension.height - 100,
        // backgroundColor: 'red'
    }
})
