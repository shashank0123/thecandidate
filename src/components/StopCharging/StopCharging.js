import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import Para from '../Shared/Text/Para'
import { commonStyle } from '../../utils/Style/Style'
import { Button, Spinner } from 'native-base'

export default class StopCharging extends Component {
    render() {
        return (
            <View style={{ paddingHorizontal: 30, paddingVertical: 20 }}>
                <View>
                    <Para style={styles.title} extraBold={true}>Complete Charging</Para>
                    <Para>Are you sure?</Para>
                    <View style={styles.buttonView}>
                        <Button onPress={this.props.closeModal} style={styles.noButton}><Para bold={true} style={commonStyle.blue}>No</Para></Button>
                        <Button style={[commonStyle.button, styles.yesButton]} onPress={this.props.stopCharging}>
                            {
                                this.props.loading ?
                                    <Spinner color='white' size={20}></Spinner>
                                    :
                                    <Para bold={true} style={commonStyle.white}>Yes</Para>
                            }
                        </Button>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        marginBottom: 10
    },
    buttonView: {
        marginTop: 40,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    noButton: {
        width: '30%',
        height: 50,
        backgroundColor: 'white',
        display: 'flex',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#1371EF',
        marginRight: 15,
        elevation: 0

    },
    yesButton: {
        width: '30%',
        height: 50,
    }
})
