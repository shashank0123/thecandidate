import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import Para from '../Shared/Text/Para'

export default class SwipingScreen extends Component {
    render() {
        return (
            <View style={styles.onboarding}>
                <View>
                    {this.props.screen.image}
                </View>
                <Para extraBold={true} style={styles.head}>{this.props.screen.head}</Para>
                <Para style={styles.text}>{this.props.screen.para}</Para>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    onboarding: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30
    },
    head: {
        fontSize: 25,
        marginTop: 15,
        marginBottom: 15,
        textAlign: 'center'

    },
    text: {
        textAlign: 'center',
        color: '#4A6887'
    }
})
