import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, ToastAndroid, Platform } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Scan from '../../screens/Scan/Scan';
import Profile from '../../screens/Profile/Profile';
import { Icon } from 'native-base';
import { commonStyle } from '../../utils/Style/Style';
import Feedback from '../Feedback/Feedback';
// import Search from '../Search/Search';
import { BackHandler } from 'react-native';
const CustomTab = createBottomTabNavigator();
// var count = 0;

export default class Tab extends Component {

    constructor(props) {
        super(props);
        this.props.navigation.addListener('focus', () => {
            this.onFocus();
        })
    }

    onFocus = () => {
        // if (this.props.route && this.props.route.state && this.props.route.state.index) {
        //     let index = this.props.route.state.index;
        //     let val = 'station';

        //     if (index == 1) {
        //         val = 'scan';
        //     }
        //     else if (index == 2) {
        //         val = 'profile';
        //     }
        //     this.tabPressed(val);
        // }
    }

    state = {
        active: 'station',
        backCount: 0
    }

    tabPressed = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                active: val
            }
        })
    }




    render() {
        return (
            <CustomTab.Navigator
                // backBehavior='initialRoute'
                // initialRouteName="Stations"
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused, color, size }) => {
                        if (route.name === 'Stations') {
                            return this.state.active == 'station' ?
                                <Image style={[commonStyle.img, styles.icon]} source={require('../../../assets/images/station-a.png')} />
                                :
                                <Image style={[commonStyle.img, styles.icon]} source={require('../../../assets/images/station.png')} />

                        } else if (route.name === 'Scan') {
                            return <Image style={[commonStyle.img, styles.scanIcon]} source={require('../../../assets/images/scan.png')} />
                        }
                        else {
                            return this.state.active == 'profile' ?
                                <Image style={[commonStyle.img, styles.icon]} source={require('../../../assets/images/profile-a.png')} />
                                :
                                <Image style={[commonStyle.img, styles.icon]} source={require('../../../assets/images/profile.png')} />
                        }
                    },
                })}
                tabBarOptions={{
                    activeTintColor: '#1371EF',
                    inactiveTintColor: '#808080',
                    tabStyle: {
                        paddingVertical: 5,
                    },
                    labelStyle: {
                        fontFamily: 'CoreSansG-Medium',
                        fontSize: 12
                    },
                    style: {
                        height: Platform.OS == 'android' ? 55 : 90
                    }
                }}

            >
                

                <CustomTab.Screen
                    name="Scan"
                    component={Scan}
                    listeners={({ navigation, route }) => ({
                        tabPress: () => this.tabPressed('scan'),
                    })}
                />

                <CustomTab.Screen
                    name="Profile"
                    component={Profile}
                    listeners={({ navigation, route }) => ({
                        tabPress: () => this.tabPressed('profile'),
                    })}
                />
            </CustomTab.Navigator >
        )
    }
}

const styles = StyleSheet.create({
    icon: {
        height: 20,
        width: 20,
        marginBottom: 5
    },
    scanIcon: {
        height: 50,
        width: 50,
        marginBottom: 35
    }
})
