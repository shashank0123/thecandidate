import React, { Component } from 'react'
import PropTypes from 'prop-types';
import ReactNative from 'react-native'
// import times from 'lodash.times'
import { times } from 'lodash';
import { fontFamily } from '../../utils/FontFamily/FontFamily';
import moment from 'moment';

var {
    StyleSheet,
    ScrollView,
    Dimensions,
    Text,
    View,
} = ReactNative

const GAUGE_WIDTH = Math.floor(Dimensions.get('window').width)
const INTERVAL_WIDTH = 12
const LEFTOFFSET = 24;

const scale = (v, inputMin, inputMax, outputMin, outputMax) => {
    return Math.round(((v - inputMin) / (inputMax - inputMin)) * (outputMax - outputMin) + outputMin)
}

export default class TimeTest extends Component {

    constructor(props) {
        super(props)

        this._handleScroll = this._handleScroll.bind(this)
        this._handleScrollEnd = this._handleScrollEnd.bind(this)
        this._handleContentSizeChange = this._handleContentSizeChange.bind(this)

        this.scrollMin = 0
        this.scrollMax = this._getScrollMax(props)
        this._scrollQueue = null
        this._value = props.value || props.min

        this.state = {
            contentOffset: 0,
            rightOffset: 0
        }

        // setTimeout(() =>{
        //     this.scrollKr();
        // },2000)
    }

    scrollLeftRight = (val) => {
        console.log(val, 'scroll val')
        setTimeout(() => {
            if (this._scrollView) {
                this._scrollView.scrollTo({ x: val, y: 0, animated: true })
            }
        }, 50)
    }

    componentDidMount() {
        this.setPadding(this.props);
        setTimeout(() => {
            this.setState({
                contentOffset: this._scaleValue(this._value),
            })
        }, 0)
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps, 'next level props >>>')
        this.scrollMax = this._getScrollMax(nextProps)

        if (nextProps.value !== this._value) {
            this._setScrollQueue({
                x: this._scaleValue(nextProps.value, nextProps),
                animate: true,
            })

            if (!this._contentSizeWillChange(nextProps)) {
                this._resolveScrollQueue()
            }
        }

        if (nextProps.startingTime) {
            // console.log(nextProps.startingTime, this.props.slots);
            let index = nextProps.slots.findIndex(time => {
                // console.log(moment(time.time, 'hh:mm a').format('hh:mm'),nextProps.startingTime,'yha p starting time')
                return moment(time.time, 'hh:mm a').format('HH:mm') == moment(nextProps.startingTime, 'HH:mm').format('HH:mm');
            })

            if (index >= 0) {
                console.log(index, this._value)
                if (index > this._value) {
                    this.scrollLeftRight((index - this._value) * 12);
                }
                else {
                    this.scrollLeftRight(-((index - this._value) * 12));
                }
            }
        }

        this.setPadding(nextProps);

    }

    setPadding = (props) => {
        let secondPointer = (props.slotTime * (1 / 15) * 12) + 24;
        let right = GAUGE_WIDTH - secondPointer
        this.setState(prev => {
            return {
                ...prev,
                rightOffset: right
            }
        })
    }

    _contentSizeWillChange(nextProps) {
        let { min, max } = nextProps
        if (min !== this.props.min || max !== this.props.max) {
            return true
        }

        return false
    }

    _getScrollMax(props = this.props) {
        return (props.max - props.min) * INTERVAL_WIDTH;

    }

    _scaleScroll(x, props = this.props) {
        let { min, max } = props
        return scale(x, this.scrollMin, this.scrollMax, min, max)
    }

    _scaleValue(v, props = this.props) {
        let { min, max } = props
        return scale(v, min, max, this.scrollMin, this.scrollMax)
    }

    _setScrollQueue(scrollTo) {
        this._scrollQueue = scrollTo
    }

    _resolveScrollQueue() {
        if (this._scrollQueue !== null) {
            this._scrollView && this._scrollView.scrollTo(this._scrollQueue)
            this._handleScrollEnd()
        }
    }

    _handleContentSizeChange() {
        this._resolveScrollQueue()
    }

    _handleScroll(event) {
        if (this._scrollQueue) return

        let offset = event.nativeEvent.contentOffset.x

        let val = this._scaleScroll(offset)

        if (val !== this._value) {
            this._value = val
            this.props.onChange(val)
        }
    }

    _handleScrollEnd() {
        this._value = this.props.value;
        this._scrollQueue = null
    }

    _getIntervalSize(val) {
        let { largeInterval, mediumInterval } = this.props

        if (val % largeInterval === 0) return 'large'
        // if (val % mediumInterval === 0) return 'medium'
        return 'small'
    }

    _renderIntervals() {
        let { min, max } = this.props
        let range = max - min + 1

        let values = times(range, (i) => i + min)
        // console.log(values)
        return this.props.slots.map((val, slotIndex) => {
            // console.log(val)
            let intervalSize = this._getIntervalSize(val.index)

            return (
                <View key={`val-${val.index}`} style={styles.intervalContainer}>
                    {(intervalSize === 'large') ?
                        <Text style={[styles.intervalValue, this.props.styles.intervalValue]}>{val.time}</Text> : null
                    }

                    <View style={[styles.interval, styles[intervalSize], this.props.styles.interval, this.props.styles[intervalSize]]} />
                    <View style={[styles.bookedLine, val.booked ? styles.redLine : null]} />
                </View>
            )
        })
    }

    render() {
        return (
            <View style={[styles.container, this.props.styles.container]}>
                <ScrollView
                    ref={r => this._scrollView = r}
                    automaticallyAdjustInsets={false}
                    horizontal={true}
                    decelerationRate={0}
                    snapToInterval={INTERVAL_WIDTH}
                    snapToAlignment="start"
                    showsHorizontalScrollIndicator={false}
                    onScroll={this._handleScroll}
                    onMomentumScrollEnd={this._handleScrollEnd}
                    onContentSizeChange={this._handleContentSizeChange}
                    scrollEventThrottle={100}
                    contentOffset={{ x: this.state.contentOffset }}
                >

                    <View style={[styles.intervals, this.props.styles.intervals, this.state.rightOffset > 0 ? { paddingRight: this.state.rightOffset } : null]}>
                        {this._renderIntervals()}
                    </View>
                </ScrollView>
                <View style={[styles.selectedLine, { width: (this.props.slotTime * (24 / 30)) }]}></View>
                <View style={[styles.centerline, this.props.styles.centerline]} />
                <View style={[styles.secondCenter, { left: (-7.5 + LEFTOFFSET) + (this.props.slotTime * (24 / 30)) }]} />
            </View>
        )
    }
}

TimeTest.propTypes = {
    min: PropTypes.number,
    max: PropTypes.number,
    largeInterval: PropTypes.number,
    mediumInterval: PropTypes.number,
    value: PropTypes.number,
    onChange: PropTypes.func,
    styles: PropTypes.object,
}

TimeTest.defaultProps = {
    min: 1,
    max: 100,
    mediumInterval: 5,
    largeInterval: 10,
    onChange: () => { },
    styles: {},
}

var styles = StyleSheet.create({
    container: {
        height: 55,
        // backgroundColor:'red',
        width: GAUGE_WIDTH,
        // borderTopWidth: StyleSheet.hairlineWidth,
        // borderBottomWidth: StyleSheet.hairlineWidth,
        borderTopColor: '#DDDDDD',
        borderBottomColor: '#DDDDDD',
        backgroundColor: 'white',
        marginVertical: 8,
    },
    intervals: {
        flexDirection: 'row',
        // backgroundColor:'red',
        alignItems: 'flex-end',
        paddingLeft: LEFTOFFSET,
        // paddingRight: RIGHTOFFSET,
        marginHorizontal: -INTERVAL_WIDTH / 2,
    },
    intervalContainer: {
        width: INTERVAL_WIDTH,
        alignItems: 'center',
    },
    interval: {
        width: 1,
        // height:30,
        marginRight: -1,
        backgroundColor: '#979797',
    },
    intervalValue: {
        fontSize: 10,
        // position:'absolute',
        marginBottom: 3,
        width: 30,
        textAlign: 'center',
        // fontWeight: 'bold',
        fontFamily: fontFamily.bold
    },
    small: {
        height: 13,
    },
    medium: {
        height: 20,
    },
    large: {
        backgroundColor: '#1371EF',
        width: 3,
        height: 26,
    },
    centerline: {
        // height: 54,
        width: 15,
        height: 15,
        borderRadius: 100,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#1371EF',
        position: 'absolute',
        left: -7.5 + LEFTOFFSET,
        // opacity: 0.6,
        bottom: -7.5,
        zIndex: 3
    },
    secondCenter: {
        width: 15,
        height: 15,
        borderRadius: 100,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#1371EF',
        position: 'absolute',
        // left: (GAUGE_WIDTH / 2) - 15,
        // opacity: 0.6,
        bottom: -7.5,
        zIndex: 3
    },
    selectedLine: {
        left: LEFTOFFSET,
        borderBottomWidth: 5,
        borderColor: '#1371EF',
        position: 'absolute',
        bottom: 0,
        zIndex: 2,
        // width:48
    },
    bookedLine: {
        width: INTERVAL_WIDTH,
        // height: 30,
        // borderRadius: 100,
        backgroundColor: 'white',
        borderBottomWidth: 4,
        borderColor: '#979797',
        position: 'absolute',
        left: 7.5,
        // opacity: 0.6,
        bottom: 0,
        zIndex: 1
    },
    redLine: {
        borderColor: 'red'
    }
})
