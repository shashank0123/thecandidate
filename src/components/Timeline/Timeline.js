import React, { Component } from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
// import MultiSlider from '@ptomasroos/react-native-multi-slider';
// import { CustomMarker } from './CustomMarker';
import { TimelineSelected } from '../TimelineSelected/TimelineSelected';
import moment from 'moment';

export class Timeline extends Component {

    constructor(props) {
        super(props);
        console.log(this.props)
        this.state = {
            multiSliderValue: [this.props.min, this.props.min + 1],
            first: this.props.min,
            second: this.props.max,
        }
    }

    componentWillReceiveProps = (props) =>{
        this.renderScale();
    }


    render() {
        return (
            <View style={{ paddingHorizontal: 15 }}>
                <View style={[styles.column, { marginLeft: this.props.LRpadding, marginRight: this.props.LRpadding }]}>
                    {this.renderScale()}
                </View>
                <View style={styles.container}>

                    {/* <MultiSlider
                        trackStyle={{ backgroundColor: '#bdc3c7' }}
                        selectedStyle={{ backgroundColor: "#1371EF",position:'relative',zIndex:100 }}
                        values={this.state.multiSliderValue}
                        sliderLength={this.props.sliderLength}
                        onValuesChange={this.multiSliderValuesChange}
                        min={this.props.min}
                        max={this.props.max}
                        step={1}
                        allowOverlap={false}
                        snapped={true}
                        customMarker={(e) => {
                            return (<View
                                currentValue={e.currentValue} style={{ borderWidth: 2, backgroundColor: 'white', borderColor: '#1371EF', width: 28, height: 28, borderRadius: 100 }}></View>)
                        }}

                    /> */}
                </View>
            </View>
        );
    }

    multiSliderValuesChange = values => {
        console.log(values)
        this.setState({
            multiSliderValue: values,
            first: values[0],
            second: values[1],
        })
        this.props.callback(values)
        this.renderScale();
    }

    stepClick = (val) => {
        if (val > this.state.multiSliderValue[1]) {
            this.multiSliderValuesChange([this.state.multiSliderValue[0], val]);
        }
        else if (val < this.state.multiSliderValue[0]) {
            this.multiSliderValuesChange([val, this.state.multiSliderValue[1]]);
        }
        else if ((val > this.state.multiSliderValue[0] && val < this.state.multiSliderValue[1])) {

        }
    }

    renderScale = () => {
        const items = [];
        for (let i = 0; i < this.props.timeSlots.length; i++) {
            let startTime = moment(this.props.timeSlots[i].slotStart).format("hh:mm a");
            let endTime = moment(this.props.timeSlots[i].slotEnd).format("hh:mm a");
            let active = false;
            active = (this.state.multiSliderValue.indexOf(i) >= 0) ? true : false;
            items.push(
                <TimelineSelected
                    value={i}
                    first={i}
                    second={i + 1}
                    last={this.props.timeSlots.length}
                    active={active}
                    time={startTime}
                    booked={this.props.timeSlots[i].booked}
                    stepClick={this.stepClick}
                />
            );
        }

        return items;
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    column: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        bottom: -20,
    },
    active: {
        textAlign: 'center',
        fontSize: 20,
        color: '#5e5e5e',
    },
    inactive: {
        textAlign: 'center',
        fontWeight: 'normal',
        color: '#bdc3c7',
    },
    line: {
        textAlign: 'center',
    }
});