import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Para from '../Shared/Text/Para';
import { TouchableOpacity } from 'react-native-gesture-handler';

export class TimelineSelected extends Component {

    render() {
        return (
            <View style={{ width: 40,position:'relative',left:20}}>
                <View style={{ position: 'relative', top: 30, alignItems: 'flex-start', }}>
                    <Para style={[styles.active, this.props.active ? {} : { opacity: 0 }]} bold={true}>{this.props.time}</Para>
                </View>
                <View style={styles.slashed}>
                    <Text onPress={() => this.props.stepClick(this.props.value)}>|</Text>
                </View>
                {
                    (this.props.value !== (this.props.last-1))?
                    <View style={[(this.props.booked) ? styles.redLine : null]}>
                    </View>
                    :null
                }
               
            </View>
        );
    }

    // checkActive = () => {
    //     if (this.props.value >= this.props.first && this.props.value <= this.props.second)
    //         return true
    //     else
    //         return false
    // }
}

const styles = StyleSheet.create({
    active: {
        textAlign: 'center',
        fontSize: 13,
        bottom: 25,
        left: -18,
        color: '#1371EF',
    },
    redLine: {
        borderBottomWidth: 2,
        position: 'relative',
        borderBottomColor: 'red',
        top: 6,
        zIndex: 1
    },
   
    slashed: {
        position: 'relative',
        // backgroundColor: 'red',
        top: 13
    }
});