import 'react-native-gesture-handler'
import React from 'react'
import {NavigationContainer} from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'


import {Icons} from '../utils'


// screens import 
import Home from '../screens/Home'
import Login from '../screens/Login'

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();


function AppHome() {
	return (
			<Tab.Navigator
				initialRouteName="AppHome"
			>
				<Tab.Screen
					name="Home"
					component={Home}
					options={{
						tabBarLabel:"Home"
					}}
				/>
			</Tab.Navigator>
		);
}


function Navigation() {
	return (
		<NavigationContainer>
			<Stack.Navigator
				initialRouteName="Home"

			>
				<Stack.Screen name="AppHome" component={Home}/>
			</Stack.Navigator>
		</NavigationContainer>
	)
}