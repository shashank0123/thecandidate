import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, BackHandler, ToastAndroid } from 'react-native'
import CustomHeader from '../../components/Shared/CustomHeader/CustomHeader'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import Para from '../../components/Shared/Text/Para'
import { Button, Icon, Spinner, Root, Container } from 'native-base';
import { Item, Picker } from 'native-base';
import { commonStyle } from '../../utils/Style/Style'
import { fontFamily } from '../../utils/FontFamily/FontFamily';
import validate from '../../utils/Validation/Validation'
import { url, imageUrl } from '../../utils/BaseUrl/BaseUrl'
import axios from 'axios';
import { msg, errMsg } from '../../utils/Toaster/Toaster'
import { disableRoute } from '../../utils/Constants/Constatnts';
import ImagePicker from 'react-native-image-picker';
import Dimension from '../../utils/Dimension/Dimension';
import Modal from 'react-native-modal';
import OtherModel from '../../components/OtherModel/OtherModel'

var count = 0;

export default class AddVechile extends Component {

    constructor(props) {
        super(props);
        if (this.props.route.params.goTo == 'MyVehicle') {
            this.currentRouteName = '';
        }
        else {
            this.currentRouteName = 'addvehicle';
        }

        this.props.navigation.addListener('blur', () => {
            this.onLeave();
        });
        this.props.navigation.addListener('focus', () => {
            this.onFocus();
        });

    }

    state = {
        manufactureList: [],
        modelList: [],
        powersArray: [],
        pinsArray: [],
        allVehicles: [],
        otherModal: false,
        otherModalList: [],
        showOptions: null,
        controls: {
            manufacturer: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                    required: true,
                }
            },
            model: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                    required: true,
                }
            },
            pin: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },

            },
            power: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },

            },
            type: {
                value: '2 Wheeler',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                    required: true,
                }
            },
        },
        // otherManufacturer: null,
        // otherModel: null,
        image: null,
        loading: false
    }

    componentDidMount = () => {
        this.getManufecturer({ type: '2 Wheeler' });
        this.getPowerPin();
        this.getVehicles();
    }

    getVehicles = async () => {
        try {
            let res = await axios.get(`${url}/user`);
            if (res) {

                this.setState((prev) => {
                    return {
                        ...prev,
                        allVehicles: res['data']['data']['vehicles']
                    }
                });
            }
        }
        catch (err) {
            if (err) {
                console.log(err, err.response)
            }
        }
    }

    getManufecturer = async (params) => {
        try {
            let res = await axios.get(`${url}/vehicle/manufacturer`, { params: params });
            if (res) {
                console.log(res['data']['data'])
                let manufactureIndex = res['data']['data'].findIndex(element => {
                    return element._id == 'manufacturer';
                })

                this.getModelList(res['data']['data'][manufactureIndex].data[0]);
                this.setState((prev) => {
                    return {
                        ...prev,
                        manufactureList: res['data']['data'][manufactureIndex].data,
                        controls: {
                            ...prev.controls,
                            manufacturer: {
                                ...prev.controls.manufacturer,
                                value: res['data']['data'][manufactureIndex].data[0]
                            },

                        }
                    }
                });

            }
        }
        catch (err) {
            if (err) {
                console.log(err, err.response)
            }
        }
    }

    getModelList = async (manufacturer) => {
        // console.log(manufacturer)
        try {
            let res = await axios.get(`${url}/vehicle`, { params: { manufacturer: manufacturer } });
            if (res) {
                console.log(res['data']['data'])

                this.setState((prev) => {
                    return {
                        ...prev,
                        modelList: (res['data']['data'].length > 0) ? res['data']['data'] : this.state.otherModalList,
                        controls: {
                            ...prev.controls,
                            model: {
                                ...prev.controls.model,
                                value: res['data']['data'][0] ? res['data']['data'][0].model : ''
                            },

                        }
                    }
                });
            }
        }
        catch (err) {
            if (err) {
                console.log(err, err.response)
            }
        }
    }

    getPowerPin = async () => {
        try {
            let res = await axios.get(`${url}/common/meta`)
            if (res) {
                // console.log(res['data']['data'])
                this.setState(prev => {
                    return {
                        ...prev,
                        powersArray: res['data']['data']['powers'],
                        pinsArray: res['data']['data']['pin'],
                        controls: {
                            ...prev.controls,

                            power: {
                                ...prev.controls.power,
                                value: res['data']['data']['powers'][0]
                            },
                            pin: {
                                ...prev.controls.pin,
                                value: res['data']['data']['pin'][0]
                            }
                        }
                    }
                })
            }
        }
        catch (err) {
            if (err) {
                console.log(err)
            }
        }
    }

    changeTextHandler = (event, key) => {
        this.setState(prevState => {
            return {
                controls: {
                    ...prevState.controls,
                    [key]: {
                        ...prevState.controls[key],
                        value: event,
                        touched: true,
                        valid: validate(event, prevState.controls[key].validate, key)
                    }
                }
            };
        });
        if (event) {
            if (key == 'manufacturer') {
                this.getModelList(event);
                if (event.toLowerCase() == 'other') {
                    this.showModal(true, 'both');
                }
            }
            if (key == 'model') {
                if (event.toLowerCase() == 'other') {
                    this.showModal(true, 'model');
                }
            }
        }

    };

    showModal = (val, data) => {
        this.setState(prevState => {
            return {
                ...prevState,
                otherModal: val,
                showOptions: data
            };
        });
    }

    wheelChange = (wheel) => {
        this.setState(prevState => {
            return {
                controls: {
                    ...prevState.controls,
                    type: {
                        ...prevState.controls.type,
                        value: wheel
                    }
                }
            };
        });
        this.getManufecturer({ type: wheel })
    }

    addVechile = async () => {
        let err = false;
        let data = {};
        Object.keys(this.state.controls).map(key => {
            if (!this.state.controls[key].value) {
                err = true;
            }
            data[key] = this.state.controls[key].value;
        });

        if (!err) {
            this.showLoading(true);
            if (this.state.image) {
                data.image = this.state.image;
            }
            let vehiclesList = [];
            vehiclesList = this.state.allVehicles;
            vehiclesList = vehiclesList.map(vehicle => {
                return {
                    power: vehicle.power,
                    pin: vehicle.pin,
                    model: vehicle.model,
                    manufacturer: vehicle.manufacturer,
                    type: vehicle.type,
                }
            })
            vehiclesList.push(data);

            console.log(vehiclesList)
            try {
                let res = await axios.put(`${url}/user/update-profile`, { vehicles: vehiclesList });
                if (res) {
                    this.showLoading(false);
                    msg('Vehicle is added successfully.');
                    if (this.props.route.params.goTo == 'MyVehicle') {
                        this.props.route.params.refresh();
                    }
                    this.props.navigation.navigate(this.props.route.params.goTo);
                }
            }
            catch (err) {
                if (err) {
                    this.showLoading(false);
                    errMsg(err.response);
                }
            }
        }
        else {
            errMsg('All fields are required');
        }

    }

    showLoading = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                loading: val
            }
        })
    }

    goBack = () => {
        // console.log('go-back')
        if (this.props.route.params.goTo == 'Tab') {
            this.props.navigation.navigate(this.props.route.params.goTo);
        }
        else {
            this.props.navigation.goBack()
        }
    }

    onFocus() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    onLeave() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }


    handleBackButtonClick = () => {
        if (disableRoute.indexOf(this.currentRouteName) >= 0) {
            count += 1;
            if (count == 1) {
                ToastAndroid.show('Press again for exit!', ToastAndroid.SHORT);
            }
            else if (count == 2) {
                count = 0;
                BackHandler.exitApp();
            }
            return true;
        }
        else {
            return false;
        }

    }

    openCamera = () => {
        const options = {
            title: 'Select Picture',
            // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let source = {};
                if (Platform.OS === 'android') {
                    source = { uri: response.uri }
                } else {
                    source = { uri: response.uri.replace('file://', '') }
                }

                this.uploadImage(source);
            }
        });
    }

    uploadImage = async (source) => {
        this.showLoading(true);
        let formData = new FormData();
        formData.append('images', {
            uri: source.uri,
            type: source.type ? source.type : 'image/png',
            name: 'image'
        });

        try {
            let res = await axios.post(`${url}/common/images`,
                formData,
                {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data'
                    }
                }
            );
            if (res) {
                this.showLoading(false);
                this.setState(prev => {
                    return {
                        ...prev,
                        image: res['data']['data']['files'][0].path
                    }
                })
            }
        }
        catch (err) {
            if (err) {
                this.showLoading(false);
                errMsg(err['response']);
            }
        }
    }

    removeImg = () => {
        this.setState(prev => {
            return {
                ...prev,
                image: null
            }
        })
    }

    closeModal = (val) => {
        this.showModal(false, null);
        if (val == 'both') {
            this.setState(prev => {
                return {
                    ...prev,
                    controls: {
                        ...prev.controls,
                        manufacturer: {
                            ...prev.controls.manufacturer,
                            value: prev.manufactureList[0]
                        }
                    }
                }
            })

            this.getModelList(this.state.manufactureList[0]);
        }
        else {
            this.setState(prev => {
                return {
                    ...prev,
                    controls: {
                        ...prev.controls,
                        model: {
                            ...prev.controls.model,
                            value: prev.modelList[0]
                        }
                    }
                }
            })
        }
    }

    modelData = (data) => {
        if (data.model) {
            let models = this.state.modelList;
            let otherModalList = this.state.otherModalList;
            models.push({ model: data.model });
            otherModalList.push({ model: data.model });
            this.setState(prev => {
                return {
                    ...prev,
                    modelList: models,
                    otherModalList: otherModalList,
                    controls: {
                        ...prev.controls,
                        model: {
                            ...prev.controls.model,
                            value: data.model
                        }
                    }
                }
            })
        }
        if (data.manufacturer) {
            let manufecturers = this.state.manufactureList;
            manufecturers.push(data.manufacturer);
            this.setState(prev => {
                return {
                    ...prev,
                    manufactureList: manufecturers,
                    controls: {
                        ...prev.controls,
                        manufacturer: {
                            ...prev.controls.manufacturer,
                            value: data.manufacturer
                        }
                    }
                }
            })
        }

        this.showModal(false, null);
    }

    render() {
        return (
            <ScrollView stickyHeaderIndices={[0]}
                showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="handled">
                <CustomHeader title="Add Vehicle" backBtn={true} rightIcon={false} goBack={this.goBack}></CustomHeader>
                <View style={styles.view}>
                    <View style={styles.tab}>
                        <TouchableOpacity style={[styles.tabContent, (this.state.controls.type.value == '2 Wheeler') ? styles.active : null]} onPress={() => this.wheelChange('2 Wheeler')}>
                            <Para style={[(this.state.controls.type.value == '2 Wheeler') ? styles.activePara : null]}>2 Wheeler</Para>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.tabContent, (this.state.controls.type.value == '3 Wheeler') ? styles.active : null]} onPress={() => this.wheelChange('3 Wheeler')}>
                            <Para style={[(this.state.controls.type.value == '3 Wheeler') ? styles.activePara : null]}>3 Wheeler</Para>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.tabContent, (this.state.controls.type.value == '4 Wheeler') ? styles.active : null]} onPress={() => this.wheelChange('4 Wheeler')}>
                            <Para style={[(this.state.controls.type.value == '4 Wheeler') ? styles.activePara : null]}>4 Wheeler</Para>
                        </TouchableOpacity>
                    </View>
                    <View style={{ padding: 8 }}>
                        <View style={styles.dropdowns}>
                            <View style={styles.manufacture}>
                                <Para bold={true}>Vehicle Manufacture</Para>
                                <Item picker style={styles.pickerItem}>
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={styles.picker}
                                        // textStyle = {{fontSize: 17,color:'yellow',fontFamily:fontFamily.bold}}
                                        placeholder="Select Manufacture"
                                        placeholderStyle={{ color: "#808080", fontFamily: fontFamily.bold }}
                                        placeholderIconColor="#808080"
                                        selectedValue={this.state.controls.manufacturer.value}
                                        onValueChange={(e) => this.changeTextHandler(e, 'manufacturer')}
                                    >
                                        {
                                            this.state.manufactureList.map((manu, i) => {
                                                return <Picker.Item label={manu} key={i} value={manu} />
                                            })
                                        }
                                    </Picker>
                                </Item>
                            </View>
                            <View style={styles.model}>
                                <Para bold={true}>Vehicle Model</Para>
                                <Item picker style={styles.pickerItem}>
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={styles.picker}
                                        // textStyle = {{fontSize: 17,color:'yellow',fontFamily:fontFamily.bold}}
                                        placeholder="Select Model"
                                        placeholderStyle={{ color: "#808080", fontFamily: fontFamily.bold }}
                                        placeholderIconColor="#808080"
                                        selectedValue={this.state.controls.model.value}
                                        onValueChange={(e) => this.changeTextHandler(e, 'model')}
                                    >
                                        {
                                            this.state.modelList.map((model, i) => {
                                                return <Picker.Item label={model.model} key={i} value={model.model} />
                                            })
                                        }
                                    </Picker>
                                </Item>
                            </View>
                            <View style={styles.model}>
                                <Para bold={true}>Power</Para>
                                <Item picker style={styles.pickerItem}>
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={styles.picker}
                                        // textStyle = {{fontSize: 17,color:'yellow',fontFamily:fontFamily.bold}}
                                        placeholder="Select Power"
                                        placeholderStyle={{ color: "#808080", fontFamily: fontFamily.bold }}
                                        placeholderIconColor="#808080"
                                        selectedValue={this.state.controls.power.value}
                                        onValueChange={(e) => this.changeTextHandler(e, 'power')}
                                    >

                                        {
                                            this.state.powersArray ?
                                                this.state.powersArray.map((power, i) => {
                                                    return <Picker.Item label={power} value={power} key={i} />
                                                })
                                                : null
                                        }
                                    </Picker>
                                </Item>
                            </View>
                            <View style={styles.model}>
                                <Para bold={true}>Pin</Para>
                                <Item picker style={styles.pickerItem}>
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={styles.picker}
                                        // textStyle = {{fontSize: 17,color:'yellow',fontFamily:fontFamily.bold}}
                                        placeholder="Select Pin"
                                        placeholderStyle={{ color: "#808080", fontFamily: fontFamily.bold }}
                                        placeholderIconColor="#808080"
                                        selectedValue={this.state.controls.pin.value}
                                        onValueChange={(e) => this.changeTextHandler(e, 'pin')}
                                    >

                                        {
                                            this.state.pinsArray ?
                                                this.state.pinsArray.map((pin, i) => {
                                                    return <Picker.Item label={pin} value={pin} key={i} />
                                                })
                                                : null
                                        }
                                    </Picker>
                                </Item>
                            </View>
                        </View>
                        <View style={styles.uploadDiv}>
                            {
                                !this.state.image ?
                                    <TouchableOpacity onPress={this.openCamera} style={commonStyle.alignCenter}>
                                        <Para bold={true} style={styles.uploadText}>+ Upload your vehicle image</Para>
                                        <Image source={require('../../../assets/images/add-vechile.png')} style={[styles.carImage, commonStyle.img]} />
                                    </TouchableOpacity>
                                    : <View>
                                        <View style={styles.imgDiv}>
                                            <Image source={{ uri: `${imageUrl}${this.state.image}` }} style={[styles.carImg, commonStyle.img]} />

                                        </View>
                                        <View style={[commonStyle.flexRow, commonStyle.textCenter, styles.remove]}>
                                            <TouchableOpacity onPress={this.removeImg}><Para bold={true} style={commonStyle.blue}>Remove</Para></TouchableOpacity>
                                            <TouchableOpacity onPress={this.openCamera}><Para bold={true} style={[commonStyle.blue, { marginLeft: 15 }]}>Change</Para></TouchableOpacity>
                                        </View>
                                    </View>
                            }
                        </View>
                        <TouchableOpacity onPress={this.addVechile}>
                            <Button style={commonStyle.button}>
                                {
                                    this.state.loading ?
                                        <Spinner color='white' size={40}></Spinner>
                                        :
                                        <Para bold={true} style={commonStyle.white}>Add Vechile</Para>

                                }
                            </Button>
                        </TouchableOpacity>
                    </View>
                </View>
                <Modal isVisible={this.state.otherModal} style={styles.modal} animationOut='slideOutDown' animationIn='slideInUp' backdropOpacity={0.2}>
                    <Root>
                        <Container style={{ backgroundColor: 'transparent', display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                            <View style={styles.modalView}>
                                <View style={styles.modalContent}>
                                    {/* <Delete title="Cancel Booking" ></Delete> */}
                                    <OtherModel options={this.state.showOptions} modalData={this.modelData} closeModal={this.closeModal}></OtherModel>
                                </View>
                            </View>
                        </Container>
                    </Root>
                </Modal>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        padding: 17
    },
    tab: {
        display: "flex",
        justifyContent: 'space-between',
        flexDirection: 'row',
        padding: 8
    },
    tabContent: {
        backgroundColor: 'white',
        borderRadius: 10,
        textAlign: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 8,
        paddingHorizontal: 10,
        paddingVertical: 8,
    },
    dropdowns: {
        paddingTop: 20
    },
    uploadDiv: {
        alignItems: 'center',
        paddingVertical: 20
    },
    uploadText: {
        // marginBottom: 15,
        color: '#1371EF'
    },
    button: {
        backgroundColor: '#E7EFF9',
        elevation: 0,
        borderRadius: 10,
        paddingHorizontal: 15,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50

    },
    buttonText: {
        color: '#808080'
    },
    activePara: {
        color: 'white'
    },
    active: {
        backgroundColor: '#1371EF'
    },
    carImage: {
        // height:100,
        width: '70%',
    },
    picker: {
        fontFamily: fontFamily.bold,

    },
    pickerItem: {
        backgroundColor: '#E7EFF9',
        borderRadius: 10,
        borderColor: 'transparent',
        marginTop: 10,
        marginBottom: 20
    },
    carImg: {
        // maxWidth: '100%',
        // maxHeight: 100,
        // height: '100%',
        // width: '100%',
        // borderRadius: 10,
        width: '100%',
        height: '100%',
        borderRadius: 10
    },
    imgDiv: {
        height: 170,
        width: Dimension.width,
        // backgroundColor:'red'
    },
    remove: {
        marginBottom: 20,
        marginTop: 15
    },
    modalContent: {
        paddingTop: 20,
        paddingBottom: 10
    },
    modalView: {
        backgroundColor: 'white',
        borderRadius: 10,
        position: 'relative',
        width: '100%',
        // width:Dimension.width,
        // flex:1
        // left:0
        // position:'absolute',
        // bottom:0
    },
    modal: {
        // margin: 0, // This is the important style you need to set
        // alignItems: undefined,
        // justifyContent: 'flex-end',
    },

})
