import React, { Component } from 'react'
import { Text, StyleSheet, View, ScrollView, Image, TouchableOpacity } from 'react-native'
import CustomHeader from '../../components/Shared/CustomHeader/CustomHeader'
import Para from '../../components/Shared/Text/Para'
import { commonStyle } from '../../utils/Style/Style'
import { Icon, Button, Root, Container } from 'native-base'
import axios from 'axios';
import { url } from '../../utils/BaseUrl/BaseUrl'
import AddFeedback from '../../components/AddFeedback/AddFeedback'
import Modal from 'react-native-modal';
import { msg, errMsg } from '../../utils/Toaster/Toaster'

export default class Faq extends Component {

    state = {
        questions: [],
        showFeedback: false
    }

    componentDidMount = () => {
        this.getFaq();
    }


    getFaq = async () => {
        try {
            let res = await axios.get(`${url}/constant/faq`);
            if (res) {
                this.setState((prev) => {
                    return {
                        ...prev,
                        questions: res['data']['data']['value']
                    }
                });
            }
        }
        catch (err) {
            if (err) {
                console.log(err.response)
            }
        }
    }

    feedbackModal = (val) => {
        this.setState((prev) => {
            return {
                ...prev,
                showFeedback: val
            }
        });
    }

    addFeedback = async (data) => {
        console.log(data)
        try {
            let res = await axios.post(`${url}/feedback`, data);
            if (res) {
                msg(res['data']['msg']);
                this.feedbackModal(false);
            }
        }
        catch (err) {
            if (err) {
                errMsg(err.response);
            }
        }
    }

    goback = () => {
        this.props.navigation.goBack()
    }

    render() {
        return (
            <ScrollView stickyHeaderIndices={[0]}
                showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="handled">
                <CustomHeader title="FAQs & Help" backBtn={true} goBack={this.goback}></CustomHeader>
                <View style={styles.faq}>
                    <Para bold={true}>Customer Support</Para>
                    <View style={[commonStyle.flexRow, commonStyle.alignCenter, styles.supportNumber]}>
                        <View>
                            <Para style={commonStyle.blue}>1800 2540 1245</Para>
                            <Para style={commonStyle.blue}>Aasd@chargebizz.com</Para>
                        </View>
                        <View>
                            <Image source={require('../../../assets/images/support.png')} style={[commonStyle.img, styles.faqImg]} />
                        </View>
                    </View>
                    <View style={styles.questionTitle}>
                        <View style={[commonStyle.textCenter]}>
                            <Button style={styles.feedbackBtn} onPress={() => this.feedbackModal(true)}><Para bold={true} style={[commonStyle.blue]}>Give Feedback</Para></Button>
                        </View>
                    </View>
                    <View style={styles.question}>
                        <Para bold={true}>FAQs</Para>
                        {
                            this.state.questions.length > 0 ?
                                this.state.questions.map((question, i) => {
                                    return <View style={styles.queAns} key={i}>
                                        <Para style={commonStyle.blue}>{question.question}</Para>
                                        <Para style={[commonStyle.gray, styles.answer]}>{question.answer}</Para>
                                    </View>
                                })
                                : null
                        }
                    </View>
                </View>
                <Modal isVisible={this.state.showFeedback} style={styles.modal} animationType="slide" animationOut='slideOutDown' animationIn='slideInUp' backdropOpacity={0.2}>
                    <Root>
                        <Container style={{ backgroundColor: 'transparent', display: 'flex', flexDirection: 'row', alignItems: 'flex-end' }}>
                            <View style={styles.modalView}>
                                <View style={styles.closeBtn}>
                                    <TouchableOpacity onPress={() => this.feedbackModal(false)}>
                                        <Image source={require('../../../assets/images/close.png')} style={[commonStyle.img, styles.closeIcon]} />
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.modalContent}>
                                    <AddFeedback addFeedback={this.addFeedback}></AddFeedback>
                                </View>
                            </View>
                        </Container>
                    </Root>
                </Modal>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    faq: {
        padding: 25
    },
    supportNumber: {
        justifyContent: 'space-between'
    },
    faqImg: {
        height: 60,
        width: 60
    },
    answer: {
        marginTop: 10
    },
    queAns: {
        marginTop: 20
    },
    questionTitle: {
        marginVertical: 20
    },
    feedbackBtn: {
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: 'white',
        height: 60
    },
    modal: {
        margin: 0, // This is the important style you need to set
        // alignItems: undefined,
        justifyContent: 'flex-end',
    },
    closeBtn: {
        position: 'absolute',
        right: 15,
        top: -28,
        zIndex: 10,
        height: 60,
        width: 60,
    },
    closeIcon: {
        width: '100%',
        height: '100%'
    },
    modalContent: {
        paddingTop: 20,
        paddingBottom: 10
    },
    modalView: {
        backgroundColor: 'white',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        position: 'relative',
        width:'100%'
        // width:Dimension.width,
        // flex:1
        // left:0
        // position:'absolute',
        // bottom:0
    },
})
