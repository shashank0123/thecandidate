import React, { Component } from 'react'
import { StyleSheet, Image, View, TouchableOpacity, Linking } from 'react-native'
import Para from '../../components/Shared/Text/Para'
import CustomInput from '../../components/Shared/CustomInput/CustomInput';
import { Icon, Button, Spinner } from 'native-base';
import axios from 'axios';
import { msg, errMsg } from '../../utils/Toaster/Toaster';
import validate from '../../utils/Validation/Validation';
import { url, tcUrl } from '../../utils/BaseUrl/BaseUrl';
import { commonStyle } from '../../utils/Style/Style';
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';
import { saveToken, saveLoginCount } from '../../utils/Token/Token';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-community/async-storage';
import Dimension from '../../utils/Dimension/Dimension';

export default class Login extends Component {

    state = {
        controls: {

            phoneNumber: {
                value: '',
                touched: false,
                type: 'number',
                valid: {
                    isValid: false,
                    text: 'Phone Number is required'
                },
                validate: {
                    required: true,
                    isPhone: true
                }
            },
        },
        blockModel: this.props.blocked,
        loading: false
    }

    componentDidMount = () => {
    }

    changeTextHandler = (event, key) => {
        this.setState(prevState => {
            return {
                controls: {
                    ...prevState.controls,
                    [key]: {
                        ...prevState.controls[key],
                        value: (prevState.controls[key].type == 'number') ? event.replace(/[^0-9]/g, '') : event,
                        touched: false,
                        valid: validate(event, prevState.controls[key].validate, key)
                    }
                }
            };
        });
    };

    login = async () => {
        if (this.state.controls.phoneNumber.valid.isValid) {
            this.showLoading(true);
            let data = {
                phoneNumber: this.state.controls.phoneNumber.value
            }
            try {
                let res = await axios.post(`${url}/user/login-mobile`, data);
                if (res) {
                    console.log(res, 'response');
                    this.showLoading(false);
                    msg(res['data']['msg']);
                    this.props.navigateToOtp({ ...{ phoneNumber: data.phoneNumber, name: '' }, ...res['data']['data'] });
                }
            }
            catch (err) {
                if (err) {
                    console.log('aahj error', err)
                    this.showLoading(false);
                    errMsg(err['response']);
                    if (err.response && err.response['data'] && err.response['data']['data']) {
                        if (err.response['data']['data'].blocked) {
                            this.blockPopup(true);
                        }
                    }
                }
            }
        }
        else {
            this.showErr('phoneNumber');
        }
        console.log(this.state.controls);

    }

    showErr = (key) => {
        this.setState(prevState => {
            return {
                ...prevState,
                controls: {
                    ...prevState.controls,
                    [key]: {
                        ...prevState.controls[key],
                        touched: true,
                        // valid: {
                        //     ...prevState.controls[key].valid,
                        //     text: "Phone Number is required"
                        // }
                    }
                }
            }
        })
    }

    showLoading = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                loading: val
            }
        })
    }



    fbAuth = () => {
        LoginManager.logInWithPermissions(['public_profile']).then(
            (result) => {
                if (result.isCancelled) {
                    console.log('Login was cancelled');
                } else {
                    AccessToken.getCurrentAccessToken().then((data) => {
                        const { accessToken } = data
                        this.initUser(accessToken);
                    })
                }
            },
            (err) => {
                console.log('Login failed with error: ' + error);
            }
        );
    }

    fbLogout() {
        var current_access_token = '';
        AccessToken.getCurrentAccessToken().then((data) => {
            current_access_token = data.accessToken.toString();
        }).then(() => {
            let logout =
                new GraphRequest(
                    "me/permissions/",
                    {
                        accessToken: current_access_token,
                        httpMethod: 'DELETE'
                    },
                    (error, result) => {
                        if (error) {
                            console.log('Error fetching data: ' + error.toString());
                        } else {
                            LoginManager.logOut();
                        }
                    });
            new GraphRequestManager().addRequest(logout).start();
        })
            .catch(error => {
                console.log(error)
            });
    }

    initUser = (token) => {
        fetch('https://graph.facebook.com/v2.5/me?fields=email,name&access_token=' + token)
            .then((response) => response.json())
            .then((user) => {
                this.socialLogin(user.id, 'Facebook');

            })
            .catch((err) => {
                console.log(err);
            })
    }


    googleAuth = () => {
        GoogleSignin.signIn()
            .then((user) => {
                this.socialLogin(user.user.id, 'Google');
            })
            .catch((err) => {
                console.log('WRONG SIGNIN', err);
            })
            .done();
    }


    googleSignOut = () => {
        GoogleSignin.signOut()
            .then((user) => {

            })
            .catch((err) => {
                console.log('WRONG SIGNIN', err);
            })
            .done();
    }

    setupGoogleSignin = async () => {
        try {
            await GoogleSignin.hasPlayServices({ autoResolve: true, showPlayServicesUpdateDialog: true });
            await GoogleSignin.configure({
                // iosClientId: settings.iOSClientId,
                webClientId: '1015927727812-dbvgn6duaq7dpfsi6v9r2q1r25bbdkk0.apps.googleusercontent.com',
                offlineAccess: false
            });

            // GoogleSignin.getCurrentUser().then(res =>{
            //     console.log(res);
            // },(err) =>{
            //     console.log(err)
            // })
            // // console.log(user);
        }
        catch (err) {
            console.log("Google signin error", err, err.code, err.message);
        }
    }


    socialLogin = async (socialId, type) => {
        let token = await AsyncStorage.getItem('fcmToken');

        let data = {
            socialId: socialId,
            type: type,
            deviceToken: token
        }
        try {
            let res = await axios.post(`${url}/user/login-social`, data);
            if (res) {
                console.log(res['data'])
                saveToken(res['data']['data']['token']);
                saveLoginCount('yes');
                msg(res['data']['msg']);
                this.props.afterOtp('Tab', {});
            }
        }
        catch (err) {
            if (err) {
                if (type == 'Google') {
                    this.googleSignOut();
                }
                else {
                    this.fbLogout();
                }
                errMsg(err['response']);
                if (err.response && err.response['data'] && err.response['data']['data']) {
                    if (err.response['data']['data'].blocked) {
                        this.blockPopup(true);
                    }
                }
            }
        }
    }

    openTC = () => {
        Linking.openURL(tcUrl);
    }

    blockPopup = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                blockModel: val
            }
        })

        if (!val) {
            this.props.showBlocked(false);
        }
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.formDiv}>
                    <View style={styles.nameDiv}>
                        <Para style={[styles.nameFont, styles.hello]}>Hello</Para>
                        <Para extraBold={true} style={styles.nameFont}>Environmentalist !</Para>
                    </View>
                    <View style={styles.inputBox}>

                        <View style={styles.inputDiv}>
                            <View style={styles.input}>
                                <CustomInput placeholder="Phone" style={styles.custom}
                                    placeholderTextColor="#9FBDE5"
                                    onChangeText={(event) => this.changeTextHandler(event, 'phoneNumber')}
                                    value={this.state.controls.phoneNumber.value}
                                    valid={this.state.controls.phoneNumber.valid}
                                    keyboardType="numeric"
                                    maxLength={10}
                                    onSubmitEditing={this.login}
                                ></CustomInput>

                            </View>
                            <Icon name='call' style={styles.icon} />

                        </View>
                        <View>
                            {
                                (!this.state.controls.phoneNumber.valid.isValid && this.state.controls.phoneNumber.touched) ?
                                    <Para style={{ color: 'white', marginTop: 8, fontSize: 14 }} bold={true}>{this.state.controls.phoneNumber.valid.text}</Para>
                                    : null
                            }
                        </View>
                    </View>
                    <View style={styles.socialIconDiv}>
                        <TouchableOpacity onPress={this.googleAuth}>
                            <Image
                                source={require("../../../assets/images/google.png")}
                                style={styles.socialIcon}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.fbAuth}>
                            <Image
                                source={require("../../../assets/images/fb.png")}
                                style={styles.socialIcon}
                            />
                        </TouchableOpacity>
                    </View>

                </View>
                <View style={styles.submitDiv}>
                    <View>
                        <TouchableOpacity onPress={this.openTC}><Para bold={true} style={styles.submitText}>Terms & Conditions</Para></TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={this.login}>

                        {
                            this.state.loading ?
                                <Spinner color='white' size={40}></Spinner>
                                :
                                <Image
                                    source={require("../../../assets/images/forward.png")}
                                    style={commonStyle.forward}
                                />
                        }
                    </TouchableOpacity>
                </View>
                <Modal isVisible={this.state.blockModel} style={styles.modal} animationOut='slideOutDown' animationIn='slideInUp' backdropOpacity={0.2}>
                    <View style={styles.modalView}>
                        <View style={styles.modalContent}>
                            <Para bold={true} style={styles.title}>Your account is blocked!</Para>
                            <Para bold={true} style={styles.support}>Contact with support</Para>
                            <View>
                                <Para style={commonStyle.blue}>1800 2540 1245</Para>
                                <Para style={commonStyle.blue}>Aasd@chargebizz.com</Para>
                            </View>
                            <View style={[commonStyle.flexEnd]}>
                                <Button style={styles.cancelBtn} onPress={() => this.blockPopup(false)}><Para bold={true} style={commonStyle.white}>Close</Para></Button>
                            </View>
                        </View>
                    </View>
                </Modal>
                

            </View>
        )
    }
}

const styles = StyleSheet.create({

    formDiv: {
        paddingVertical: 70,
    },
    nameDiv: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 40
    },
    nameFont: {
        fontSize: 25,
        color: 'white'
    },
    hello: {
        marginRight: 10
    },
    inputBox: {
        marginBottom: 20,
    },
    inputDiv: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#0F5ABF',
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 3,
        paddingHorizontal: 20,
        paddingVertical: 3

    },
    input: {
        // backgroundColor: 'red',
        flex: 1,
        marginRight: 15
    },
    icon: {
        color: '#9FBDE5',
        fontSize: 22
    },
    custom: {
        color: 'white'
    },
    socialIconDiv: {
        flexDirection: 'row'
    },
    socialIcon: {
        marginRight: 10,
        marginTop: 5,
        height: 50,
        width: 50.

    },
    submitDiv: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between'
    },
    submitText: {
        color: 'white',
        fontSize: 17
    },
    modalContent: {
        padding: 25
    },
    modalView: {
        backgroundColor: 'white',
        borderRadius: 10,
        position: 'relative'
        // width:Dimension.width,
        // flex:1
        // left:0
        // position:'absolute',
        // bottom:0
    },
    modal: {

        // alignItems: undefined,
        // justifyContent: 'flex-end',
    },
    title: {
        fontSize: 20
    },
    cancelBtn: {
        paddingHorizontal: 15,
        borderRadius: 5,
        marginTop: 20
        // backgroundColor:
    },
    support: {
        marginTop: 15,
        marginBottom: 5
    }

})
