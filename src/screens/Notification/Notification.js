import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import NotificationCard from '../../components/NotificationCard/NotificationCard'
import { ScrollView } from 'react-native-gesture-handler'
import CustomHeader from '../../components/Shared/CustomHeader/CustomHeader'
import NoContent from '../../components/NoContent.js/NoContent'
import { commonStyle } from '../../utils/Style/Style'
import Dimension from '../../utils/Dimension/Dimension'
import BookingSkeleton from '../../components/Skeleton/BookingSkeleton'
import Axios from 'axios'
import { url } from '../../utils/BaseUrl/BaseUrl'

export default class Notification extends Component {

    state = {
        notifications: [],
        loading: false,
        skeleton: ['', '', '', '']
    }

    componentDidMount = () => {
        this.getNotification();
    }

    getNotification = async () => {
        this.showLoading(true);
        try {
            let res = await Axios.get(`${url}/notification`);
            if (res) {
                // console.log(res['data']['data'][0].notificationData.notification,'new data')
                this.showLoading(false);
                this.setState(prev => {
                    return {
                        ...prev,
                        notifications: res['data']['data'],
                    }
                })
            }
        }
        catch (err) {
            if (err) {
                this.showLoading(false);
            }
        }
    }

    showLoading = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                loading: val,
            }
        })
    }

    goback = () => {
        this.props.navigation.goBack()
    }

    navigate = () => {
        this.props.navigation.navigate('Stations');
    }

    render() {
        return (
            <ScrollView stickyHeaderIndices={[0]}
                showsVerticalScrollIndicator={false}>
                <CustomHeader title="Notification" backBtn={true} rightIcon={false} goBack={this.goback}></CustomHeader>
                <View style={styles.notificationView}>
                    {
                        this.state.loading ?
                            this.state.skeleton.map((skel, i) => {
                                return <BookingSkeleton key={i}></BookingSkeleton>
                            })
                            :
                            this.state.notifications.length > 0 ?
                                this.state.notifications.map((noti, i) => {
                                    return <NotificationCard noti={noti.notificationData ? noti.notificationData.notification : ''} key={i} navigate={this.navigate}></NotificationCard>
                                })
                                : <View style={styles.noContent}>
                                    <NoContent title="No Notifications yet !" image={<Image source={require('../../../assets/images/no-notification.png')} style={[styles.notificationImg, commonStyle.img]} />} para="Check this section for updates, news ond offers."></NoContent>
                                </View>

                    }
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    notificationView: {
        padding: 25,
    },
    notificationImg: {
        height: 100
    },
    noContent: {
        height: Dimension.height - 100,
        // backgroundColor:'red'
    }
})
