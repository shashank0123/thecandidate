import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, TouchableOpacity, BackHandler, ToastAndroid } from 'react-native';
import Swiper from 'react-native-swiper'
import SwipingScreen from '../../components/SwipingScreen/SwipingScreen';
import dimension from '../../utils/Dimension/Dimension';
import Para from '../../components/Shared/Text/Para';
import { disableRoute } from '../../utils/Constants/Constatnts';
import { commonStyle } from '../../utils/Style/Style';
import WebView from 'react-native-webview';
var count = 0;

export default class Onbording extends Component {

    constructor(props) {
        super(props);
        this.currentRouteName = 'onboarding'
        this.props.navigation.addListener('blur', () => {
            this.onLeave();
        });
        this.props.navigation.addListener('focus', () => {
            this.onFocus();
        });

    }

    state = {
        index: 0
    }

    changeIndex = (index) => {
        this.setState(prev => {
            return {
                ...prev,
                index: index
            }
        })
    }

    navigate = () => {
        this.props.navigation.navigate('Auth', {})
    }

    onFocus() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    onLeave() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }


    handleBackButtonClick = () => {
        if (disableRoute.indexOf(this.currentRouteName) >= 0) {
            count += 1;
            if (count == 1) {
                ToastAndroid.show('Press again for exit!', ToastAndroid.SHORT);
            }
            else if (count == 2) {
                count = 0;
                BackHandler.exitApp();
            }
            return true;
        }
        else {
            return false;
        }

    }


    state = {
        screens: [
            {
                image: <Image source={require('../../../assets/images/first.png')} style={styles.image} />,
                head: 'Locate EV Station',
                para: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vel venenatis id lacus. Tincidunt congue proin tellus penatibus '
            },
            {
                image: <Image source={require('../../../assets/images/second.png')} style={styles.image} />,
                head: 'Book EV Station',
                para: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vel venenatis id lacus. Tincidunt congue proin tellus penatibus '
            },
            {
                image: <Image source={require('../../../assets/images/third.png')} style={styles.image} />,
                head: 'Start Charging',
                para: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vel venenatis id lacus. Tincidunt congue proin tellus penatibus ',
                showLogin: true
            }
        ]
    }

    render() {
        return (
            <>
                <Swiper style={styles.wrapper}
                    showsButtons={true}
                    loop={false}
                    nextButton={<Image source={require('../../../assets/images/next.png')} style={styles.icon} />}
                    prevButton={< Image source={require('../../../assets/images/previous.png')} style={styles.icon} />}
                    buttonWrapperStyle={styles.buttonWrap}
                    onIndexChanged={(index) => this.changeIndex(index)}
                >
                    {
                        this.state.screens.map((screen, i) => {
                            return <SwipingScreen screen={screen} key={i}></SwipingScreen>

                        })
                    }
                </Swiper >
                {
                    this.state.index == 2 ?
                        <TouchableOpacity style={styles.signupBtn} onPress={this.navigate}>
                            <Para style={commonStyle.gray}>Sign up</Para>
                        </TouchableOpacity>
                        : null
                }
              
            </>
        )
    }
}

const styles = StyleSheet.create({
    image: {
        height: dimension.width - 50,
        width: dimension.width - 50
    },
    icon: {

    },
    buttonWrap: {
        alignItems: 'flex-end',
        paddingBottom: 23
    },
    signupBtn: {
        position: 'absolute',
        bottom: 25,
        right: 13
    }
})
