import React, { Component } from 'react'
import { StyleSheet, Image, View, TouchableOpacity } from 'react-native'
import Para from '../../components/Shared/Text/Para'
import CustomInput from '../../components/Shared/CustomInput/CustomInput';
import { Icon, Spinner } from 'native-base';
import { commonStyle } from '../../utils/Style/Style';
import OtpInputs from '../../components/OtpInputs/OtpInputs';
import { errMsg, msg } from '../../utils/Toaster/Toaster';
import axios from 'axios';
import { url } from '../../utils/BaseUrl/BaseUrl';
import { saveToken, saveLoginCount } from '../../utils/Token/Token';
import AsyncStorage from '@react-native-community/async-storage';
// import RNOtpVerify from 'react-native-otp-verify';

export default class Otp extends Component {

    state = {
        controls: {

            otp: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                    required: true,
                }
            },
            phoneNumber: {
                value: this.props.user.phoneNumber,
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                    required: true,
                    isPhone: true
                }
            },
        },
        loading: false
    }

    componentDidMount = () => {
        //    this.getHash();
        //    this.startListeningForOtp();
    }

    changeTextHandler = (event, key) => {
        this.setState(prevState => {
            return {
                controls: {
                    ...prevState.controls,
                    [key]: {
                        ...prevState.controls[key],
                        value: event,
                        touched: true,
                        valid: validate(event, prevState.controls[key].validate, key)
                    }
                }
            };
        });
    };

    otpVerify = async () => {
        this.showLoading(true);
        let token = await AsyncStorage.getItem('fcmToken');

        let data = {
            _id: this.props.user._id,
            otp: this.state.controls.otp.value,
            deviceToken: token
        }

        try {
            let res = await axios.post(`${url}/user/verify-otp`, data);
            if (res) {
                // msg(res['data']['msg']);
                this.showLoading(false);
                if (res['data']['data']) {
                    saveToken(res['data']['data']['token'], { name: res['data']['data']['name'], phoneNumber: res['data']['data']['phoneNumber'] });
                    saveLoginCount('yes');
                    if (this.props.active == 'login') {
                        this.props.afterOtp('Tab', {});
                    }
                    else {
                        this.props.afterOtp('AddVechile', { goTo: 'Tab' });
                    }
                }
            }
        }
        catch (err) {
            if (err) {
                this.showLoading(false);
                errMsg(err['response']);
            }
        }
    }

    showLoading = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                loading: val
            }
        })
    }

    getOtp = (otp) => {
        this.setState(prev => {
            return {
                ...prev,
                controls: {
                    ...prev.controls,
                    otp: {
                        ...prev.controls.otp,
                        value: otp
                    }
                }
            }
        })
    }

    resendOtp = async () => {
        let data = {
            _id: this.props.user._id,
        }
        try {
            let res = await axios.post(`${url}/user/resend-otp`, data);
            if (res) {
                msg(res['data']['msg']);
            }
        }
        catch (err) {
            console.log(err, err.response);
            if (err) {
                errMsg(err['response']);
            }
        }
    }

    // getHash = () =>
    //     RNOtpVerify.getHash()
    //         .then(console.log)
    //         .catch(console.log);


    // startListeningForOtp = () => {
    //     RNOtpVerify.getOtp()
    //         .then(p => {
    //             console.log(p);
    //             RNOtpVerify.addListener((message) => {
    //                 try {
    //                     if (message) {
    //                         console.log(message,'message')
    //                         const otp = /(\d{4})/g.exec(message)[1];
    //                         console.log(otp, 'autofetch')
    //                         RNOtpVerify.removeListener();
    //                         Keyboard.dismiss();
    //                     }
    //                 }
    //                 catch (err) {
    //                     console.log(err,'error')
    //                 }
    //             })
    //         })
    //         .catch(p => console.log(p, 'err'));
    // }

    componentWillUnmount() {
        // RNOtpVerify.removeListener();
    }

    wrongNumber = () => {
        this.props.tabChange(this.props.active);
    }

    render() {
        return (
            <View>
                <View style={styles.formDiv}>
                    <View style={styles.nameDiv}>
                        <Para style={[styles.nameFont, styles.hello]}>Hello</Para>
                        <Para extraBold={true} style={styles.nameFont}>{this.props.user ? this.props.user.name ? this.props.user.name : 'Environmentalist' : 'Environmentalist'} !</Para>
                    </View>
                    <Para style={[styles.para, { marginBottom: 10 }]}>Please enter OTP sent to</Para>
                    <View style={styles.inputBox}>

                        <View style={styles.inputDiv}>
                            <View style={styles.input}>
                                <CustomInput placeholder="Phone" style={styles.custom}
                                    placeholderTextColor="#9FBDE5"
                                    onChangeText={(event) => this.changeTextHandler(event, 'phoneNumber')}
                                    value={this.state.controls.phoneNumber.value}
                                    keyboardType="numeric"
                                    maxLength={10}
                                    disabled
                                ></CustomInput>
                            </View>
                            <Icon name='call' style={styles.icon} />

                        </View>
                    </View>
                    <TouchableOpacity style={styles.wrongNumber} onPress={this.wrongNumber}>
                        <Para extraBold={true} style={styles.para}>Wrong Number?</Para>
                    </TouchableOpacity>
                    <View style={styles.otpInput}>
                        <OtpInputs getOtp={(otp) => this.getOtp(otp)}></OtpInputs>
                    </View>
                    <TouchableOpacity style={[commonStyle.flexRow, commonStyle.textCenter, commonStyle.alignCenter]} onPress={this.resendOtp}><Para style={[styles.para]} bold={true}>Resend</Para><Para extraBold={true} style={[styles.para, { marginLeft: 10 }]}>OTP</Para></TouchableOpacity>

                </View>
                <View style={styles.submitDiv}>
                    <View>
                        <Para bold={true} style={styles.submitText}>Terms & Conditions</Para>
                    </View>
                    <TouchableOpacity onPress={this.otpVerify}>
                        {
                            this.state.loading ?
                                <Spinner color='white' size={40}></Spinner>
                                :
                                <Image
                                    source={require("../../../assets/images/forward.png")}
                                    style={commonStyle.forward}
                                />
                        }

                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    formDiv: {
        paddingVertical: 70,
    },
    nameDiv: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 40
    },
    nameFont: {
        fontSize: 25,
        color: 'white'
    },
    hello: {
        marginRight: 10
    },
    inputDiv: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#0F5ABF',
        marginBottom: 8,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 3,
        paddingHorizontal: 20,
        paddingVertical: 3

    },
    input: {
        // backgroundColor: 'red',
        flex: 1,
        marginRight: 15
    },
    icon: {
        color: '#9FBDE5',
        fontSize: 22
    },
    custom: {
        color: 'white'
    },

    submitDiv: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between'
    },
    submitText: {
        color: 'white',
        fontSize: 17
    },
    wrongNumber: {
        alignItems: 'flex-end'
    },
    para: {
        color: '#A1C6F9'
    },
    otpInput: {
        flexDirection: 'row',
        justifyContent: "space-between",
        marginTop: 25,
        marginBottom: 10
    }

})
