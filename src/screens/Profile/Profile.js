import React, { Component } from 'react'
import { Text, StyleSheet, View, ImageBackground, Linking, BackHandler, ToastAndroid } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import CustomHeader from '../../components/Shared/CustomHeader/CustomHeader'
import { commonStyle } from '../../utils/Style/Style'
import { Image } from 'react-native-animatable'
import Para from '../../components/Shared/Text/Para'
import MenuCard from '../../components/MenuCard/MenuCard'
import axios from 'axios';
import { url, tcUrl, imageUrl } from '../../utils/BaseUrl/BaseUrl'
import { disableRoute } from '../../utils/Constants/Constatnts'
import { removeToken } from '../../utils/Token/Token'
import Dimension from '../../utils/Dimension/Dimension';
import Modal from 'react-native-modal';
import Delete from '../../components/Delete/Delete'
import ProfileSkeleton from '../../components/Skeleton/ProfileSkeleton'
import AsyncStorage from '@react-native-community/async-storage'
import { Root, Container } from 'native-base'

var count = 0;

export default class Profile extends Component {

    constructor(props) {
        super(props);
        this.currentRouteName = 'profile'
        this.props.navigation.addListener('blur', () => {
            this.onLeave();
        });
        this.props.navigation.addListener('focus', () => {
            this.onFocus();
        });
    }

    state = {
        userData: null,
        logoutVisible: false
    }

    componentDidMount = () => {
        this.getUserData();
    //      removeToken().then(res => {

    // })
   
    }


    // componentWillReceiveProps = (props) => {
    //     console.log(props)
    // }

    logoutModal = (val) => {
        this.setState((prev) => {
            return {
                ...prev,
                logoutVisible: val
            }
        });
    }

    getUserData = async () => {
        try {
            let res = await axios.get(`${url}/user`);
            if (res) {
                console.log(res['data'])
                this.setState((prev) => {
                    return {
                        ...prev,
                        userData: res['data']['data']
                    }
                });
            }
        }
        catch (err) {
            if (err) {
                console.log(err.response)
            }
        }
    }

    state = {
        menus: [
            {
                title: 'My Bookings',
                image: <Image source={require('../../../assets/images/booking.png')} style={[styles.menuImg, commonStyle.img]} />,
                navigate: 'Booking'
            },
            {
                title: 'My Vehicles',
                image: <Image source={require('../../../assets/images/vehicle.png')} style={[styles.menuImg, commonStyle.img]} />,
                navigate: 'MyVehicle'

            },
            // {
            //     title: 'My Payment History',
            //     image: <Image source={require('../../../assets/images/payment.png')} style={[styles.menuImg, commonStyle.img]} />,
            //     navigate: 'PaymentHistory'

            // },
            {
                title: 'FAQs / Help',
                image: <Image source={require('../../../assets/images/faq.png')} style={[styles.menuImg, commonStyle.img]} />,
                navigate: 'Faq'

            }
        ]
    }

    refresh = () => {
        this.getUserData();
    }

    logout = async () => {
        let token = await AsyncStorage.getItem('fcmToken');
        try {
            let res = await axios.post(`${url}/user/logout`, { deviceToken: token });
            if (res) {
                removeToken().then(res => {
                    AsyncStorage.removeItem('fcmToken').then(res => {
                        this.logoutModal(false);
                        this.props.navigation.navigate('Stations');
                        this.props.navigation.navigate('Auth', { active: 'login' });
                    })
                })
            }
        }
        catch (err) {
            if (err) {
                console.log(err.response)
            }
        }
    }

    navigate = (menu) => {
        console.log(menu)
        this.props.navigation.navigate(menu.navigate, { onGoBack: () => this.refresh() });
    }

    openTC = () => {
        Linking.openURL(tcUrl);
    }

    onFocus() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    onLeave() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    callSupport = () => {
        let phoneNumber = '1898787678'
        Linking.openURL(`tel:${phoneNumber}`)
    }


    handleBackButtonClick = () => {
        if (disableRoute.indexOf(this.currentRouteName) >= 0) {
            count += 1;
            if (count == 1) {
                ToastAndroid.show('Press again for exit!', ToastAndroid.SHORT);
            }
            else if (count == 2) {
                count = 0;
                BackHandler.exitApp();
            }
            return true;
        }
        else {
            return false;
        }

    }

    render() {
        return (
            <ScrollView>
                {
                    this.state.userData ?
                        <View>
                            <View style={styles.header}>
                                <View style={styles.backgroundDiv}>
                                    <Image source={require('../../../assets/images/profile-back.png')} style={[styles.backgroundImage]} />
                                </View>
                                <View style={styles.relative}>
                                    <View style={[commonStyle.flexRow, { justifyContent: 'flex-end' }]}>
                                        <TouchableOpacity style={commonStyle.flexEnd} onPress={() => this.navigate({ navigate: 'Notification' })}>
                                            <Image source={require('../../../assets/images/notification.png')} style={[commonStyle.img, styles.notificationImg]} />
                                        </TouchableOpacity>
                                        <TouchableOpacity style={commonStyle.flexEnd} onPress={() => this.logoutModal(true)}>
                                            <Image source={require('../../../assets/images/logout.png')} style={[commonStyle.img, styles.notificationImg]} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ alignItems: 'center' }}>
                                        <View style={styles.profileImgDiv}>
                                            {
                                                this.state.userData.profilePic ?
                                                    <Image source={{ uri: `${imageUrl}${this.state.userData.profilePic}` }} style={[styles.profileImg]} />
                                                    : <Image source={require('../../../assets/images/placeholder.png')} style={[commonStyle.img, styles.profileImg]} />
                                            }
                                        </View>
                                    </View>
                                </View>


                            </View>
                            <View style={styles.editDiv}>
                                <View style={styles.editFix}>
                                    <View style={styles.more}><TouchableOpacity onPress={() => this.navigate({ navigate: 'Account' })}><Image source={require('../../../assets/images/more.png')} style={[commonStyle.img, styles.moreImg]} /></TouchableOpacity></View>
                                </View>
                            </View>

                            <View style={styles.mainContent}>
                                <View style={[commonStyle.textCenter, commonStyle.flexRow]}>
                                    <Para extraBold={true} style={styles.name}>{this.state.userData.name}</Para>
                                </View>
                                <View style={styles.save}>
                                    <Para>You have saved {Math.floor(this.state.userData.savedTrees)}</Para>
                                    <Image source={require('../../../assets/images/tree.png')} style={[styles.tree, commonStyle.img]} />
                                    <Para>by charging with us</Para>
                                </View>
                                <View style={styles.menuDiv}>
                                    {
                                        this.state.menus.map((menu, i) => {
                                            return <MenuCard menu={menu} key={i} navigate={this.navigate}></MenuCard>
                                        })
                                    }
                                </View>
                                <View>
                                    <TouchableOpacity onPress={this.callSupport}><Para extraBold={true}>Call Customer Support</Para></TouchableOpacity>
                                    <TouchableOpacity onPress={this.openTC}><Para extraBold={true} style={{ marginTop: 5 }}>T&C</Para></TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        : <ProfileSkeleton></ProfileSkeleton>
                }

                <Modal
                    isVisible={this.state.logoutVisible}
                    style={styles.modal}
                    animationOut='slideOutDown'
                    // hasBackdrop={false}
                    // onBackButtonPress={this.removeModal(false, null)}
                    animationIn='slideInUp' backdropOpacity={0.2}>
                    <Root>
                        <Container style={{ backgroundColor: 'transparent', display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                            <View style={styles.modalView}>
                                <View style={styles.modalContent}>
                                    <Delete title="Logout" cancel={this.logout} closeModal={() => this.logoutModal(false)}></Delete>
                                </View>
                            </View>
                        </Container>
                    </Root>
                </Modal>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        height: 150,
        position: 'relative',
        // backgroundColor:'red'
    },
    backgroundDiv: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        minHeight: 150,
        // backgroundColor:'red'

    },
    backgroundImage: {
        // width: '100%',
        height: '100%',
        resizeMode: 'contain',
        width: Dimension.width
    },
    relative: {
        position: 'relative',
        zIndex: 10
    },
    notificationImg: {
        height: 60,
        width: 60
    },
    profileImgDiv: {
        backgroundColor: 'white',
        height: 100,
        width: 100,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        borderRadius: 28,
        position: 'relative',
        top: 35,
        shadowColor: "#e0e0e0",
        zIndex: 1000,
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 1.25,
        shadowRadius: 3.84,

        elevation: 2,
    },
    profileImg: {
        height: '93%',
        width: '93%',
        borderRadius: 22,
        resizeMode: 'cover'
    },
    editDiv: {
        alignItems: 'center'
    },
    editFix: {
        width: 100,
        display: 'flex',
        alignItems: 'flex-end',
        // backgroundColor:'red'
    },
    more: {
        position: 'relative',
        height: 50,
        width: 50,
        // bottom: -25,
        // right: -20,
        top:18,
        right:-20,
        zIndex: 10000
    },
    moreImg: {
        height: '100%',
        width: '100%'
    },
    mainContent: {
        paddingTop: 30,
        paddingHorizontal: 25,
        paddingBottom: 25
    },
    name: {
        fontSize: 25
    },
    save: {
        paddingTop: 10,
        paddingBottom: 20,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    tree: {
        height: 23,
        width: 23,
        marginHorizontal: 3
    },
    menuImg: {
        height: 25
    },
    modalContent: {
        paddingTop: 20,
        paddingBottom: 10
    },
    modalView: {
        backgroundColor: 'white',
        borderRadius: 10,
        position: 'relative',
        width: '100%'
        // width:Dimension.width,
        // flex:1
        // left:0
        // position:'absolute',
        // bottom:0
    },
    modal: {
        // margin: 0, // This is the important style you need to set
        // alignItems: undefined,
        justifyContent: 'center',
    },
})
