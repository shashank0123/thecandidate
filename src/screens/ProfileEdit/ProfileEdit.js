import React, { Component } from 'react'
import { Text, StyleSheet, View, Platform } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import CustomHeader from '../../components/Shared/CustomHeader/CustomHeader'
import { url, imageUrl } from '../../utils/BaseUrl/BaseUrl'
import axios from 'axios';
import { Image } from 'react-native-animatable'
import { commonStyle } from '../../utils/Style/Style'
import CustomInput from '../../components/Shared/CustomInput/CustomInput'
import Para from '../../components/Shared/Text/Para'
import { fontFamily } from '../../utils/FontFamily/FontFamily'
import validate from '../../utils/Validation/Validation'
import { Button, Spinner } from 'native-base'
import { errMsg, msg } from '../../utils/Toaster/Toaster'
import ImagePicker from 'react-native-image-picker';
import { getToken } from '../../utils/Token/Token'

export default class ProfileEdit extends Component {

    state = {
        userData: null,
        controls: {
            name: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                    required: true
                }
            },
            email: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: ''
                },
                validate: {
                    isEmail: true
                }
            },
        },
        showImage: null,
        loading: false,
        imageLoading: false
    }

    componentDidMount = () => {
        this.getUserData();
        console.log(this.props)
    }

    getUserData = async () => {
        try {
            let res = await axios.get(`${url}/user`);
            if (res) {
                console.log(res)
                this.setState((prev) => {
                    return {
                        ...prev,
                        userData: res['data']['data'],
                        controls: {
                            ...prev.controls,
                            name: {
                                ...prev.controls.name,
                                value: res['data']['data']['name']
                            },
                            email: {
                                ...prev.controls.name,
                                value: res['data']['data']['email']
                            }
                        },
                        showImage: res['data']['data']['profilePic']
                    }
                });
            }
        }
        catch (err) {
            if (err) {
                console.log(err.response)
            }
        }
    }

    changeTextHandler = (event, key) => {
        this.setState(prevState => {
            return {
                controls: {
                    ...prevState.controls,
                    [key]: {
                        ...prevState.controls[key],
                        value: event,
                        touched: true,
                        valid: validate(event, prevState.controls[key].validate, key)
                    }
                }
            };
        });
    };

    submit = async () => {
        this.showLoading('loading', true);
        let data = {};
        Object.keys(this.state.controls).forEach(key => {
            data[key] = this.state.controls[key].value
        })
        try {
            let res = await axios.put(`${url}/user/update-profile`, data);
            if (res) {
                this.showLoading('loading', false);
                msg(res['data']['msg']);
                // this.props.navigateToOtp({ ...data, ...res['data']['data'] });
                this.props.route.params.onGoBack();
                this.goBack();
            }
        }
        catch (err) {
            if (err) {
                this.showLoading('loading', false);
                errMsg(err['response']);
            }
        }
    }

    showLoading = (key, val) => {
        this.setState(prev => {
            return {
                ...prev,
                [key]: val
            }
        })
    }

    goBack = () => {
        this.props.navigation.goBack()
    }

    openCamera = () => {
        const options = {
            title: 'Select Profile Picture',
            // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let source = {};
                if (Platform.OS === 'android') {
                    source = { uri: response.uri }
                } else {
                    source = { uri: response.uri.replace('file://', '') }
                }

                this.uploadImage(source);
            }
        });
    }

    uploadImage = async (source) => {
        this.showLoading('imageLoading', true);
        let formData = new FormData();
        formData.append('profilePic', {
            uri: source.uri,
            type: source.type ? source.type : 'image/png',
            name: 'image'
        });

        try {
            let res = await axios.put(`${url}/user/update-profile-pic`,
                formData,
                {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data'
                    }
                }
            );
            if (res) {
                this.showLoading('imageLoading', false);
                this.props.route.params.onGoBack();
                this.getUserData();
                this.setState(prev => {
                    return {
                        ...prev,
                        showImage: res['data']['data']['profilePic']
                    }
                })
            }
        }
        catch (err) {
            if (err) {
                this.showLoading('imageLoading', false);
                errMsg(err['response']);
            }
        }
    }

    render() {
        return (
            <ScrollView stickyHeaderIndices={[0]}
                showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="handled">
                <CustomHeader title="Your Account" backBtn={true} goBack={this.goBack} rightIcon={false}></CustomHeader>
                <View style={styles.profile}>
                    <View style={{ alignItems: 'center', paddingBottom: 20 }}>
                        <View style={styles.profileImgDiv}>
                            {/* <Image source={require('../../../assets/images/user-profile.png')} style={[styles.profileImg, commonStyle.img]} /> */}
                            {
                                this.state.imageLoading ?
                                    <Spinner color='#1371EF' size={40}></Spinner>
                                    :
                                    (this.state.userData && this.state.showImage) ?
                                        <Image source={{ uri: `${imageUrl}${this.state.showImage}` }} style={[styles.profileImg]} />
                                        : <Image source={require('../../../assets/images/placeholder.png')} style={[commonStyle.img, styles.profileImg]} />
                            }
                            <View style={styles.more}><TouchableOpacity onPress={this.openCamera}><Image source={require('../../../assets/images/edit.png')} style={[commonStyle.img, styles.moreImg]} /></TouchableOpacity></View>
                        </View>
                    </View>
                    <View style={{ marginTop: 25 }}>
                        <View style={styles.input}>
                            <Para style={commonStyle.gray}>Name</Para>
                            <CustomInput
                                style={styles.custom}
                                placeholderTextColor="#9FBDE5"
                                onChangeText={(event) => this.changeTextHandler(event, 'name')}
                                value={this.state.controls.name.value}
                            ></CustomInput>
                        </View>
                        <View style={styles.input}>
                            <Para style={commonStyle.gray}>Email</Para>
                            <CustomInput
                                style={styles.custom}
                                placeholderTextColor="#9FBDE5"
                                onChangeText={(event) => this.changeTextHandler(event, 'email')}
                                value={this.state.controls.email.value}
                            ></CustomInput>
                        </View>
                    </View>
                    <View style={{ padding: 10 }}>
                        <Button style={commonStyle.button} onPress={this.submit}>
                            {
                                this.state.loading ?
                                    <Spinner color='white' size={40}></Spinner>
                                    :
                                    <Para bold={true} style={commonStyle.white}>Submit</Para>
                            }
                        </Button>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    profile: {
        padding: 25
    },
    profileImgDiv: {
        backgroundColor: 'white',
        height: 100,
        width: 100,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        borderRadius: 28,
        // position: 'relative',
        // top: 35,
        shadowColor: "#e0e0e0",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 1.25,
        shadowRadius: 3.84,

        elevation: 2,
    },
    profileImg: {
        height: '93%',
        width: '93%',
        borderRadius: 22,
        resizeMode: 'cover'
    },
    more: {
        position: 'absolute',
        height: 60,
        width: 60,
        bottom: -25,
        right: -20,

    },
    moreImg: {
        height: '100%',
        width: '100%'
    },
    custom: {
        borderBottomWidth: 1,
        borderBottomColor: '#e0e0e0',
        height: 40,
        color: '#1371EF',
        fontFamily: fontFamily.bold
    },
    input: {
        paddingHorizontal: 15,
        marginBottom: 20
    }
})
