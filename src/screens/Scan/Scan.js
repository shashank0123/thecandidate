'use strict';

import React, { Component } from 'react'
import { Text, StyleSheet, View, Linking, PermissionsAndroid, Vibration, Image, TouchableOpacity, BackHandler, ToastAndroid, Platform } from 'react-native'
// import QRCodeScanner from 'react-native-qrcode-scanner';
import Dimension from '../../utils/Dimension/Dimension';
import { CameraKitCameraScreen, CameraKitCamera } from 'react-native-camera-kit';
import { url } from '../../utils/BaseUrl/BaseUrl';
import axios from 'axios';
import { errMsg, msg } from '../../utils/Toaster/Toaster';
import { commonStyle } from '../../utils/Style/Style';
import { disableRoute } from '../../utils/Constants/Constatnts';
import Para from '../../components/Shared/Text/Para';
import { request, PERMISSIONS } from 'react-native-permissions';

var count = 0;

export default class Scan extends Component {

    constructor(props) {
        super(props);
        this.currentRouteName = 'scan'
        this.props.navigation.addListener('blur', () => {
            this.onLeave();
        })
        this.props.navigation.addListener('focus', () => {
            this.onFoucs();
        })
    }


    state = {
        torch: false,
        frontCamera: false,
        scan: false,
        permission: false
    }

    componentDidMount = () => {
        this.checkPermission();
    }

    checkPermission = async () => {
        try {
            if (Platform.OS == 'android') {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    // {
                    //     'title': 'Cool Photo App Camera Permission',
                    //     'message': 'Cool Photo App needs access to your camera ' +
                    //         'so you can take awesome pictures.'
                    // }
                )
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    this.setState((prev) => {
                        return {
                            ...prev,
                            permission: true,
                            scan: true
                        }
                    });
                } else {
                    errMsg("Camera permission denied");
                }
            }
            else {
                request(PERMISSIONS.IOS.CAMERA).then((result) => {
                    if (result == 'granted') {
                        this.setState((prev) => {
                            return {
                                ...prev,
                                permission: true,
                                scan: true
                            }
                        });
                    }
                    else {
                        errMsg("Camera permission denied");
                    }
                });
            }
        } catch (err) {
            console.log(err)
        }
    }


    // barcodeRecognized = ({ barcodes }) => {
    //     this.setState({ barcodes })
    //     console.log(barcodes);
    // }

    onBarcodeScan = async (event) => {
        let { codeStringValue } = event.nativeEvent;
        console.log(codeStringValue, this.state.scan)
        if (codeStringValue && this.state.scan) {
            this.updateScan(false);
            try {
                let res = await axios.post(`${url}/booking/activate`, { code: codeStringValue });
                if (res) {
                    Vibration.vibrate(50);
                    this.props.navigation.navigate('Charging', { booking: res['data']['data'] });
                }
            }
            catch (err) {
                if (err) {
                    console.log(err, err.response, 'errr')
                    errMsg(err['response']);
                    // this.props.navigation.navigate('Charging', { qr: codeStringValue });
                    this.updateScan(false);
                }
            }
        }
    }

    flashChange = async () => {
        let torch = !this.state.torch
        this.setState(prev => {
            return {
                ...prev,
                torch: torch
            }
        })
        const success = await this.camera.onSetTorch(torch ? 'torch' : 'off');
        console.log(success, 'success');
    }

    updateScan = (val) => {
        this.setState((prev) => {
            return {
                ...prev,
                scan: val
            }
        });
    }

    cameraChange = async () => {
        let frontCamera = !this.state.frontCamera
        this.setState(prev => {
            return {
                ...prev,
                frontCamera: frontCamera
            }
        })
        const success = await this.camera.onSwitchCameraPressed();
    }

    componentWillUnmount = () => {
        this.updateScan(true);
    }

    onFoucs() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        this.checkPermission();
    }

    onLeave() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }


    handleBackButtonClick = () => {
        if (disableRoute.indexOf(this.currentRouteName) >= 0) {
            count += 1;
            if (count == 1) {
                ToastAndroid.show('Press again for exit!', ToastAndroid.SHORT);
            }
            else if (count == 2) {
                count = 0;
                BackHandler.exitApp();
            }
            return true;
        }
        else {
            return false;
        }

    }

    render() {
        return (
            <View style={styles.cameraDiv}>
                {/* 
                <QRCodeScanner
                    reactivate={true}
                    showMarker={true}
                    containerStyle={{

                    }}
                    cameraStyle={{

                    }}
                    markerStyle={{ borderColor: 'transparent' }}
                    ref={(node) => { this.scanner = node }}
                    onRead={this.onSuccess}
                    cameraProps={{ flashMode: this.state.torch ? Camera.Constants.FlashMode.torch : Camera.Constants.FlashMode.off, type: Camera.Constants.Type.back }}

                />
                <View style={styles.overlayDiv}>
                    <View style={[styles.overlay, { height: '28%', width: '100%' }]}>

                    </View>
                    <View style={[styles.mainDiv, { height: '40%' }]}>
                        <View style={[styles.overlay, { height: '100%', width: '17%' }]}></View>
                        <View style={[styles.blank, { height: '100%', width: '66%' }]}>

                        </View>
                        <View style={[styles.overlay, { height: '100%', width: '17%' }]}></View>
                    </View>
                    <View style={[styles.overlay, {height: '28%', width: '100%' }]}>

                    </View>
            </View> */}
                {
                    this.state.scan ?
                        <View>
                            <CameraKitCameraScreen
                                ref={cam => this.camera = cam}
                                // style={{
                                // flex: 1,
                                // backgroundColor: 'red'
                                // }}
                                cameraOptions={{
                                    flashMode: this.state.torch,
                                }}
                                actions={{ rightButtonText: 'Done', leftButtonText: 'Cancel' }}
                                onBottomButtonPressed={(event) => this.onBottomButtonPressed(event)}
                                scanBarcode={true}
                                laserColor={"red"}
                                frameColor={"white"}

                                onReadCode={((event) => this.onBarcodeScan(event))} //optional
                                hideControls={true}           //(default false) optional, hide buttons and additional controls on top and bottom of screen
                                showFrame={true}   //(default false) optional, show frame with transparent layer (qr code or barcode will be read on this area ONLY), start animation for scanner,that stoped when find any code. Frame always at center of the screen
                                offsetForScannerFrame={10}   //(default 30) optional, offset from left and right side of the screen
                                heightForScannerFrame={300}  //(default 200) optional, change height of the scanner frame
                                colorForScannerFrame={'red'} //(default white) optional, change colot of the scanner frame

                            />
                            <View style={styles.flash} >
                                <TouchableOpacity onPress={this.flashChange}><Image source={require('../../../assets/images/flash.png')} style={[commonStyle.img, styles.icon]} /></TouchableOpacity>
                            </View>
                            <View style={styles.front} >
                                <TouchableOpacity onPress={this.cameraChange}><Image source={require('../../../assets/images/camera.png')} style={[commonStyle.img, styles.icon]} /></TouchableOpacity>
                            </View>
                        </View>
                        : <View style={styles.cameraNot}>
                            {
                                !this.state.permission ?
                                    <>
                                        <Para bold={true} style={{ fontSize: 20, marginBottom: 5 }}>Please allow camera permission.</Para>
                                        <TouchableOpacity onPress={this.checkPermission}><Para style={[commonStyle.blue, commonStyle.small]}>Click here for permission</Para></TouchableOpacity>
                                    </>
                                    :
                                    <>
                                        <Para bold={true} style={{ fontSize: 20, marginBottom: 5, textAlign: 'center' }}>Please try again to scan.</Para>
                                        <TouchableOpacity onPress={() => this.updateScan(true)}><Para style={[commonStyle.blue, commonStyle.small]}>Scan Again</Para></TouchableOpacity>
                                    </>
                            }
                        </View>
                }

            </View >
        )
    }
}

const styles = StyleSheet.create({
    cameraDiv: {
        // width: '100%',
        // height: '100%',
        flex: 1
    },
    camera: {
        flex: 1,
        width: '100%',
    },
    overlay: {
        backgroundColor: '#ffffff3b',
    },
    overlayDiv: {
        position: 'absolute',
        top: 30,
        left: 0,
        // backgroundColor:'red',
        height: Dimension.height - 55,
        width: Dimension.width
    },
    blank: {
        backgroundColor: 'transparent',
        borderRadius: 10,
        // backgroundColor:'red'
    },
    mainDiv: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    flash: {
        // backgroundColor:'red',
        position: 'absolute',
        top: 10,
        right: 5
    },
    front: {
        position: 'absolute',
        top: 70,
        right: 5
    },
    icon: {
        height: 60,
        width: 60
    },
    cameraNot: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
