import React, { Component } from 'react'
import { StyleSheet, Image, View, TouchableOpacity, Linking } from 'react-native'
import Para from '../../components/Shared/Text/Para'
import CustomInput from '../../components/Shared/CustomInput/CustomInput';
import { Icon, Spinner } from 'native-base';
import validate from '../../utils/Validation/Validation'
import axios from "axios";
import { url, tcUrl } from '../../utils/BaseUrl/BaseUrl';
import { msg, errMsg, simpleMsg } from '../../utils/Toaster/Toaster';
import { commonStyle } from '../../utils/Style/Style';
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';


export default class Signup extends Component {

    state = {
        controls: {
            name: {
                value: '',
                touched: false,
                valid: {
                    isValid: false,
                    text: 'Name is required'
                },
                validate: {
                    required: true
                }
            },
            phoneNumber: {
                value: '',
                touched: false,
                type: 'number',
                valid: {
                    isValid: false,
                    text: 'Phone Number is required'
                },
                validate: {
                    required: true,
                    isPhone: true
                }
            },
        },
        socialId: null,
        type: null,
        loading: false
    }

    componentDidMount = () => {
        this.setupGoogleSignin();
    }

    changeTextHandler = (event, key) => {
        this.setState(prevState => {
            return {
                controls: {
                    ...prevState.controls,
                    [key]: {
                        ...prevState.controls[key],
                        value: (prevState.controls[key].type == 'number') ? event.replace(/[^0-9]/g, '') : event,
                        touched: false,
                        valid: validate(event, prevState.controls[key].validate, key)
                    }
                }
            };
        });
    };

    signup = async () => {
        if (this.state.controls.phoneNumber.valid.isValid && this.state.controls.name.valid.isValid) {
            this.showLoading(true);
            let data = {};
            Object.keys(this.state.controls).forEach(key => {
                data[key] = this.state.controls[key].value
            })
            if (this.state.socialId) {
                data.socialId = this.state.socialId;
                data.type = this.state.type;
            }

            try {
                let res = await axios.post(`${url}/user/signup`, data);
                if (res) {
                    this.showLoading(false);
                    msg(res['data']['msg']);
                    this.props.navigateToOtp({ ...data, ...res['data']['data'] });
                }
            }
            catch (err) {
                if (err) {
                    this.googleSignOut();
                    this.fbLogout();
                    this.showLoading(false);
                    errMsg(err['response']);
                }
            }
        }
        else {
            for (let key of Object.keys(this.state.controls)) {
                this.showErr(key);
            }
            console.log(this.state)
        }

    }

    fbLogout() {
        var current_access_token = '';
        AccessToken.getCurrentAccessToken().then((data) => {
            current_access_token = data.accessToken.toString();
        }).then(() => {
            let logout =
                new GraphRequest(
                    "me/permissions/",
                    {
                        accessToken: current_access_token,
                        httpMethod: 'DELETE'
                    },
                    (error, result) => {
                        if (error) {
                            console.log('Error fetching data: ' + error.toString());
                        } else {
                            LoginManager.logOut();
                        }
                    });
            new GraphRequestManager().addRequest(logout).start();
        })
            .catch(error => {
                console.log(error)
            });
    }

    showErr = (key) => {
        this.setState(prevState => {
            return {
                ...prevState,
                controls: {
                    ...prevState.controls,
                    [key]: {
                        ...prevState.controls[key],
                        touched: true,
                    }
                }
            }
        })
    }

    showLoading = (val) => {
        this.setState(prev => {
            return {
                ...prev,
                loading: val
            }
        })
    }


    fbAuth = () => {
        LoginManager.logInWithPermissions(['public_profile']).then(
            (result) => {
                if (result.isCancelled) {
                    console.log('Login was cancelled');
                } else {
                    AccessToken.getCurrentAccessToken().then((data) => {
                        const { accessToken } = data
                        this.initUser(accessToken);
                    })
                }
            },
            (err) => {
                // console.log('Login failed with error: ' + error);
                // alert('WRONG SIGNIN' + JSON.stringify(err));
                errMsg('ERR');

            }
        );
    }

    initUser = (token) => {
        fetch('https://graph.facebook.com/v2.5/me?fields=email,name&access_token=' + token)
            .then((response) => response.json())
            .then((user) => {
                simpleMsg('Please Add your phone number to continue');
                this.setState(prev => {
                    return {
                        ...prev,
                        controls: {
                            ...prev.controls,
                            name: {
                                ...prev.controls.name,
                                value: user.name,
                                valid: {
                                    ...prev.controls.name.valid,
                                    isValid: true
                                }
                            }
                        },
                        socialId: user.id,
                        type: 'Facebook'
                    }
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }


    googleAuth = () => {
        GoogleSignin.signIn()
            .then((user) => {
                simpleMsg('Please Add your phone number to continue');
                console.log(user)
                this.setState(prev => {
                    return {
                        ...prev,
                        controls: {
                            ...prev.controls,
                            name: {
                                ...prev.controls.name,
                                value: user.user.name,
                                valid: {
                                    ...prev.controls.name.valid,
                                    isValid: true
                                }
                            }
                        },
                        socialId: user.user.id,
                        type: 'Google'
                    }
                });
            })
            .catch((err) => {
                errMsg('ERR');
                // alert('WRONG SIGNIN' + JSON.stringify(err));
            })
            .done();
    }

    googleSignOut = () => {
        GoogleSignin.signOut()
            .then((user) => {

            })
            .catch((err) => {
                console.log('WRONG SIGNIN', err);
            })
            .done();
    }

    setupGoogleSignin = async () => {
        try {
            await GoogleSignin.hasPlayServices({ autoResolve: true, showPlayServicesUpdateDialog: true });
            await GoogleSignin.configure({
                // iosClientId: settings.iOSClientId,
                webClientId: '1015927727812-dbvgn6duaq7dpfsi6v9r2q1r25bbdkk0.apps.googleusercontent.com',
                offlineAccess: false
            });

            // GoogleSignin.getCurrentUser().then(res =>{
            //     console.log(res);
            // },(err) =>{
            //     console.log(err)
            // })
            // // console.log(user);
        }
        catch (err) {
            console.log("Google signin error", err, err.code, err.message);
        }
    }

    openTC = () => {
        Linking.openURL(tcUrl);
    }



    render() {
        return (
            <View>
                <View style={styles.formDiv}>
                    <View style={styles.nameDiv}>
                        <Para style={[styles.nameFont, styles.hello]}>Hello</Para>
                        <Para extraBold={true} style={styles.nameFont}>Environmentalist !</Para>
                    </View>
                    <View style={styles.inputBox}>
                        <View style={styles.inputDiv}>
                            <View style={styles.input}>
                                <CustomInput
                                    placeholder="Name"
                                    style={styles.custom}
                                    placeholderTextColor="#9FBDE5"
                                    onChangeText={(event) => this.changeTextHandler(event, 'name')}
                                    value={this.state.controls.name.value}
                                ></CustomInput>
                            </View>
                            <Icon name='person' style={styles.icon} />

                        </View>
                        <View>
                            {
                                (!this.state.controls.name.valid.isValid && this.state.controls.name.touched) ?
                                    <Para style={{ color: 'white', marginTop: 8, fontSize: 14 }} bold={true}>{this.state.controls.name.valid.text}</Para>
                                    : null
                            }
                        </View>
                    </View>
                    <View style={styles.inputBox}>

                        <View style={styles.inputDiv}>
                            <View style={styles.input}>
                                <CustomInput placeholder="Phone"
                                    style={styles.custom} placeholderTextColor="#9FBDE5"
                                    onChangeText={(event) => this.changeTextHandler(event, 'phoneNumber')}
                                    value={this.state.controls.phoneNumber.value}
                                    keyboardType="numeric"
                                    onSubmitEditing={this.signup}
                                    maxLength={10}></CustomInput>
                            </View>
                            <Icon name='call' style={styles.icon} />

                        </View>
                        <View>
                            {
                                (!this.state.controls.phoneNumber.valid.isValid && this.state.controls.phoneNumber.touched) ?
                                    <Para style={{ color: 'white', marginTop: 8, fontSize: 14 }} bold={true}>{this.state.controls.phoneNumber.valid.text}</Para>
                                    : null
                            }
                        </View>
                    </View>
                    <View style={styles.socialIconDiv}>
                        <TouchableOpacity onPress={this.googleAuth}>
                            <Image
                                source={require("../../../assets/images/google.png")}
                                style={[styles.socialIcon, commonStyle.img]}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.fbAuth}>
                            <Image
                                source={require("../../../assets/images/fb.png")}
                                style={[styles.socialIcon, commonStyle.img]}

                            />
                        </TouchableOpacity>
                    </View>

                </View>
                <View style={styles.submitDiv}>
                    <View>
                        {/* <Para bold={true} style={styles.submitText}>Skip Login</Para> */}
                        <TouchableOpacity onPress={this.openTC}><Para bold={true} style={styles.submitText}>Terms & Conditions</Para></TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={this.signup}>
                        {
                            this.state.loading ?
                                <Spinner color='white' size={40}></Spinner>
                                :
                                <Image
                                    source={require("../../../assets/images/forward.png")}
                                    style={commonStyle.forward}

                                />
                        }

                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    formDiv: {
        paddingVertical: 70
    },
    nameDiv: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 40
    },
    nameFont: {
        fontSize: 25,
        color: 'white'
    },
    hello: {
        marginRight: 10
    },
    inputDiv: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#0F5ABF',
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 3,
        paddingHorizontal: 20,
        paddingVertical: 3

    },
    inputBox: {
        marginBottom: 20,
    },
    input: {
        // backgroundColor: 'red',
        flex: 1,
        marginRight: 15
    },
    icon: {
        color: '#9FBDE5',
        fontSize: 22
    },
    custom: {
        color: 'white'
    },
    socialIconDiv: {
        flexDirection: 'row'
    },
    socialIcon: {
        marginRight: 10,
        marginTop: 5,
        height: 50,
        width: 50
    },
    submitDiv: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between'
    },
    submitText: {
        color: 'white',
        fontSize: 17,
        marginVertical: 2
    }

})
