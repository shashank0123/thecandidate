import React, { Component } from 'react'
import { ImageBackground, Text, StyleSheet, View, Image, EventEmitter } from 'react-native'
import Para from '../../components/Shared/Text/Para'
import { getToken, removeToken, getLoginCount } from '../../utils/Token/Token';
const backgroundImage = require("../../../assets/images/login-signup.png");
import axios from 'axios';

export default class Splash extends Component {

    componentDidMount = () => {
        this.checkToken();
        // let event = new EventEmitter();
        // event.addListener('blocked', (res) => {
        //     console.log('event listen')
        //     this.props.navigation.navigate('Auth', { active: 'login', blocked: true });
        // })
    }

    checkToken = () => {
        getToken().then(res => {
            if (res) {
                axios.defaults.headers.common["Authorization"] =
                    "Bearer " + res;
                this.props.navigation.navigate('Tab');
            }
            else {
                getLoginCount().then(res => {
                    if (res == 'yes') {
                        this.props.navigation.navigate('Auth');
                    }
                    else {
                        this.props.navigation.navigate('Onbording');
                    }
                })
            }
        }, (err) => {

        })
    }


    // removeToken = () => {
    //     removeToken().then(res => {
    //         this.props.navigation.navigate('Auth', { active: 'login' });
    //     })
    // }

    render() {
        return (
            <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
                <View style={styles.splashPage}>
                    <Image
                        source={require("../../../assets/images/logo.png")}
                        style={styles.image}
                    />
                    <Para style={styles.para}>Welcome to</Para>
                    <Para style={styles.heading} extraBold={true}>Chargebizz</Para>
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    splashPage: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",

    },
    backgroundImage: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },
    image: {
        maxWidth: '100%',
        maxHeight: '100%',
        width: 100,
        height: 100
    },
    heading: {
        fontSize: 25,
        color: 'white'
    },
    para: {
        marginVertical: 15,
        color: 'white'
    }
})
