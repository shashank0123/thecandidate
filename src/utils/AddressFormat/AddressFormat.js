export function addressFormat(adddress) {
    let addr ='';   
    if(adddress.lineOne){
        addr += adddress.lineOne;
    }
    if(adddress.area){
        addr += ', ';
        addr += adddress.area;
    }
    if(adddress.city){
        addr += ', ';
        addr += adddress.city;
    }
    if(adddress.state){
        addr += ', ';
        addr += adddress.state;
    }
    if(adddress.pin){
        addr += ', ';
        addr += adddress.pin;
    }

    return addr;
}