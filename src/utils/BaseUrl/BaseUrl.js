export const url = 'https://dev.chargebizz.com/api';
// export const url = 'http://192.168.43.215:8081/api'
export const mapUrl = 'https://maps.googleapis.com/maps/api/place/autocomplete/json';
export const mapPlaceUrl = 'https://maps.googleapis.com/maps/api/place/details/json';
export const placesParams = {
    key: 'AIzaSyBxJQedWcV3yjCq9nUBe0FH9xVmMISqiiM',
    sessiontoken: 1234567890,
    fields: 'photos,formatted_address,name,rating,opening_hours,geometry'
}

export const placesSelectedParams = {
    key: 'AIzaSyBxJQedWcV3yjCq9nUBe0FH9xVmMISqiiM',
    inputtype: 'textquery',
    fields: 'photos,formatted_address,name,rating,opening_hours,geometry'
}

export const tcUrl = 'https://chargebizz.com/term-conditions/';
export const imageUrl = 'https://dev.chargebizz.com/';