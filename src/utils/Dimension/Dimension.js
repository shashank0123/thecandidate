import {Dimensions} from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default dimension ={
    width:windowWidth,
    height:windowHeight
}