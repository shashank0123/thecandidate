export const fontFamily = {
    regular: "Muli-Regular",
    bold: "CoreSansG-Medium",
    extraBold: "CoreSansG-Bold"
}
