export const commonStyle = {
    white: {
        color: 'white'
    },
    flexRow: {
        flexDirection: 'row'
    },
    flexEnd: {
        alignItems: 'flex-end'
    },
    textCenter: {
        justifyContent: 'center'
    },
    alignCenter: {
        alignItems: 'center'
    },
    small: {
        fontSize: 13
    },
    img: {
        resizeMode: 'contain'
    },
    forward: {
        height: 55,
        width: 55,
    },
    blue: {
        color: '#1371EF'
    },
    gray: {
        color: '#808080'
    },
    button: {
        width: '100%',
        display: 'flex',
        borderRadius: 5,
        height: 60,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:'#1371EF'
    },
    onlyTextCenter:{
        textAlign:'center'
    }
}