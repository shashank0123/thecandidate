import moment from 'moment';

export function timeArray(mainTime) {
    let endTime = moment('23:45', 'HH:mm').format('HH:mm')
    let startofDay;
    if (mainTime) {
        console.log(mainTime)
        startofDay = moment(mainTime, 'HH:mm').format('hh:mm A');
    }
    else {
        startofDay = moment().startOf('day').format("hh:mm A");
    }
    let timesArray = [];
    let startTime = moment(startofDay, 'hh:mm A').format('HH:mm');
    while (startTime < endTime) {
        let value = moment(startofDay, 'hh:mm A').format('HH:mm');
        startTime = value;
        timesArray.push({ name: startofDay, value: value });
        startofDay = moment(startofDay, 'hh:mm A').add(15, 'minutes').format("hh:mm A");

    }

    return timesArray;
}

export function slotTimeArrays() {
    let timesArrays = [{ name: '30 mins', value: 30 }];
    for (let i = 0; i < 9; i++) {
        let min = timesArrays[timesArrays.length - 1].value;
        let newTime = min + 30;
        let name = Math.floor(newTime / 60) + ' hrs';
        if ((newTime % 60) > 0) {
            name += ' ' + (newTime % 60) + ' mins'
        }
        timesArrays.push({
            name: name,
            value: newTime
        })
    }
    return timesArrays;
}

export function dateFormat(date) {
    // return moment(date).format(format)
    // console.log()
    return moment(new Date(date)).format('DD MMM YY,hh:mm a');
}

export function time_convert(num) {
    var hours = Math.floor(num / 60);
    var minutes = num % 60;
    let time = '';
    if (hours > 0) {
        time += hours + ' hour ';
    }

    if (minutes > 0) {
        time += minutes + ' min';
    }

    return time;
}