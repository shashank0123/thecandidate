// import { Toast } from 'native-base';
import Toast from 'react-native-tiny-toast'
import Dimension from '../Dimension/Dimension';


export function msg(message, err) {
    // Toast.show({
    //     text: message,
    //     buttonText: 'Close',
    //     duration: 3000,
    //     position: "top",
    //     textStyle: { fontFamily: 'CoreSansG-Medium' },
    //     type: "success",
    //     style: {
    //         marginHorizontal: 20,
    //         zIndex: 100000,
    //         position: 'relative',
    //         borderRadius: 5
    //     }
    // });

    Toast.show(message, {
        position: Toast.position.TOP,
        containerStyle: {
            width: Dimension.width-40,
            marginHorizontal: 20,
            borderRadius: 5,
            paddingVertical:15,
            backgroundColor:'green',
            position:'relative',
            top:-20,
        },
        textStyle: {
            fontFamily:'CoreSansG-Medium'
        },
    });
}

export function errMsg(err) {
    let message = err['data'] ? err['data'] ? err['data']['msg'] : err : err;
    // Toast.show({
    //     text: message,
    //     buttonText: 'Close',
    //     duration: 3000,
    //     position: "top",
    //     textStyle: { fontFamily: 'CoreSansG-Medium' },
    //     type: "danger",
    //     style: {
    //         marginHorizontal: 20,
    //         borderRadius: 5,
    //         position: 'relative',
    //         zIndex: 100000
    //     }

    // });
    Toast.show(message, {
        position: Toast.position.TOP,
        containerStyle: {
            width: Dimension.width-40,
            marginHorizontal: 20,
            borderRadius: 5,
            paddingVertical:15,
            backgroundColor:'red',
            position:'relative',
            top:-20,
            zIndex:10000
        },
        textStyle: {
            fontFamily:'CoreSansG-Medium'
        },
    });
}


export function simpleMsg(msg) {
    let message = msg;
    // Toast.show({
    //     text: message,
    //     buttonText: '',
    //     duration: 4000,
    //     position: "top",
    //     textStyle: { fontFamily: 'CoreSansG-Medium', fontSize: 15 },
    //     // type: "danger",
    //     style: {
    //         marginHorizontal: 20,
    //         borderRadius: 5,
    //         position: 'relative',
    //         zIndex: 100000,
    //         backgroundColor: 'black',
    //     }

    // });
    Toast.show(message, {
        position: Toast.position.TOP,
        containerStyle: {
            width: Dimension.width-40,
            marginHorizontal: 20,
            borderRadius: 5,
            paddingVertical:15,
            backgroundColor:'black',
            position:'relative',
            top:-20,
        },
        textStyle: {
            fontFamily:'CoreSansG-Medium',
            fontSize: 15 
        },
    });
}