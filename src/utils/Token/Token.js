import AsyncStorage from "@react-native-community/async-storage";
import axios from 'axios';

export function saveToken(token, user) {
    AsyncStorage.setItem("token", token);
    // if (user) {
    //     AsyncStorage.setItem("userData", JSON.stringify(user));
    // }
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + token;
}

export function getToken() {
    return AsyncStorage.getItem('token');
}

export function removeToken() {
    return AsyncStorage.removeItem('token');
}

export function saveLoginCount(val) {
    return AsyncStorage.setItem('login', val);
}

export function getLoginCount() {
    return AsyncStorage.getItem('login');
}