const validate = (val, rules, control) => {
    let isValid = {
        isValid: true,
        text: ''
    };

    control = control.toUpperCase()[0] + control.slice(1)
    for (let rule in rules) {

        if (rule === 'required') {
            isValid = requiredValidator(val, isValid.isValid, control);
            if (!isValid.isValid) {
                break;
            }
        }

        else if (rule === 'isEmail') {
            isValid = emailValidator(val, isValid.isValid, control);
            if (!isValid.isValid) {
                break;
            }
        }
        else if (rule === 'isPhone') {
            isValid = phoneValidator(val, isValid.isValid, control);
            if (!isValid.isValid) {
                break;
            }
        }
        else {
            isValid = {
                isValid: true,
                text: ''
            };
        }

    }

    return isValid;
}
const requiredValidator = (val, previousValid, control) => {
    if (val !== '') {
        return { isValid: (true && previousValid), text: '' }
    }
    else {
        return { isValid: false, text: `${control} is Required` }
    }

}

const emailValidator = (val, previousValid, control) => {
    if (/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(val)) {
        return { isValid: (true && previousValid), text: '' }
    }
    else {
        return { isValid: false, text: `${control} is not valid` }
    }
}

const phoneValidator = (val, previousValid, control) => {
    if (/[0-9]{10}/.test(val)) {
        return { isValid: (true && previousValid), text: '' }
    }
    else {
        return { isValid: false, text: `${control} is not valid` }
    }

}


export default validate;